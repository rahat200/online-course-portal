CREATE SEQUENCE user_seq;

CREATE OR REPLACE TRIGGER user_inc 
BEFORE INSERT ON users 
FOR EACH ROW

BEGIN
  SELECT user_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/


///////////////////////////////////////////////////////////////////////////////////////////////////
CREATE SEQUENCE course_seq;

CREATE OR REPLACE TRIGGER course_inc 
BEFORE INSERT ON course 
FOR EACH ROW

BEGIN
  SELECT course_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

///////////////////////////////////////////////////////////////////////////////////////////////////
CREATE SEQUENCE quiz_seq;

CREATE OR REPLACE TRIGGER quiz_inc 
BEFORE INSERT ON quiz
FOR EACH ROW

BEGIN
  SELECT quiz_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

///////////////////////////////////////////////////////////////////////////////////////////////////

CREATE SEQUENCE ques_seq;

CREATE OR REPLACE TRIGGER ques_inc 
BEFORE INSERT ON question 
FOR EACH ROW

BEGIN
  SELECT ques_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

/////////////////////////////////////////////////////////////////////////////////////////////////////////

CREATE SEQUENCE choice_seq;

CREATE OR REPLACE TRIGGER choice_inc 
BEFORE INSERT ON choice
FOR EACH ROW

BEGIN
  SELECT choice_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

////////////////////////////////////////////////////////////////////////////////////////////////////////////






////////////////////////////////////////////////////////////////////////////////////////////////////////////


CREATE SEQUENCE category_seq;

CREATE OR REPLACE TRIGGER category_inc 
BEFORE INSERT ON category 
FOR EACH ROW

BEGIN
  SELECT category_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

////////////////////////////////////////////////////////////////////////////////////////////////////////////

CREATE SEQUENCE material_seq;

CREATE OR REPLACE TRIGGER material_inc 
BEFORE INSERT ON material 
FOR EACH ROW

BEGIN
  SELECT material_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

////////////////////////////////////////////////////////////////////////////////////////////////////////////




CREATE SEQUENCE assignment_seq;

CREATE OR REPLACE TRIGGER assignment_inc 
BEFORE INSERT ON assignment 
FOR EACH ROW

BEGIN
  SELECT assignment_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

////////////////////////////////////////////////////////////////////////////////////////////////////////////



CREATE SEQUENCE submitted_assignment_seq;

CREATE OR REPLACE TRIGGER submitted_assignment_inc 
BEFORE INSERT ON submitted_assignment 
FOR EACH ROW

BEGIN
  SELECT submitted_assignment_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

////////////////////////////////////////////////////////////////////////////////////////////////////////////


CREATE SEQUENCE faq_seq;

CREATE OR REPLACE TRIGGER faq_inc 
BEFORE INSERT ON faq 
FOR EACH ROW

BEGIN
  SELECT faq_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

////////////////////////////////////////////////////////////////////////////////////////////////////////////


CREATE SEQUENCE lecture_seq;

CREATE OR REPLACE TRIGGER lecture_inc 
BEFORE INSERT ON lecture 
FOR EACH ROW

BEGIN
  SELECT lecture_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

////////////////////////////////////////////////////////////////////////////////////////////////////////////







////////////////////////////////////////////////////////////////////////////////////////////////////////////


CREATE SEQUENCE teacherapproval_seq;

CREATE OR REPLACE TRIGGER teacherapproval_inc 
BEFORE INSERT ON teacher_approval
FOR EACH ROW

BEGIN
  SELECT teacherapproval_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/



////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////
CREATE SEQUENCE forumquestion_seq;

CREATE OR REPLACE TRIGGER forumquestion_inc 
BEFORE INSERT ON forum_question 
FOR EACH ROW

BEGIN
  SELECT forumquestion_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/


/////////////////////////////////////////////////////////////////////////////////////////////////////////

CREATE SEQUENCE forumanswer_seq;

CREATE OR REPLACE TRIGGER forumanswer_inc 
BEFORE INSERT ON forum_answer 
FOR EACH ROW

BEGIN
  SELECT forumanswer_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

/////////////////////////////////////////////////////////////////////////////////////////////////////////

CREATE SEQUENCE assignment_seq;

CREATE OR REPLACE TRIGGER assignment_inc 
BEFORE INSERT ON assignment
FOR EACH ROW

BEGIN
  SELECT assignment_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/


/////////////////////////////////////////////////////////////////////////////////////////////////////////

CREATE SEQUENCE submitted_assignment_seq;

CREATE OR REPLACE TRIGGER submitted_assignment_inc 
BEFORE INSERT ON submitted_assignment
FOR EACH ROW

BEGIN
  SELECT submitted_assignment_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

create or replace trigger teacher_approved
after update on TEACHER_APPROVAL
for each row
declare
	
begin	
	update TEACHER set is_approved  = 1 where id = :new.teacher_id;	

end;
/


//UPDATE TEACHER_APPROVAL SET APPROVED_DATE = '12-MAR-199//4' WHERE TEACHER_ID=43;


///////////////////////////////////////////////////////////////////////////////////////////////////////

create or replace trigger teacher_request
after insert on TEACHER
for each row
declare
	
begin	
	insert into teacher_approval (teacher_id) values (:new.id);
end;
/

///////////////////////////////////////////////////////////////////////////////////////////////////////


update USERS set user_role = 2 where id = :new.teacher_id;

///////////////////////////////////////////////////////////////////////////////////////////////////////



create or replace trigger teacher_approved_users
after update on TEACHER_APPROVAL
for each row
declare
	
begin	
	update USERS set user_role = 2 where id = :new.teacher_id;
end;
/


/////////////////////////////////////////////////////////////////////////////////////////////////////////




create or replace trigger delete_lecture_material
after delete on LECTURE
for each row
declare
begin
  delete from MATERIAL where lecture_id = :old.id;
end;
/


create or replace trigger delete_quiz_questions
  after delete on quiz
  for each row
  declare
  begin
    delete from question where quiz_id = :old.id;
  end;
  /

create or replace trigger delete_question_choice
  after delete on question
  for each row
  declare
  begin
    delete from choice where ques_id = :old.id;
  end;
  /

create or replace trigger delete_course_data
  before delete on course
  for each row
  declare
  begin
    delete from course_teacher where course_id = :new.id;
    delete from registered_student where course_id = :new.id;
    delete from assignment where course_id = :new.id;
    delete from quiz where course_id = :new.id;
    delete from lecture where course_id = :new.id;
  end;
  /

