/////////category_model/////////////////

create or replace procedure create_category(cat_name in varchar2) as

begin
	insert into category (name) values (cat_name);
end;
/

///////course_model////course_teacher_model/////done/////


create or replace procedure create_course
	( teacherid in number,
	 vtitle in varchar2,
	 prerequisite in varchar2,
	 catid in number,
	 startdate in date,
	 imagepath in varchar2,
	 vsyllabus in varchar2,
	 format in varchar2,
	 vdescription in varchar2,
	 about in varchar2,
	 videopath in varchar2,
	 language in varchar2,
	 length in number,
	 effort in number,
	 ispublished in number,
	 posted_date in date) is

last_inserted_id number;

begin
	insert into course (title,pre_req_course,category_id,start_date,image_path,
		syllabus,course_format,description,about_course,intro_video_path,
		course_language,course_length_week,effort_per_week,is_published,course_posted_date) 
	values (vtitle,prerequisite,catid,startdate,imagepath,vsyllabus,format,
		vdescription,about,videopath,language,length,effort,ispublished,posted_date);

	select course_seq.currval into last_inserted_id from dual;
	
	insert into course_teacher values (teacherid,last_inserted_id);

end;
/

create or replace procedure update_course
	(courseid in number,
	 vtitle in varchar2,
	 prerequisite in varchar2,
	 catid in number,
	 startdate in date,
	 vsyllabus in varchar2,
	 format in varchar2,
	 vdescription in varchar2,
	 about in varchar2,
	 language in varchar2,
	 length in number,
	 effort in number,
	 ispublished in number) is

begin

	update course set 
		title = vtitle,pre_req_course = prerequisite,category_id=catid,
		start_date = startdate,syllabus = vsyllabus,course_format = format
		,description = vdescription,about_course = about,course_language = language,
		course_length_week = length,effort_per_week = effort,
		is_published = ispublished where id = courseid;

end;
/

#execute create_course(101,'title','prereq',10,'01-jan-12','imagepath','vsyllabus','format','vdescription','about','videopath','language',5,10,0);

////////lecture_model//material_model//////done/////

create or replace procedure create_lecture
	(topic in varchar2,
	courseid in number,
	weeknum in number,
	overview in varchar2,
	updatetime in date,
	mat_type in varchar2,
	contentpath in varchar2) is

last_inserted_id number;
begin
	insert into lecture (topic_name,course_id,week_num,lecture_overview,update_time)
		values (topic,courseid,weeknum,overview,updatetime);
	
	select lecture_seq.currval into last_inserted_id from dual;

	insert into material (material_type,content_path,lecture_id)
		values (mat_type,contentpath,last_inserted_id);

end;
/

#execute create_lecture('topic',10,10,'overview','01-jan-12','video','contentpath');

create or replace procedure update_lecture
	(lecture_id in number,
	topic in varchar2,
	overview in varchar2,
	updatetime in date) is
begin
	update lecture set topic_name = topic,lecture_overview=overview
		,update_time = updatetime where id = lecture_id;
end;
/


create or replace procedure delete_lecture
	(lecid in number) is
begin
	delete from lecture where id = lecid;
end;
/

////////////////student_model//////done////////

create or replace procedure register_for_course 
	(courseid in number,studentid in number) is

begin
	insert into registered_student values (courseid,studentid);
end;
/	

//////////////////teacher_model/////////done///////////

create or replace procedure create_teacher
	(teacherid in number,
	phone_v in varchar2,
	gender_v in varchar2,
	picturepath in varchar2,
	birthdate in date,
	inst_name in varchar2,
	expertise in varchar2,
	isapproved in number) is
begin 
	insert into teacher (id,phone,gender,picture_path,birth_date,institution_name,
		field_of_expertise,is_approved) 
	values (teacherid,phone_v,gender_v,picturepath,birthdate,inst_name,expertise,isapproved);
end;
/

////////////////user_model////////done////////////

create or replace procedure create_user
	(u_name in varchar2,
	pass in varchar2,
	mail in varchar2,
	fullname in varchar2,
	userrole in number) is
begin
	insert into users (username,password,email,full_name,user_role) 
	values (u_name,pass,mail,fullname,userrole);
end;
/


create or replace procedure delete_user
	(userid in number) is
begin
	delete from users where id = userid;
end;
/

////////////quiz_model/////////////////

create or replace procedure create_quiz
	(courseid in number,
	weeknum in number,
	marks_v in number,
	updatetime in date,
	harddeadline in date,
	topic_v in varchar2 ) is
begin
	insert into quiz (course_id,week_num,marks,update_time,hard_deadline,topic)
	 values (courseid,weeknum,marks_v,updatetime,harddeadline,topic_v);
end;
/





create or replace procedure delete_quiz
	(quizid in number) is
begin
	delete from quiz where id = quizid;
end;
/


//////////forum_question_model//////////done///////////

create or replace procedure forum_new_question
	(userid in number,
	 text_v in varchar2,
	 lec_id in number,
	 postdate in date) is
begin
	insert into forum_question (user_id,text,lecture_id,POSTED_DATE) 
		values (userid,text_v,lec_id,postdate);
end;
/


////////////forum_answer_model//////////done///////////

create or replace procedure forum_new_answer
	(userid in number,
	 text_v in varchar2,
	 forumq_id in number,
	 postdate in date) is
begin
	insert into forum_answer (user_id,text,forum_question_id,POSTED_DATE) 
		values (userid,text_v,forumq_id,postdate);
end;
/

//////////////////quiz_result_model/////////////
create or replace procedure insert_quiz_result
	(quizid in number,
	stdid in number,
	marks_v in number,
	grade_v in varchar2) is
begin
	insert into quiz_result values (quizid,stdid,marks_v,grade_v);
end;
/

create or replace procedure delete_quiz (quizid in number) is

begin
	delete from quiz where id = quizid;
end;
/

/////////////question_model////////////
create or replace procedure create_question
	(qsntext in varchar2,
		ansid in number,
		quizid in number,
		ch1 in varchar2,
		ch2 in varchar2,
		ch3 in varchar2,
		ch4 in varchar2,
		ans in varchar2) is

last_inserted_id number;
begin
    insert into question (ques_text,answer_id,quiz_id) values (qsntext,ansid,quizid);
    select question_seq.currval into last_inserted_id from dual;
    insert into choice (ques_id,choice_text) values (last_inserted_id,ch1);
    insert into choice (ques_id,choice_text) values (last_inserted_id,ch2);
    insert into choice (ques_id,choice_text) values (last_inserted_id,ch3);
    insert into choice (ques_id,choice_text) values (last_inserted_id,ch3);
end;
/
