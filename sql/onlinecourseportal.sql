

CREATE TABLE users (
  id number(11)  NOT NULL ,
  username varchar2(100) NOT NULL,
  password varchar2(40) NOT NULL,
  email varchar2(100) NOT NULL,        
  full_name varchar2(100) NOT NULL,  
  user_role number(1) NOT NULL,
  PRIMARY KEY (id)
);



CREATE SEQUENCE user_seq;

CREATE OR REPLACE TRIGGER user_inc 
BEFORE INSERT ON users 
FOR EACH ROW

BEGIN
  SELECT user_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

INSERT INTO users (username, password, email, full_name, user_role) VALUES
('administrator','59beecdf7fc966e2f17fd8f65a4a9aeb09d4a3d4','g@g.com','FAYSAL',0);

  
  
ALTER TRIGGER grp_bir
  RENAME TO grp_inc;
  
 //////////////////////////////////////////////////////////////////////////


CREATE TABLE courses (
  id number(11) NOT NULL ,  
  title varchar2(100) NOT NULL,
  pre_req_course varchar2(50) DEFAULT NULL,
  category_id number(11)  DEFAULT NULL,  
  start_date date NOT NULL,  
  image_path varchar2(100) DEFAULT NULL,
  syllabus varchar2(200)  DEFAULT NULL,
  course_format varchar2(200)  DEFAULT NULL,
  description varchar2(200)  DEFAULT NULL,
  intro_video_path varchar2(100)  DEFAULT NULL,
  course_language varchar2(50)  NOT NULL,
  course_length_week number(3)  DEFAULT NULL,
  effort_per_week number(3) DEFAULT NULL,
  is_published number (1) DEFAULT NULL,
  course_posted_date date DEFAULT ('1-JAN-80'),  
  PRIMARY KEY (id)
);

CREATE SEQUENCE course_seq;

CREATE OR REPLACE TRIGGER course_inc 
BEFORE INSERT ON course 
FOR EACH ROW

BEGIN
  SELECT course_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

///////////////////////////////////////////////////////////////////////////////////////////////////




CREATE TABLE exams (
  id number(11) NOT NULL ,
  course_id number(11)  NOT NULL,  
  title varchar2(50) NOT NULL,   
  start_date date DEFAULT ('1-JAN-80'),
  end_date date DEFAULT ('1-JAN-80'),
  exam_time interval day to second NOT NULL,
  full_mark number(4)  NOT NULL,
  syllabus varchar2(200) NOT NULL,     
  PRIMARY KEY (id)
);

CREATE SEQUENCE exam_seq;

CREATE OR REPLACE TRIGGER exam_inc 
BEFORE INSERT ON exams 
FOR EACH ROW

BEGIN
  SELECT exam_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

INSERT into exams(course_id,title,start_date,end_date,exam_time,full_mark,syllabus)
values(1,'Algorithm','3-JAN-81','3-FEB-81', numtodsinterval( 1250, 'second' ), 80, 'No Syllabus');

//////////////////////////////////////////////////////////////////////////////////////////////


CREATE TABLE questions (
  id number(11) NOT NULL ,  
  ques varchar2(200) NOT NULL,     
  answer_id number(2) NOT NULL ,  
  exam_id number(11) NOT NULL ,    
  PRIMARY KEY (id)
);

CREATE SEQUENCE ques_seq;

CREATE OR REPLACE TRIGGER ques_inc 
BEFORE INSERT ON questions 
FOR EACH ROW

BEGIN
  SELECT ques_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

/////////////////////////////////////////////////////////////////////////////////////////////////////////

CREATE TABLE choices (
  id number(11) NOT NULL ,  
  ques_id number(11) NOT NULL,     
  text varchar2(200) NOT NULL,
  PRIMARY KEY (id)
);

CREATE SEQUENCE choice_seq;

CREATE OR REPLACE TRIGGER choice_inc 
BEFORE INSERT ON choices 
FOR EACH ROW

BEGIN
  SELECT choice_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

////////////////////////////////////////////////////////////////////////////////////////////////////////////


CREATE TABLE teachers (
  id number(11) NOT NULL ,
  phone varchar2(20) DEFAULT NULL,
  gender varchar2(6) DEFAULT NULL,
  picture_path varchar2(100) DEFAULT NULL,
  birth_date date DEFAULT NULL,
  institution_name varchar2(100) DEFAULT NULL,
  field_of_expertise varchar2(50) NOT NULL,
  isApproved number(1),
  PRIMARY KEY (id)
);



CREATE SEQUENCE teacher_seq;

CREATE OR REPLACE TRIGGER teacher_inc 
BEFORE INSERT ON teachers 
FOR EACH ROW

BEGIN
  SELECT teacher_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/



////////////////////////////////////////////////////////////////////////////////////////////////////////////



CREATE TABLE category(
  id number (11) NOT NULL,
  name varchar2(50) NOT NULL,  
  PRIMARY KEY (id)
);

CREATE SEQUENCE category_seq;

CREATE OR REPLACE TRIGGER category_inc 
BEFORE INSERT ON category 
FOR EACH ROW

BEGIN
  SELECT category_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

////////////////////////////////////////////////////////////////////////////////////////////////////////////




CREATE TABLE material(
  id number (11) NOT NULL,
  material_type varchar2(50) NOT NULL,  
  content_path varchar2(100) NOT NULL,  
  lecture_id number(11) NOT NULL,  
  PRIMARY KEY (id)
);

CREATE SEQUENCE material_seq;

CREATE OR REPLACE TRIGGER material_inc 
BEFORE INSERT ON material 
FOR EACH ROW

BEGIN
  SELECT material_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

////////////////////////////////////////////////////////////////////////////////////////////////////////////




CREATE TABLE assignment(
  id number (11) NOT NULL,
  course_id number (11) NOT NULL,  
  start_date date ,
  end_date date ,
  title varchar2(50) NOT NULL,   
  text varchar2(50) DEFAULT NULL,  
  content_path varchar2(100) DEFAULT NULL,    
  PRIMARY KEY (id)
);

CREATE SEQUENCE assignment_seq;

CREATE OR REPLACE TRIGGER assignment_inc 
BEFORE INSERT ON assignment 
FOR EACH ROW

BEGIN
  SELECT assignment_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

////////////////////////////////////////////////////////////////////////////////////////////////////////////



CREATE TABLE submittedassignment(
  id number (11) NOT NULL,
  assignment_id number (11) NOT NULL,  
  student_id number (11) NOT NULL,  
  submitted_date date DEFAULT('1-JAN-80'),  
  content_path varchar2(100) DEFAULT NULL,    
  PRIMARY KEY (id)
);

CREATE SEQUENCE submittedassignment_seq;

CREATE OR REPLACE TRIGGER submittedassignment_inc 
BEFORE INSERT ON submittedassignment 
FOR EACH ROW

BEGIN
  SELECT submittedassignment_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

////////////////////////////////////////////////////////////////////////////////////////////////////////////




CREATE TABLE faq(
  id number (11) NOT NULL,
  course_id number (11) NOT NULL,  
  question_text varchar2(200) DEFAULT NULL,    
  answer_text varchar2(200) DEFAULT NULL,    
  PRIMARY KEY (id)
);

CREATE SEQUENCE faq_seq;

CREATE OR REPLACE TRIGGER faq_inc 
BEFORE INSERT ON faq 
FOR EACH ROW

BEGIN
  SELECT faq_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

////////////////////////////////////////////////////////////////////////////////////////////////////////////





CREATE TABLE lecture(
  id number (11) NOT NULL,
  name varchar2(100) NOT NULL,
  course_id number (11) NOT NULL,  
  week_id number (11) NOT NULL,  
  lecture_overview varchar2(200) NOT NULL,
  PRIMARY KEY (id)
);

CREATE SEQUENCE lecture_seq;

CREATE OR REPLACE TRIGGER lecture_inc 
BEFORE INSERT ON lecture 
FOR EACH ROW

BEGIN
  SELECT lecture_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

////////////////////////////////////////////////////////////////////////////////////////////////////////////



CREATE TABLE week(
  id number (11) NOT NULL,
  name varchar2(50) NOT NULL,
  PRIMARY KEY (id)
);
INSERT INTO WEEK(id,name) VALUES (1,'WEEK-1');
INSERT INTO WEEK(id,name) VALUES (2,'WEEK-2');
INSERT INTO WEEK(id,name) VALUES (3,'WEEK-3');
INSERT INTO WEEK(id,name) VALUES (4,'WEEK-4');
////////////////////////////////////////////////////////////////////////////////////////////////////////////

CREATE TABLE examresult(
  id number (11) NOT NULL,  
  exam_id number (11) NOT NULL,  
  student_id number (11) NOT NULL,  
  mark number(4) NOT NULL,
  grade varchar2(6) NOT NULL,
  PRIMARY KEY (id)
);

CREATE SEQUENCE examresult_seq;

CREATE OR REPLACE TRIGGER examresult_inc 
BEFORE INSERT ON examresult 
FOR EACH ROW

BEGIN
  SELECT examresult_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

////////////////////////////////////////////////////////////////////////////////////////////////////////////


create TABLE forumquestion (
  id number(11) NOT NULL,
  user_id number(11) NOT NULL,
  text varchar2(200) NOT NULL,
  lecture_id number(11) NOT NULL,  
  date_time date NOT NULL,
  PRIMARY KEY(id)
);


CREATE SEQUENCE forumquestion_seq;

CREATE OR REPLACE TRIGGER forumquestion_inc 
BEFORE INSERT ON forumquestion 
FOR EACH ROW

BEGIN
  SELECT forumquestion_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/





////////////////////////////////////////////////////////////////////////////////////////////////////////////


create TABLE forumanswer(
  id number(11) NOT NULL,
  user_id number(11) NOT NULL,
  text varchar2(200) NOT NULL,
  forum_question_id number(11) NOT NULL,  
  date_time date NOT NULL,
  PRIMARY KEY(id)
);

CREATE SEQUENCE forumanswer_seq;

CREATE OR REPLACE TRIGGER forumanswer_inc 
BEFORE INSERT ON forumanswer 
FOR EACH ROW

BEGIN
  SELECT forumanswer_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/



////////////////////////////////////////////////////////////////////////////////////////////////////////////



CREATE TABLE courseteacher(
  id number (11) NOT NULL,
  teacher_id number(11) NOT NULL,
  course_id number(11) NOT NULL,
  PRIMARY KEY (id)
);

CREATE SEQUENCE course_teacher_seq;

CREATE OR REPLACE TRIGGER choice_teacher_inc 
BEFORE INSERT ON courseteacher 
FOR EACH ROW

BEGIN
  SELECT course_teacher_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

////////////////////////////////////////////////////////////////////////////////////////////////////////////


CREATE TABLE registeredstudent(
  id number (11) NOT NULL,
  course_id number (11) NOT NULL,  
  student_id number (11) NOT NULL,  
  PRIMARY KEY (id)
);

CREATE SEQUENCE registeredstudent_seq;

CREATE OR REPLACE TRIGGER registeredstudent_inc 
BEFORE INSERT ON registered_student 
FOR EACH ROW

BEGIN
  SELECT registeredstudent_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

////////////////////////////////////////////////////////////////////////////////////////////////////////////



CREATE TABLE course_sequel(
  id number(11) NOT NULL,
  parent_course_id number(11) NOT NULL,
  child_course_id number(11) NOT NULL,  
  PRIMARY KEY(id)
);


////////////////////////////////////////////////////////////////////////////////////////////////////////////



CREATE TABLE weekly_assignment(
  id number(11) NOT NULL,
  week_id number(11) NOT NULL,
  assignment_id number(11) NOT NULL,  
  PRIMARY KEY(id)
);


////////////////////////////////////////////////////////////////////////////////////////////////////////////


CREATE TABLE weekly_exam(
  id number(11) NOT NULL,
  week_id number(11) NOT NULL,
  exam_id number(11) NOT NULL,  
  PRIMARY KEY(id)
);


////////////////////////////////////////////////////////////////////////////////////////////////////////////



CREATE TABLE teacher_approval(
  id number(11) NOT NULL,
  teacher_id number(11) NOT NULL,
  admin_id number(11) NOT NULL,  
  approved_date date DEFAULT NULL,
  PRIMARY KEY(id)
);


////////////////////////////////////////////////////////////////////////////////////////////////////////////
