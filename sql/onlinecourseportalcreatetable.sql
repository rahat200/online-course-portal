drop table users;
drop table course;
drop table quiz;
drop table question;
drop table choice;
drop table teacher;
drop table category;
drop table material;
drop table category;
drop table week;
drop table lecture;
drop table faq;
drop table assignment;
drop table teacher_approval;
drop table weekly_exam;
drop table course_sequel;
drop table weekly_assignment;
drop table submitted_assignment;
drop table course_teacher;
drop table registered_student;
drop table forum_answer;
drop table forum_question ;
drop table examresult;





CREATE TABLE users (
  id number(11)  NOT NULL ,
  username varchar2(100) NOT NULL,
  password varchar2(40) NOT NULL,
  email varchar2(100) NOT NULL,        
  full_name varchar2(100) NOT NULL,  
  user_role number(1) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE course (
  id number(11) NOT NULL ,  
  title varchar2(100) NOT NULL,
  pre_req_course varchar2(1000) DEFAULT NULL,
  category_id number(11)  DEFAULT NULL,  
  start_date date NOT NULL,  
  image_path varchar2(100) DEFAULT NULL,
  syllabus varchar2(200)  DEFAULT NULL,
  course_format varchar2(1000)  DEFAULT NULL,
  description varchar2(1500)  DEFAULT NULL,
  about_course varchar2(1000) DEFAULT NULL,
  intro_video_path varchar2(100)  DEFAULT NULL,
  course_language varchar2(50)  NOT NULL,
  course_length_week number(3)  DEFAULT NULL,
  effort_per_week number(3) DEFAULT NULL,
  is_published number (1) DEFAULT NULL,
  course_posted_date date DEFAULT NULL,  
  PRIMARY KEY (id)
);


CREATE TABLE quiz(
  id number(11) NOT NULL ,
  course_id number(11)  NOT NULL,
  week_num number(3) NOT NULL,
  marks number(5) DEFAULT 20,     
  update_time date ,
  hard_deadline date ,
  topic varchar2(200) NOT NULL,     
  PRIMARY KEY (id)
);


CREATE TABLE question (
  id number(11) NOT NULL ,  
  ques_text varchar2(1000) NOT NULL,     
  answer_id number(2) DEFAULT NULL ,  
  quiz_id number(11) NOT NULL ,    
  PRIMARY KEY (id)
);

CREATE TABLE choice (
  id number(11) NOT NULL ,  
  ques_id number(11) NOT NULL,     
  choice_text varchar2(200) NOT NULL,
  PRIMARY KEY (id)
);
 
CREATE TABLE teacher (
  id number(11) NOT NULL ,
  phone varchar2(20) DEFAULT NULL,
  gender varchar2(6) DEFAULT NULL,
  picture_path varchar2(100) DEFAULT NULL,
  birth_date date DEFAULT NULL,
  institution_name varchar2(200) DEFAULT NULL,
  field_of_expertise varchar2(100) NOT NULL,
  is_approved number(1),
  PRIMARY KEY (id)
);


CREATE TABLE category(
  id number (11) NOT NULL,
  name varchar2(100) NOT NULL,  
  PRIMARY KEY (id)
);

CREATE TABLE material(
  id number (11) NOT NULL,
  material_type varchar2(50) NOT NULL,  
  content_path varchar2(100) NOT NULL,  
  lecture_id number(11) NOT NULL,  
  PRIMARY KEY (id)
);

CREATE TABLE assignment(
  id number (11) NOT NULL,
  course_id number (11) NOT NULL,  
  start_date date ,
  end_date date ,
  title varchar2(50) NOT NULL,   
  text varchar2(1500) DEFAULT NULL,  
  content_path varchar2(100) DEFAULT NULL,
  foreign key (course_id) references course,    
  PRIMARY KEY (id)
);




CREATE TABLE faq(
  id number (11) NOT NULL,
  course_id number (11) NOT NULL,  
  question_text varchar2(1000) DEFAULT NULL,    
  answer_text varchar2(1000) DEFAULT NULL,    
  PRIMARY KEY (id)
);

CREATE TABLE lecture(
  id number (11) NOT NULL,
  topic_name varchar2(500) NOT NULL,
  course_id number (11) NOT NULL,  
  week_num number (3) NOT NULL,  
  lecture_overview varchar2(1000) NOT NULL,
  update_time date DEFAULT NULL,
  PRIMARY KEY (id)
);




create TABLE forum_question (
  id number(11) NOT NULL,
  user_id number(11) NOT NULL,
  text varchar2(1000) NOT NULL,
  lecture_id number(11) NOT NULL,  
  POSTED_DATE date NOT NULL,
  foreign key (lecture_id) references lecture,
  foreign key (user_id) references users,
  PRIMARY KEY(id)
);


create TABLE forum_answer(
  id number(11) NOT NULL,
  user_id number(11) NOT NULL,
  text varchar2(1000) NOT NULL,
  forum_question_id number(11) NOT NULL,
  POSTED_DATE date NOT NULL,
  foreign key (user_id) references users,
  foreign key (forum_question_id) references forum_question,
  PRIMARY KEY(id)
);


CREATE TABLE quiz_result(  
  quiz_id number (11) NOT NULL,  
  student_id number (11) NOT NULL,  
  marks number(4) NOT NULL,
  grade varchar2(6) NOT NULL,
  foreign key (quiz_id) references quiz,
  foreign key (student_id) references users
);


CREATE TABLE submitted_assignment(
  id number (11) NOT NULL,
  assignment_id number (11) NOT NULL,  
  student_id number (11) NOT NULL,  
  submitted_date date,  
  content_path varchar2(100) DEFAULT NULL,    
  foreign key (assignment_id) references assignment,
  foreign key (student_id) references users,
  PRIMARY KEY (id)
);

CREATE TABLE course_teacher(
  teacher_id number(11) NOT NULL,
  course_id number(11) NOT NULL,
  foreign key (course_id) references course,
  foreign key (teacher_id) references teacher,
  PRIMARY KEY (course_id,teacher_id)
);

CREATE TABLE registered_student(
  course_id number (11) NOT NULL,  
  student_id number (11) NOT NULL,  
  foreign key (course_id) references course,
  foreign key (student_id) references users,
  PRIMARY KEY (course_id,student_id)
);

CREATE TABLE course_sequel(
  id number(11) NOT NULL,
  parent_course_id number(11) NOT NULL,
  child_course_id number(11) NOT NULL,  
  PRIMARY KEY(id)
);







CREATE TABLE teacher_approval(
  id number(11) NOT NULL,
  teacher_id number(11) NOT NULL,
  admin_id number(11) DEFAULT NULL,  
  approved_date date DEFAULT NULL,
  foreign key (teacher_id) references teacher,
  foreign key (admin_id) references users,  
  PRIMARY KEY(id)
);