<?php 
class Quiz_model extends CI_Model {
        
    const TABLENAME = 'QUIZ';
    const SEQUENCENAME = 'QUIZ_SEQ';
    
    
        
    function __construct()
    {
        parent::__construct();
    }
    
    function getQuizById($quiz_id,$course_id) {
        $sql = "SELECT * FROM ".constant('quiz_model::TABLENAME')
            ." WHERE ID = ".$quiz_id
            ." AND COURSE_ID=".$course_id;
        $query = $this->db->query($sql);
        if ($query->num_rows() == 1) return $query->row();
        return NULL;
    }
    
    function getQuizesOfCourse($course_id) {
        $sql = "SELECT * FROM ".constant('quiz_model::TABLENAME')
            ." WHERE COURSE_ID=".$course_id;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) return $query->result();
        return NULL;
    }
    
    function getLastInsertedQuizId() {
        $query = $this->db->query("select ".constant('quiz_model::SEQUENCENAME')
                                    .".CURRVAL as LASTID from dual");
        $row = $query->row();
        if(is_null($row)) {
            echo "last inserted id cannot be retrieved";
        }
        return $row->LASTID;
    }
    
    
    function createQuiz($data)
    {               
        return $this->db->insert(constant('quiz_model::TABLENAME'), $data); 
    }
    
    function deleteQuiz($quiz_id) {
        $qry="CALL delete_quiz({$quiz_id})";
        $stmt=oci_parse($this->db->conn_id, $qry);
        return oci_execute($stmt);
        
    }
 /*   function updateLecture($data,$lec_id) {
        $sql = "UPDATE ".constant('lecture_model::TABLENAME')." SET "
            ." TOPIC_NAME=?,LECTURE_OVERVIEW=?,UPDATE_TIME=?"
            ." WHERE ID = ".$lec_id;
        
        $values = array($data['TOPIC_NAME'],$data['LECTURE_OVERVIEW'],$data['UPDATE_TIME']);
        
        return $this->db->query($sql,$values);
        
    }*/
    
}

?>