<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Course_model extends CI_Model
{
	const TABLENAME = 'COURSE';
    const SEQUENCENAME = 'COURSE_SEQ';
	const ID='ID';
	const TITLE='TITLE';
	const PREREQCOURSE='PRE_REQ_COURSE';
	const CATEGORYID ='CATEGORY_ID';
	const STARTDATE='START_DATE';
	const IMAGEPATH='IMAGE_PATH';	
	const SYLLABUS = 'SYLLABUS';
	const COURSEFORMAT='COURSE_FORMAT';
	const DESCRIPTION='DESCRIPTION';
	const ABOUTCOURSE='ABOUT_COURSE';
	const INTROVIDEOPATH='INTRO_VIDEO_PATH';
	const COURSELANGUAGE ='COURSE_LANGUAGE';
	const COURSELENGTHWEEK='COURSE_LENGTH_WEEK';
	const EFFORTPERWEEK='EFFORT_PER_WEEK';
	const ISPUBLISHED='IS_PUBLISHED';
	const COURSEPOSTEDDATE='COURSE_POSTED_DATE';
	private $number_of_course_to_show=10;
	
	function getCourseById($course_id)
	{
		$query = $this->db->query("SELECT * FROM ".constant('course_model::TABLENAME').
		" WHERE ID = ".$course_id);
        //echo $this->db->last_query();
		
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}
    
    function getAllCourses() {
        $query = $this->db->get(constant('course_model::TABLENAME'));
		
        if($query->num_rows()>0) return $query->result();
        return NULL;
    }
	

    function getLastInsertedCourseId() {
        $query = $this->db->query("select ".constant('course_model::SEQUENCENAME')
                                    .".CURRVAL as LASTID from dual");
        $row = $query->row();
        if(is_null($row)) {
            echo "last inserted id cannot be retrieved";
        }
        return $row->LASTID;
    }
    
	function createCourse($data)
    {               
        //return $this->db->insert('COURSE', $data);
        $qry="CALL create_course(
        '".$data['TEACHER_ID']."',
        '".$data['TITLE']."',
        '".$data['PRE_REQ_COURSE']."',
        '".$data['CATEGORY_ID']."',
        '".$data['START_DATE']."',
        '".$data['IMAGE_PATH']."',
        '".$data['SYLLABUS']."',
        '".$data['COURSE_FORMAT']."',
        '".$data['DESCRIPTION']."',
        '".$data['ABOUT_COURSE']."',
        '".$data['INTRO_VIDEO_PATH']."',
        '".$data['COURSE_LANGUAGE']."',
        '".$data['COURSE_LENGTH_WEEK']."',
        '".$data['EFFORT_PER_WEEK']."',
        '".$data['IS_PUBLISHED']."',
        '".$data['COURSE_POSTED_DATE']."'
        )";
        $stmt=oci_parse($this->db->conn_id, $qry);
        return oci_execute($stmt);   
    }
	
	
	function deleteCourse($course_id)
	{
		$this->db->where($this->ID, $course_id);
		$this->db->delete($this->TABLENAME);
		if ($this->db->affected_rows() > 0) {			
			return TRUE;
		}
		return FALSE;
	}
    
    function updateCourse($data) {
        /*$sql = 'UPDATE COURSE SET TITLE=?,
            PRE_REQ_COURSE=?,CATEGORY_ID=?,
            START_DATE=?,
            SYLLABUS=?,COURSE_FORMAT=?,
            DESCRIPTION=?,ABOUT_COURSE=?,
            COURSE_LANGUAGE=?,COURSE_LENGTH_WEEK=?,
            EFFORT_PER_WEEK=?,IS_PUBLISHED=? 
             WHERE ID = '.$course_id;
        $values = array($data['TITLE'],$data['PRE_REQ_COURSE'],
                    $data['CATEGORY_ID'],$data['START_DATE'],$data['SYLLABUS'],
                    $data['COURSE_FORMAT'],$data['DESCRIPTION'],$data['ABOUT_COURSE'],$data['COURSE_LANGUAGE'],
                    $data['COURSE_LENGTH_WEEK'],$data['EFFORT_PER_WEEK'],$data['IS_PUBLISHED']);
        
        return $this->db->query($sql,$values); */
        
        $qry="CALL update_course(
        '".$data['COURSE_ID']."',
        '".$data['TITLE']."',
        '".$data['PRE_REQ_COURSE']."',
        '".$data['CATEGORY_ID']."',
        '".$data['START_DATE']."',
        '".$data['SYLLABUS']."',
        '".$data['COURSE_FORMAT']."',
        '".$data['DESCRIPTION']."',
        '".$data['ABOUT_COURSE']."',
        '".$data['COURSE_LANGUAGE']."',
        '".$data['COURSE_LENGTH_WEEK']."',
        '".$data['EFFORT_PER_WEEK']."',
        '".$data['IS_PUBLISHED']."'
        )";
        $stmt=oci_parse($this->db->conn_id, $qry);
        return oci_execute($stmt);   
        
    }
	
	function getPastCourses() {
        $sql = "SELECT * FROM ".constant('course_model::TABLENAME')." WHERE ".constant('course_model::STARTDATE'). " < '".date('d-M-y')."'AND ROWNUM < ".$this->number_of_course_to_show ;
		
		$query= $this->db->query($sql);				
		//echo $this->db->last_query();
        if($query->num_rows()>0) return $query->result();
        return NULL;
    }
	function getNewCourses() {
        $sql = "SELECT * FROM ".constant('course_model::TABLENAME')." WHERE ".constant('course_model::STARTDATE'). " > '".date('d-M-y')."'AND ROWNUM < ".$this->number_of_course_to_show ;
		$query= $this->db->query($sql);			
        if($query->num_rows()>0) return $query->result();
        return NULL;
    }	
	
	function getCourseByCategoryGroup() {
        //$sql = "SELECT COUNT(*) AS CATEGORY_NUMBER, ".constant('course_model::CATEGORYID')."  FROM ".constant('course_model::TABLENAME')." GROUP BY ".constant('course_model::CATEGORYID') ;
		$sql="SELECT C.CATEGORY_ID,CAT.NAME,COUNT(*) AS CATEGORY_NUMBER FROM CATEGORY CAT,COURSE C WHERE C.CATEGORY_ID=CAT.ID GROUP BY C.CATEGORY_ID,CAT.NAME";
		$query= $this->db->query($sql);		
		//echo $this->db->last_query();
        if($query->num_rows()>0) return $query->result();
        return NULL;
    }
	

	function getTenCourseByCategoryGroup() {
        //$sql = "SELECT COUNT(*) AS CATEGORY_NUMBER, ".constant('course_model::CATEGORYID')."  FROM ".constant('course_model::TABLENAME')." GROUP BY ".constant('course_model::CATEGORYID') ;
		$sql="SELECT C.CATEGORY_ID,CAT.NAME,COUNT(*) AS CATEGORY_NUMBER FROM CATEGORY CAT,COURSE C WHERE C.CATEGORY_ID=CAT.ID AND ROWNUM <".$this->number_of_course_to_show." GROUP BY C.CATEGORY_ID,CAT.NAME";
		$query= $this->db->query($sql);		
		//echo $this->db->last_query();
        if($query->num_rows()>0) return $query->result();
        return NULL;
    }
	
	function getCourseByCategory($category_id) {
        $sql = "SELECT * FROM ".constant('course_model::TABLENAME')." WHERE ".constant('course_model::CATEGORYID'). " = ".$category_id ;		
		$query= $this->db->query($sql);		
		//echo $this->db->last_query();
        if($query->num_rows()>0) return $query->result();
        return NULL;
    }
	function getRelatedCourses($category_id,$course_id) {
        $sql = "SELECT * FROM ".constant('course_model::TABLENAME')." WHERE ".constant('course_model::CATEGORYID'). " = ".$category_id." AND ID !=  ".$course_id ;		
		$query= $this->db->query($sql);		
		//echo $this->db->last_query();
        if($query->num_rows()>0) return $query->result();
        return NULL;
    }
	
	function getCourseSearchByTitle($title)
	{
		$sql = "SELECT * FROM ".constant('course_model::TABLENAME')." WHERE ".constant('course_model::TITLE'). " LIKE '%".$title."%'" ;		
		$query= $this->db->query($sql);		
		//echo $this->db->last_query();
        if($query->num_rows()>0) return $query->result();
        return NULL;
	}
	function getCourseSearchByTeacher($teacher_name)
	{
		$sql = "SELECT C.COURSE_POSTED_DATE,C.TITLE,C.ID,C.DESCRIPTION FROM COURSE C,COURSE_TEACHER CT,USERS U WHERE CT.COURSE_ID=C.ID AND U.ID=CT.TEACHER_ID AND  U.FULL_NAME LIKE '%".$teacher_name."%' ";
		$query= $this->db->query($sql);		
		//echo $this->db->last_query();
        if($query->num_rows()>0) return $query->result();
        return NULL;
	}
	function getCourseSearchByCategory($category_name)
	{
		$sql = "SELECT C.COURSE_POSTED_DATE,C.TITLE,C.ID,C.ID,C.DESCRIPTION,C.IMAGE_PATH  FROM COURSE C,CATEGORY CAT WHERE CAT.ID=C.CATEGORY_ID AND  CAT.NAME LIKE '%".$category_name."%' ";
		$query= $this->db->query($sql);		
		//echo $this->db->last_query();
        if($query->num_rows()>0) return $query->result();
        return NULL;
	}
	function getCourseByMonth()
	{
		$sql= "SELECT TO_CHAR(COURSE_POSTED_DATE, 'YYYY-MM') AS MONTH_NAME, COUNT(*) AS MONTH_COURSE_NUMBER FROM COURSE GROUP BY TO_CHAR(COURSE_POSTED_DATE, 'YYYY-MM') ORDER BY 1";
		$query= $this->db->query($sql);		
		//echo $this->db->last_query();
        if($query->num_rows()>0) return $query->result();
        return NULL;
	}
	//SELECT C.TITLE,C.ID,C.ID,C.DESCRIPTION,C.IMAGE_PATH  FROM COURSE C,CATEGORY CAT WHERE CAT.ID=C.CATEGORY_ID AND  CAT.NAME LIKE '%Algo%';
	
}