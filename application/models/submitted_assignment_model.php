<?php
class Submitted_assignment_model extends CI_Model {
        
    const TABLENAME = 'SUBMITTED_ASSIGNMENT';
    const ID='ID';
    const ASSIGNMENT_ID='ASSIGNMENT_ID';
	const STUDENT_ID = 'STUDENT_ID';
    const SUBMITTED_DATE='SUBMITTED_DATE';
    const CONTENT_PATH='CONTENT_PATH';
	
	/*
	CREATE TABLE assignment(
  id number (11) NOT NULL,
  course_id number (11) NOT NULL,  
  start_date date ,
  end_date date ,
  title varchar2(50) NOT NULL,   
  text varchar2(1500) DEFAULT NULL,  
  content_path varchar2(100) DEFAULT NULL,    
  PRIMARY KEY (id)
);
*/
    
    
        
    function __construct()
    {
        parent::__construct();
    }
    
    function insertSubmittedAssignment($data) {        
        return $this->db->insert('SUBMITTED_ASSIGNMENT', $data); 
    }
	
	function getAllSubmittedAssignments($course_id)
	{
		$sql="SELECT SA.STUDENT_ID,SA.CONTENT_PATH,U.FULL_NAME FROM USERS U,SUBMITTED_ASSIGNMENT SA,ASSIGNMENT A WHERE A.ID=SA.ASSIGNMENT_ID AND U.ID=SA.STUDENT_ID AND COURSE_ID = ".$course_id;
		$query=$this->db->query($sql);
		echo $this->db->last_query();
		 if($query->num_rows()>0) {			
			return $query->result();
		}
        return NULL;
	}
	
	

}

?>