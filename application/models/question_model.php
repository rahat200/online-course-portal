<?php 
class Question_model extends CI_Model {
        
    const TABLENAME = 'QUESTION';
    const SEQUENCENAME = 'QUES_SEQ';
    
    
        
    function __construct()
    {
        parent::__construct();
    }
    
    function getQuestionById($ques_id,$quiz_id) {
        $sql = "SELECT * FROM ".constant('question_model::TABLENAME')
            ." WHERE ID = ".$ques_id
            ." AND QUIZ_ID=".$quiz_id;
        $query = $this->db->query($sql);
        if ($query->num_rows() == 1) return $query->row();
        return NULL;
    }
    
    function getQuestionsOfQuiz($quiz_id) {
        $sql = "SELECT * FROM ".constant('question_model::TABLENAME')
            ." WHERE QUIZ_ID=".$quiz_id;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) return $query->result();
        return NULL;
    }
    
    function getLastInsertedQuestionId() {
        $query = $this->db->query("select ".constant('question_model::SEQUENCENAME')
                                    .".CURRVAL as LASTID from dual");
        $row = $query->row();
        if(is_null($row)) {
            echo "last inserted id cannot be retrieved";
        }
        return $row->LASTID;
    }
    
    function getCorrectChoiceId($qsn_id) {
        $sql = "SELECT * FROM ".constant('question_model::TABLENAME')
            ." WHERE ID=".$qsn_id;
        $query = $this->db->query($sql);
        //echo $this->db->last_query();
        //var_dump($query->result());
        if ($query->num_rows() == 1) {$res = $query->result(); return $res[0]->ANSWER_ID;} 
        return NULL;
    }
    
    
    function createQuestion($data)
    {               
        return $this->db->insert(constant('question_model::TABLENAME'), $data); 
    }
    
    function deleteQuestion($ques_id) {
        $sql = "DELETE FROM ".constant('question_model::TABLENAME')
            ." WHERE ID = ".$ques_id;
        $this->db->query($sql);
        if ($this->db->affected_rows() > 0) {           
            return TRUE;
        }
        return FALSE;
        
    }
    
    function updateChoiceId($ques_id,$choice_id) {
        $sql = "UPDATE ".constant('question_model::TABLENAME')." SET "
            ." ANSWER_ID=".$choice_id
            ." WHERE ID = ".$ques_id;
        
        return $this->db->query($sql);
        
    }

    
}

?>