<?php 
class Choice_model extends CI_Model {
        
    const TABLENAME = 'CHOICE';
    const SEQUENCENAME = 'CHOICE_SEQ';
    
    
        
    function __construct()
    {
        parent::__construct();
    }
    
    function getChoiceById($choice_id) {
        $sql = "SELECT * FROM ".constant('choice_model::TABLENAME')
            ." WHERE ID = ".$choice_id;
           
        $query = $this->db->query($sql);
        if ($query->num_rows() == 1) return $query->row();
        return NULL;
    }
    
    function getChoicesOfQuestion($ques_id) {
        $sql = "SELECT * FROM ".constant('choice_model::TABLENAME')
            ." WHERE QUES_ID=".$ques_id;
        $query = $this->db->query($sql);
        if ($query->num_rows() >0 ) return $query->result();
        return NULL;
    }
    
    
    function createChoice($data)
    {               
        return $this->db->insert(constant('choice_model::TABLENAME'), $data); 
    }
    
   
    function getLastInsertedChoiceId() {
        $query = $this->db->query("select ".constant('choice_model::SEQUENCENAME')
                                    .".CURRVAL as LASTID from dual");
        $row = $query->row();
        if(is_null($row)) {
            echo "last inserted id cannot be retrieved";
        }
        return $row->LASTID;
    }
    

    
}

?>