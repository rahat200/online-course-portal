<?php
class Assignment_model extends CI_Model {
        
    const TABLENAME = 'ASSIGNMENT';
    const ID='ID';
    const COURSE_ID='COURSE_ID';
	const START_DATE = 'START_DATE';
    const END_DATE='END_DATE';
    const TITLE='TITLE';
	const TEXT = 'TEXT';
    const CONTENT_PATH='CONTENT_PATH';
	
	/*
	CREATE TABLE assignment(
  id number (11) NOT NULL,
  course_id number (11) NOT NULL,  
  start_date date ,
  end_date date ,
  title varchar2(50) NOT NULL,   
  text varchar2(1500) DEFAULT NULL,  
  content_path varchar2(100) DEFAULT NULL,    
  PRIMARY KEY (id)
);
*/
    
    
        
    function __construct()
    {
        parent::__construct();
    }
    
    function getAssignmentByID($ass_id) {
        $this->db->where($this->ID, $ass_id);
        $query = $this->db->get(constant('assignment_model::TABLENAME'));
        if ($query->num_rows() == 1) return $query->row();
        return NULL;
    }
    

    
    function insertAssignment($data) {        
        return $this->db->insert('ASSIGNMENT', $data); 
    }
	
	function getAllAssignments($course_id)
	{
		$sql= "SELECT * FROM ASSIGNMENT WHERE COURSE_ID=".$course_id;
		$query= $this->db->query($sql);
		if($query->num_rows()>0) {			
			return $query->result();
		}
        return NULL;
	}

}

?>