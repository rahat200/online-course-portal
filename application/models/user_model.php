<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class User_model extends CI_Model
{

	const TABLENAME = 'USERS';
	const ID='ID';
	const USERNAME='USERNAME';
	const PASSWORD='PASSWORD';
	const EMAIL ='EMAIL';
	const FULLNAME='FULL_NAME';
	const USERROLE='USER_ROLE';
    
	function getUserByID($user_id)
	{
		$sql = "SELECT * FROM ".constant('user_model::TABLENAME')." WHERE ".constant('user_model::ID'). " = ".$user_id ;		
		$query= $this->db->query($sql);				
        if($query->num_rows()==1) return $query->row();
        return NULL;
	}
	
	function getUserByUsername($username)
	{
		$this->db->where('LOWER('.$this->USERNAME.')=', strtolower($username));

		$query = $this->db->get($this->TABLENAME);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}
	
	function getUserByEmail($email)
	{
		$this->db->where('LOWER('.$this->EMAIL.')=', strtolower($email));

		$query = $this->db->get($this->TABLENAME);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}
	
	function getUserByLogin($login)
	{
		$this->db->where('LOWER('.$this->USERNAME.')=', strtolower($login));
		$this->db->or_where('LOWER('.$this->EMAIL.')=', strtolower($login));

		$query = $this->db->get($this->TABLENAME);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}
	
	function isUsernameAvailable($username)
	{
		//$this->db->select('1', FALSE);
		//$this->db->where('LOWER('.$this->USERNAME.')=', strtolower($username));
		//$query = $this->db->get($this->TABLENAME);
		$sql = "select * from ".constant('user_model::TABLENAME')." where ".constant('user_model::USERNAME')." = '".strtolower($username)."'";
		$query=$this->db->query($sql);
		return $query->num_rows() == 0;
	}
	
	function isEmailAvailable($email)
	{
		//$this->db->select('1', FALSE);
		//$this->db->where('LOWER(email)=', strtolower($email));	
		//$query = $this->db->get(constant('user_model::TABLENAME'));
		$sql = "select * from ".constant('user_model::TABLENAME')." where ".constant('user_model::EMAIL')." = '".strtolower($email)."'";
		$query = $this->db->query($sql);
		
		return $query->num_rows() == 0;
	}	
	function deleteUser($user_id)
	{
		$this->db->where($this->ID, $user_id);
		$this->db->delete($this->TABLENAME);
		if ($this->db->affected_rows() > 0) {			
			return TRUE;
		}
		return FALSE;
	}
	function createUser($data)
	{				
		//return $this->db->insert(constant('user_model::TABLENAME'), $data);
		$qry="CALL create_user(
		'".$data['USERNAME']."',
		'".$data['PASSWORD']."',
		'".$data['EMAIL']."',
		'".$data['FULL_NAME']."',
		'".$data['USER_ROLE']."'
        )";
        $stmt=oci_parse($this->db->conn_id, $qry);
        return oci_execute($stmt); 
	}
    
	function getUserLogin($email,$password)
	{
		$sql= "select * from ".constant('user_model::TABLENAME')." where ".constant('user_model::EMAIL')." = '".strtolower($email)."' AND ".constant('user_model::PASSWORD')." ='".$password."'";
		$query=$this->db->query($sql);
		$user_array = array();
		if($query->num_rows() == 1)
		{
			$user_array = $query->row();
		}	
		return $user_array;
	}
	function getUnregUserStatistics()
	{
		$sql = "SELECT  COUNT(U.ID) AS USER_NUMBER,U.USER_ROLE FROM USERS U WHERE NOT EXISTS (SELECT S.STUDENT_ID FROM REGISTERED_STUDENT S WHERE U.ID=S.STUDENT_ID) GROUP BY U.USER_ROLE";
		$query=$this->db->query($sql);
		if($query->num_rows()>0)
			return $query->result();
		else 
			return null;
	}
	function updateUserInfo($full_name, $email, $id)
	{
		$sql = "UPDATE USERS SET FULL_NAME =". $full_name." , EMAIL= '".$email."' where ID=".$id;
		$query=$this->db->query($sql);
		
		
	}
	
}