<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Course extends CI_Model
{

	public $course_id,$title,$pre_req_course ,$category_id ,$start_date ,$image_path,$syllabus,$course_format  ,$description  ,$intro_video_path ,$course_language,$course_length_week ,$effort_per_week   ,$is_published   ,$course_posted_date ;
	
    public function __construct()
	{
		parent::__construct();		
	} 		
	public function initialise($course_id,$title,$pre_req_course ,$category_id ,$start_date ,$image_path,$syllabus,$course_format  ,$description 
	,$intro_video_path ,$course_language,$course_length_week ,$effort_per_week   ,$is_published   ,$course_posted_date)
	{
		$this->course_id=$course_id;
		$this->title=$title;
		$this->pre_req_course=$pre_req_course;
		$this->category_id=$category_id;
		$this->start_date=$start_date;		
		$this->image_path=$image_path;
		$this->syllabus=$syllabus;
		$this->course_format=$course_format;
		$this->description=$description;
		$this->intro_video_path=$intro_video_path;
		$this->course_language=$course_language;		
		$this->course_length_week=$course_length_week;
		$this->effort_per_week=$effort_per_week;
		$this->is_published=$is_published;		
		$this->course_posted_date=$course_posted_date;		
	}
	public function getCourseID()
	{
		return $this->course_id;
	}
	public function getTitle()
	{
		return $this->title;
	}
	public function getPreReqCourse()
	{
		return $this->pre_req_course;
	}
	public function getCategoryID()
	{
		return $this->category_id;
	}
	public function getStartDate()
	{
		return $this->start_date;
	}
	public function getImagePath()
	{
		return $this->image_path;
	}
	public function getSyllabus()
	{
		return $this->syllabus;
	}
	public function getCourseFormat()
	{
		return $this->course_format;
	}
	public function getDescription()
	{
		return $this->description;
	}
	public function getIntroVideoPath()
	{
		return $this->intro_video_path;
	}
	public function getCourseLanguage()
	{
		return $this->course_language;
	}
	public function getCourseLengthWeek()
	{
		return $this->course_length_week;
	}	
	public function getEffortPerWeek()
	{
		return $this->effort_per_week;
	}
	public function getIsPublished()
	{
		return $this->is_published;
	}
	public function getCoursePostedDate()
	{
		return $this->course_posted_date;
	}
	
	public function __destruct(){} 
    public function __toString()
    {        
        return $this->course_id;
    } 
	
}