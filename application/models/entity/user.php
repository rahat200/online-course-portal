<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class User extends CI_Model
{

	public $user_id,$username,$password,$email,$full_name,$user_role;
	
    public function __construct()
	{
		parent::__construct();		
	} 		
	public function initialise($user_id,$username,$password,$email,$full_name,$user_role)
	{
		$this->user_id=$user_id;
		$this->username=$username;
		$this->password=$password;
		$this->email=$email;
		$this->full_name=$full_name;		
		$this->user_role=$user_role;
	}
	public function getUserID()
	{
		return $this->user_id;
	}
	public function getUsername()
	{
		return $this->username;
	}
	public function getPassword()
	{
		return $this->password;
	}
	public function getEmail()
	{
		return $this->email;
	}
	public function getFullName()
	{
		return $this->full_name;
	}
	public function getUserRole()
	{
		return $this->user_role;
	}
	
	public function __destruct(){} 
    public function __toString()
    {        
        return $this->user_id;
    } 
	
}