<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Course_teacher_model extends CI_Model
{
	const TABLENAME = 'COURSE_TEACHER';
	const ID='ID';
	const TEACHER_ID='TEACHER_ID';
	const COURSE_ID='COURSE_ID';	

	function getAllCourses($teacher_id)
	{
		$sql = "SELECT * FROM ".constant('course_teacher_model::TABLENAME')
		. " WHERE ".constant('course_teacher_model::TEACHER_ID')." = ".$teacher_id;
		$query=$this->db->query($sql);		
		echo $query->num_rows();
		if($query->num_rows()>0)		
		{			
			return $query->result();			
		}
		else
			return NULL;
	}
	
	function getCourseTeacherID($course_id)
	{
		$sql = "SELECT ".constant('course_teacher_model::TEACHER_ID')
		." FROM ".constant('course_teacher_model::TABLENAME'). " WHERE ".constant('course_teacher_model::COURSE_ID')." = ".$course_id;
		$query=$this->db->query($sql);				
		if($query->num_rows()>0)		
		{			
			return $query->row();			
		}
		else
			return NULL;
	}
	
    function insert($teacher_id,$course_id) {
        $sql = "INSERT INTO ".constant('course_teacher_model::TABLENAME')
            ." VALUES ({$teacher_id},{$course_id})";
        return $this->db->query($sql);
    }
	/*
	function updateTeacherApproval($teacher_id)
	{
		//already replace by trigger teacher_approved		
		$sql= " UPDATE ".constant('teacher_model::TABLENAME')." SET ".constant('teacher_model::IS_APPROVED')." = ".1. " WHERE ".constant('teacher_model::ID')." = ".$teacher_id;
		$this->db->query($sql);		
		
	}
	*/
}

