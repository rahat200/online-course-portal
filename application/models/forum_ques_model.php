<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Forum_ques_model extends CI_Model
{
	const TABLENAME = 'FORUM_QUESTION';
    const SEQUENCENAME = 'FORUMQUESTION_SEQ';
	const ID='ID';
	const USER_ID='USER_ID';
	const TEXT='TEXT';
	const LECTURE_ID ='LECTURE_ID';
	const POSTED_DATE='POSTED_DATE';
	
	private $number_of_course_to_show=6;
	
	function getAllForumQuestionsByLectureId($lecture_id)
	{		
		$sql="SELECT ROWNUM,U.FULL_NAME,FQ.ID,FQ.USER_ID,FQ.TEXT,FQ.POSTED_DATE,FQ.LECTURE_ID FROM COURSE C,FORUM_QUESTION FQ,LECTURE LEC,USERS U WHERE U.ID=FQ.USER_ID AND LEC.COURSE_ID=C.ID AND LEC.ID=FQ.LECTURE_ID AND LEC.ID=".$lecture_id." ORDER BY FQ.POSTED_DATE DESC";
        //echo $this->db->last_query();		
		$query=$this->db->query($sql);
		if ($query->num_rows() > 0 ) return $query->result();
		return NULL;
	}
	//$sql="SELECT LEC.ID FROM FORUM_QUESTION FQ,LECTURE LEC WHERE LEC.ID=FQ.LECTURE_ID AND LEC.ID=".$
	function getAllForumQuestionsByCourseId($course_id)
	{		
		$sql="SELECT ROWNUM,U.FULL_NAME,FQ.ID,FQ.USER_ID,FQ.TEXT,FQ.POSTED_DATE,FQ.LECTURE_ID FROM COURSE C,FORUM_QUESTION FQ,LECTURE LEC,USERS U WHERE U.ID=FQ.USER_ID AND LEC.COURSE_ID=C.ID AND LEC.ID=FQ.LECTURE_ID AND C.ID=".$course_id." ORDER BY FQ.POSTED_DATE DESC";
		$query=$this->db->query($sql);
        //echo $this->db->last_query();		
		if ($query->num_rows() > 0 ) return $query->result();
		return NULL;
	}
	
	function createNewQuestion($data)
	{
		//return $this->db->insert(constant('forum_ques_model::TABLENAME'), $data); 
		$qry="CALL forum_new_question(
        '".$data['USER_ID']."',
        '".$data['TEXT']."',
        '".$data['LECTURE_ID']."',
        '".$data['POSTED_DATE']."'
        )";
        $stmt=oci_parse($this->db->conn_id, $qry);
        return oci_execute($stmt);  
	}
	
	/*
    function getAllCourses() {
        $query = $this->db->get(constant('course_model::TABLENAME'));
		
        if($query->num_rows()>0) return $query->result();
        return NULL;
    }
	

    function getLastInsertedCourseId() {
        $query = $this->db->query("select ".constant('course_model::SEQUENCENAME')
                                    .".CURRVAL as LASTID from dual");
        $row = $query->row();
        if(is_null($row)) {
            echo "last inserted id cannot be retrieved";
        }
        return $row->LASTID;
    }
    
	function createCourse($data)
    {               
        return $this->db->insert('COURSE', $data); 
    }
	
	
	function deleteCourse($course_id)
	{
		$this->db->where($this->ID, $course_id);
		$this->db->delete($this->TABLENAME);
		if ($this->db->affected_rows() > 0) {			
			return TRUE;
		}
		return FALSE;
	}
    
    function updateCourse($data,$course_id) {
        $sql = 'UPDATE COURSE SET TITLE=?,
            PRE_REQ_COURSE=?,CATEGORY_ID=?,
            START_DATE=?,
            SYLLABUS=?,COURSE_FORMAT=?,
            DESCRIPTION=?,ABOUT_COURSE=?,
            COURSE_LANGUAGE=?,COURSE_LENGTH_WEEK=?,
            EFFORT_PER_WEEK=?,IS_PUBLISHED=? 
             WHERE ID = '.$course_id;
        $values = array($data['TITLE'],$data['PRE_REQ_COURSE'],
                    $data['CATEGORY_ID'],$data['START_DATE'],$data['SYLLABUS'],
                    $data['COURSE_FORMAT'],$data['DESCRIPTION'],$data['ABOUT_COURSE'],$data['COURSE_LANGUAGE'],
                    $data['COURSE_LENGTH_WEEK'],$data['EFFORT_PER_WEEK'],$data['IS_PUBLISHED']);
        
        return $this->db->query($sql,$values);
        
    }
	
	function getPastCourses() {
        $sql = "SELECT * FROM ".constant('course_model::TABLENAME')." WHERE ".constant('course_model::STARTDATE'). " < '".date('d-M-y')."'AND ROWNUM < ".$this->number_of_course_to_show ;
		
		$query= $this->db->query($sql);				
		//echo $this->db->last_query();
        if($query->num_rows()>0) return $query->result();
        return NULL;
    }
	function getNewCourses() {
        $sql = "SELECT * FROM ".constant('course_model::TABLENAME')." WHERE ".constant('course_model::STARTDATE'). " > '".date('d-M-y')."'AND ROWNUM < ".$this->number_of_course_to_show ;
		$query= $this->db->query($sql);			
        if($query->num_rows()>0) return $query->result();
        return NULL;
    }	
	
	function getCourseByCategoryGroup() {
        $sql = "SELECT COUNT(*) AS CATEGORY_NUMBER, ".constant('course_model::CATEGORYID')."  FROM ".constant('course_model::TABLENAME')." GROUP BY ".constant('course_model::CATEGORYID') ;
		$query= $this->db->query($sql);				
        if($query->num_rows()>0) return $query->result();
        return NULL;
    }
	
	function getCourseByCategory($category_id) {
        $sql = "SELECT * FROM ".constant('course_model::TABLENAME')." WHERE ".constant('course_model::CATEGORYID'). " = ".$category_id ;		
		$query= $this->db->query($sql);		
		//echo $this->db->last_query();
        if($query->num_rows()>0) return $query->result();
        return NULL;
    }
	
	function getCourseSearchByTitle($title)
	{
		$sql = "SELECT * FROM ".constant('course_model::TABLENAME')." WHERE ".constant('course_model::TITLE'). " LIKE '%".$title."%'" ;		
		$query= $this->db->query($sql);		
		//echo $this->db->last_query();
        if($query->num_rows()>0) return $query->result();
        return NULL;
	}
	function getCourseSearchByTeacher($teacher_name)
	{
		$sql = "SELECT C.TITLE,C.ID FROM COURSE C,COURSE_TEACHER CT,USERS U WHERE CT.COURSE_ID=C.ID AND U.ID=CT.TEACHER_ID AND  U.FULL_NAME LIKE '%".$teacher_name."%' ";
		$query= $this->db->query($sql);		
		//echo $this->db->last_query();
        if($query->num_rows()>0) return $query->result();
        return NULL;
	}
	function getCourseSearchByCategory($category_name)
	{
		$sql = "SELECT C.TITLE,C.ID,C.ID,C.DESCRIPTION,C.IMAGE_PATH  FROM COURSE C,CATEGORY CAT WHERE CAT.ID=C.CATEGORY_ID AND  CAT.NAME LIKE '%".$category_name."%' ";
		$query= $this->db->query($sql);		
		//echo $this->db->last_query();
        if($query->num_rows()>0) return $query->result();
        return NULL;
	}
	//SELECT C.TITLE,C.ID,C.ID,C.DESCRIPTION,C.IMAGE_PATH  FROM COURSE C,CATEGORY CAT WHERE CAT.ID=C.CATEGORY_ID AND  CAT.NAME LIKE '%Algo%';
	*/
	
}