<?php 
class Lecture_model extends CI_Model {
        
    const TABLENAME = 'LECTURE';
    const SEQUENCENAME = 'LECTURE_SEQ';
    const ID='ID';
    const TOPIC_NAME='TOPIC_NAME';
    const COURSE_ID='COURSE_ID';
    const WEEK_NUM='WEEK_NUM';
    const LECTURE_OVERVIEW='LECTURE_OVERVIEW';
    const UPDATE_TIME = 'UPDATE_TIME';
    
    
        
    function __construct()
    {
        parent::__construct();
    }
    
    function getLectureById($lec_id,$course_id) {
        $sql = "SELECT * FROM ".constant('lecture_model::TABLENAME')
            ." WHERE ID = ".$lec_id
            ." AND COURSE_ID=".$course_id;
        $query = $this->db->query($sql);
        if ($query->num_rows() == 1) return $query->row();
        return NULL;
    }
    
    function getLastInsertedLectureId() {
        $query = $this->db->query("select ".constant('lecture_model::SEQUENCENAME')
                                    .".CURRVAL as LASTID from dual");
        $row = $query->row();
        if(is_null($row)) {
            echo "last inserted id cannot be retrieved";
        }
        return $row->LASTID;
    }
    
    function getLecturesOfWeek($course_id,$week_num) {
        $sql = "SELECT * FROM ".constant('lecture_model::TABLENAME')
            ." WHERE WEEK_NUM = ".$week_num
            ." AND COURSE_ID = ".$course_id;
        
        $query = $this->db->query($sql); 
        //echo $this->db->last_query();                   
        if($query->num_rows()>0) {          
            return $query->result();
        }
        return NULL;
    }
    
    function createLecture($data)
    {               
        //return $this->db->insert(constant('lecture_model::TABLENAME'), $data); 
        $qry="CALL create_lecture(
        '".$data['TOPIC_NAME']."',
        '".$data['COURSE_ID']."',
        '".$data['WEEK_NUM']."',
        '".$data['LECTURE_OVERVIEW']."',
        '".$data['UPDATE_TIME']."',
        '".$data['MATERIAL_TYPE']."',
        '".$data['CONTENT_PATH']."'
        )";
        $stmt=oci_parse($this->db->conn_id, $qry);
        return oci_execute($stmt);  
    }
    
    function deleteLecture($lec_id) {
        /*    
        $sql = "DELETE FROM ".constant('lecture_model::TABLENAME')
            ." WHERE ID = ".$lec_id;
        $this->db->query($sql);
        if ($this->db->affected_rows() > 0) {           
            return TRUE;
        }
        return FALSE; */ 
        $qry="CALL delete_lecture({$lec_id})";
        $stmt=oci_parse($this->db->conn_id, $qry);
        return oci_execute($stmt);  
        
    }
    function updateLecture($data) {
        /*
        $sql = "UPDATE ".constant('lecture_model::TABLENAME')." SET "
            ." TOPIC_NAME=?,LECTURE_OVERVIEW=?,UPDATE_TIME=?"
            ." WHERE ID = ".$lec_id;
        
        $values = array($data['TOPIC_NAME'],$data['LECTURE_OVERVIEW'],$data['UPDATE_TIME']);
        
        return $this->db->query($sql,$values); */
        $qry="CALL update_lecture(
        '".$data['LECTURE_ID']."',
        '".$data['TOPIC_NAME']."',
        '".$data['LECTURE_OVERVIEW']."',
        '".$data['UPDATE_TIME']."'
        )";
        $stmt=oci_parse($this->db->conn_id, $qry);
        return oci_execute($stmt);  
        
        
    }
    function getWeeklyLectureCount($course_id) {
        $sql = "SELECT WEEK_NUM,count(*) as LEC_COUNT from ".constant('lecture_model::TABLENAME')
         ." WHERE COURSE_ID = ".$course_id 
         ." GROUP BY WEEK_NUM";
         $query = $this->db->query($sql);
         echo $this->db->last_query();
         if($query->num_rows()>0) return $query->result_array();
         return null;
    }
	
	function getLectureVideoPath($lecture_id)
	{
		$sql = "SELECT MAT.CONTENT_PATH FROM  MATERIAL MAT, LECTURE LEC WHERE LEC.ID=MAT.LECTURE_ID AND  
				LEC.ID=".$lecture_id;
        $query = $this->db->query($sql);
		echo $this->db->last_query();
        if ($query->num_rows() == 1) return $query->row();
        return NULL;
	}
	function getAllLectures($course_id)
	{
		$sql= "SELECT * FROM ".constant('lecture_model::TABLENAME')." WHERE COURSE_ID=".$course_id;
		$query=$this->db->query($sql);
	    if ($query->num_rows() >0) return $query->result();
        return NULL;
	}
	
}

?>