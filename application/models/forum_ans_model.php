<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Forum_ans_model extends CI_Model
{
	const TABLENAME = 'FORUM_ANSWER';
    const SEQUENCENAME = 'FORUMANSWER_SEQ';
	const ID='ID';
	const USER_ID='USER_ID';
	const TEXT='TEXT';
	const FORUM_QUESTION_ID ='FORUM_QUESTION_ID';
	const POSTED_DATE='POSTED_DATE';
	
	private $number_of_course_to_show=6;
	
	function getAllForumAnswer($question_id)
	{
		$sql = "SELECT U.FULL_NAME,FA.TEXT,FA.POSTED_DATE FROM USERS U,FORUM_ANSWER FA, FORUM_QUESTION FQ WHERE FQ.ID=FA.FORUM_QUESTION_ID AND U.ID=FA.USER_ID AND FA.FORUM_QUESTION_ID=".$question_id;
		$query=$this->db->query($sql);
    //    echo $this->db->last_query();		
		if ($query->num_rows() > 0 ) return $query->result();
		return NULL;
	}
	function createNewAnswer($data)
	{
		//return $this->db->insert(constant('forum_ans_model::TABLENAME'), $data); 
		$qry="CALL forum_new_answer(
        '".$data['USER_ID']."',
        '".$data['TEXT']."',
        '".$data['FORUM_QUESTION_ID']."',
        '".$data['POSTED_DATE']."'
        )";
        $stmt=oci_parse($this->db->conn_id, $qry);
        return oci_execute($stmt);  
	}
	
}