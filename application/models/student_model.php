<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class  Student_model extends CI_Model
{
	const TABLENAME = 'REGISTERED_STUDENT';    
	const ID='ID';
	const COURSE_ID='COURSE_ID';
	const STUDENT_ID='STUDENT_ID';
	private $number_of_course_to_show=6;
		
	
	function getAllCourse($student_id)
	{
		//$sql= "SELECT ".constant('student_model::COURSE_ID')." FROM ".constant('student_model::TABLENAME')." where ".constant('student_model::STUDENT_ID').;
	
		//SELECT * FROM COURSE WHERE ID = SELECT COURSE_ID FROM REGISTERED_STUDENT WHERE STUDENT_ID=43 ;
		$sql = "SELECT * FROM ".constant('student_model::TABLENAME')." where ".constant('student_model::STUDENT_ID')." = ".$student_id;
		$query=$this->db->query($sql);
		if($query->num_rows()>0)
			return $query->result();
		else 
			return null;
	}
	function registerForCourse($course_id,$student_id)
	{
		//$sql = "INSERT INTO ".constant('student_model::TABLENAME')." VALUES (".$course_id.",".$student_id.")";
		//$this->db->query($sql);
		//procedure
		$qry="CALL register_for_course({$course_id},{$student_id})";
        
        $stmt=oci_parse($this->db->conn_id, $qry);
        return oci_execute($stmt); 
		
	}
	function getMaxRegisteredCourse()
	{
		$sql = "SELECT ".constant('student_model::COURSE_ID')." FROM ".constant('student_model::TABLENAME')." WHERE ROWNUM < ".$this->number_of_course_to_show." GROUP BY ".constant('student_model::COURSE_ID')." ORDER BY COUNT(*) DESC ";
		$query=$this->db->query($sql);
		//echo $this->db->last_query();
		if($query->num_rows()>0)
			return $query->result();
		else 
			return null;	
	}
	function isStudentRegistered($course_id,$student_id)
	{		
		$sql = "SELECT * FROM ".constant('student_model::TABLENAME')." where ".constant('student_model::COURSE_ID')." = ".$course_id." AND ".constant('student_model::STUDENT_ID')." = ".$student_id;
		$query=$this->db->query($sql);		
		return $query->num_rows() == 1 ;
	}
	function getAllRegStudents()
	{
		$sql = "SELECT COUNT(*) AS REG_USER_NUMBER FROM REGISTERED_STUDENT " ;
		$query=$this->db->query($sql);
		//echo $this->db->last_query();
		if($query->num_rows()>0)
			return $query->row();
		else 
			return null;	
	}
	
}
/*
CREATE TABLE registered_student(
  id number (11) NOT NULL,
  course_id number (11) NOT NULL,  
  student_id number (11) NOT NULL,  
  foreign key (course_id) references course,
  foreign key (student_id) references users,
  PRIMARY KEY (id)
);
CREATE TABLE course (
  id number(11) NOT NULL ,  
  title varchar2(100) NOT NULL,
  pre_req_course varchar2(50) DEFAULT NULL,
  category_id number(11)  DEFAULT NULL,  
  start_date date NOT NULL,  
  image_path varchar2(100) DEFAULT NULL,
  syllabus varchar2(200)  DEFAULT NULL,
  course_format varchar2(200)  DEFAULT NULL,
  description varchar2(200)  DEFAULT NULL,
  intro_video_path varchar2(100)  DEFAULT NULL,
  course_language varchar2(50)  NOT NULL,
  course_length_week number(3)  DEFAULT NULL,
  effort_per_week number(3) DEFAULT NULL,
  is_published number (1) DEFAULT NULL,
  course_posted_date date DEFAULT ('1-JAN-80'),  
  PRIMARY KEY (id)
);
*/