<?php
class Category_model extends CI_Model {
        
    const TABLENAME = 'CATEGORY';
    const ID='ID';
    const TITLE='NAME';
    
    
        
    function __construct()
    {
        parent::__construct();
    }
    
    function getCategoryByID($cat_id) {
        $this->db->where($this->ID, $cat_id);
        $query = $this->db->get(constant('category_model::TABLENAME'));
        if ($query->num_rows() == 1) return $query->row();
        return NULL;
    }
    
    function getAllCategories() {
        $query = $this->db->get(constant('category_model::TABLENAME'));						
        if($query->num_rows()>0) {			
			return $query->result();
		}
        return NULL;
    }	
    
    function insertCategory($cat_name) {
        //$this->name = $cat_name; 
        $this->db->query("INSERT into category (name) VALUES ('{$cat_name}')");
        return null;
    }
	function isCategoryAvailable($cat_name)
	{
		$sql="SELECT * FROM CATEGORY  WHERE NAME= '".$cat_name."'";
		$query=$this->db->query($sql);			
		return $query->num_rows() == 0;
	}
}

?>