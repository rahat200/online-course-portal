<?php 
class Material_model extends CI_Model {
        
    const TABLENAME = 'MATERIAL';
    const SEQUENCENAME = 'MATERIAL_SEQ';
    const ID='ID';
    
    
        
    function __construct()
    {
        parent::__construct();
    }
    

    function getLectureVideo($lec_id) {
        $sql = "SELECT * FROM ".constant('material_model::TABLENAME')
            ." WHERE LECTURE_ID = ".$lec_id
            ." AND MATERIAL_TYPE = 'video'";
        $query = $this->db->query($sql);
        if ($query->num_rows() == 1) return $query->row();
        return NULL;
    }
    
    
    function getLectureHandouts($lec_id) {
        $sql = "SELECT * FROM ".constant('material_model::TABLENAME')
            ." WHERE LECTURE_ID = ".$lec_id
            ." AND MATERIAL_TYPE = 'document'";
        
        $query = $this->db->query($sql); 
        //echo $this->db->last_query();                   
        if($query->num_rows()>0) {          
            return $query->result();
        }
        return NULL;
    }
    
    function insertMaterial($data)
    {               
        return $this->db->insert(constant('material_model::TABLENAME'), $data); 
    }
    
    function deleteMaterial($mat_id) {
        $sql = "DELETE FROM ".constant('material_model::TABLENAME')
            ." WHERE ID = ".$mat_id;
        $this->db->query($sql);
        if ($this->db->affected_rows() > 0) {           
            return TRUE;
        }
        return FALSE;
        
    }
    
}

?>