<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Teacher_model extends CI_Model
{
	const TABLENAME = 'TEACHER';
	const ID='ID';
	const PHONE='PHONE';
	const GENDER='GENDER';
	const PICTURE_PATH ='PICTURE_PATH';
	const BIRTH_DATE='BIRTH_DATE';
	const INSTITUTION_NAME='INSTITUTION_NAME';	
	const FIELD_OF_EXPERTISE='FIELD_OF_EXPERTISE';	
	const IS_APPROVED='IS_APPROVED';	
		
	function createTeacher($data)
	{				
		//return $this->db->insert(constant('teacher_model::TABLENAME'), $data);
		$qry="CALL create_teacher(
        '".$data['ID']."',
        '".$data['PHONE']."',
        '".$data['GENDER']."',
        '".$data['PICTURE_PATH']."',
        '".$data['BIRTH_DATE']."',
        '".$data['INSTITUTION_NAME']."',
        '".$data['FIELD_OF_EXPERTISE']."',
        '".$data['IS_APPROVED']."'
        )";
        $stmt=oci_parse($this->db->conn_id, $qry);
        return oci_execute($stmt);				
	}
	function getTeacherDetails($teacher_id)
	{
		$sql = "SELECT * FROM ".constant('teacher_model::TABLENAME')." WHERE ".constant('teacher_model::ID'). " = ".$teacher_id ;		
		$query= $this->db->query($sql);				
        if($query->num_rows()==1) return $query->row();
        return NULL;
	}
	/*
	function updateTeacherApproval($teacher_id)
	{
		//already replace by trigger teacher_approved		
		$sql= " UPDATE ".constant('teacher_model::TABLENAME')." SET ".constant('teacher_model::IS_APPROVED')." = ".1. " WHERE ".constant('teacher_model::ID')." = ".$teacher_id;
		$this->db->query($sql);		
		
	}
	*/
}

