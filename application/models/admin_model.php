<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Admin_model extends CI_Model
{
	const TABLENAME = 'TEACHER_APPROVAL';
	const ID='ID';
	const ADMIN_ID='ADMIN_ID';
	const TEACHER_ID='TEACHER_ID';
	const APPROVED_DATE='APPROVED_DATE';

	function getAllRequest()
	{
		//$sql = "SELECT * FROM ".constant('admin_model::TABLENAME')." ORDER BY ".constant('admin_model::APPROVED_DATE')."  DESC";
		//$query= $this->db->get(constant('admin_model::TABLENAME'));
		$sql = "SELECT * FROM ".constant('admin_model::TABLENAME');
		$query=$this->db->query($sql);	
		if($query->num_rows()>0)
			return $query->result();
		else 
			return NULL;
	}
	function setTeacherApproval($teacher_id,$admin_id)
	{
		$sql = "UPDATE  ".constant('admin_model::TABLENAME')." SET ".constant('admin_model::APPROVED_DATE')." ='".date('d-M-y')."' , ".constant('admin_model::ADMIN_ID')." = ".$admin_id." WHERE ".constant('admin_model::TEACHER_ID')." = ".$teacher_id;
		echo $this->db->last_query();
		$this->db->query($sql);
	}
}

?>