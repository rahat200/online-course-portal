<?php

class Piechart extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->helper('url');        
		$this->load->library('charts');	  
    }

    function index()
    {
		/*
		$percents = array(19,34,13,20,10,4);

		$legend = array('one','two','tree','four','five','six');

		$data['image'] = $this->charts->pieChart(70,$percents,$legend);

		$this->load->view('charts',$data);
		*/
		$config = array(
        'Skew' => 60,
        'SpliceDistance'=>40,
        'SpliceHeight'=>120,
        'TitleFontSize' => 18,
        'LegendFontSize' => 12,
        'LabelFontSize' => 28,
        'PieFontSize' => 20,
        'PieFontName' => 'GeosansLight.ttf',
        'TitleFontName' => 'MankSans.ttf',
        'LegendFontName' => 'tahoma.ttf',
        'LabelFontName' => 'Silkscreen.ttf',
        'TitleBGR' => 250,
        'TitleBGG' => 70,
        'TitleBGB' => 20,
        'TitleFGR' => 0,
        'TitleFGG' => 240,
        'TitleFGB' => 0,
        'ImgR' => 120,
        'ImgG' => 180,
        'ImgB' => 80,
        'BorderR' => 20,
        'BorderG' => 60,
        'BorderB' => 80,
        'LegendR' => 20,
        'LegendG' => 250,
        'LegendB' => 20,
        'LabelBGR' => 0,
        'LabelBGG' => 10,
        'LabelBGB' => 240,
        'LabelFGR' => 250,
        'LabelFGG' => 0,
        'LabelFGB' => 0);

		$percents = array(19,34,13,20,10,4);

		$legend = array('one','two','CodeIgniter is a powerful PHP framework with a very small footprint, built for PHP coders ','four','five','six');

		$title = 'This is a title';

		$bottom_label='bottom label';

		$data['image'] = $this->charts->pieChart(100,$percents,$legend,'',$title,$bottom_label,$config);
		$this->load->view('charts',$data);
    }
}
?>