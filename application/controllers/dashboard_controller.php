<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_controller extends CI_Controller
{
	
	
	function __construct()
	{
		parent::__construct();

		$this->load->helper(array('form', 'url'));		
		$this->load->library('security');			
		$this->load->library('form_validation');
		$this->load->library('charts');	
			
	}	
	function index()
	{			
		echo "Welcome to the secret index";			
	}	
	function admin()
	{
		if($this->session->userdata('ISLOGIN'))
		{
			$data = array(
				'EMAIL' => $this->session->userdata('EMAIL'),
				'USERNAME' => $this->session->userdata('USERNAME'),
				'TAB_TITLE' =>  "Teacher Request",
				'SELECTED' => "TEACHERREQUEST"
			);										
			$this->load->view('dashboard/admin_dashboard/admin_dashboard',$data);
			
			if($this->session->userdata('USERROLE') != 0)//check if the user is authenticate 
			{					
				redirect('home_controller/home');
			}
			if($this->input->get('teacher_id'))
			{
				$teacher_id=$this->input->get('teacher_id');
				$this->load->model('admin_model');
				$admin_id = $this->session->userdata['USERID'];
				$this->admin_model->setTeacherApproval($teacher_id,$admin_id);
				redirect('dashboard_controller/admin');
			}
			else
			{		
				$this->load->model('admin_model');
				$requests=array();
				$requests=$this->admin_model->getAllRequest();		
				if(!is_null($requests))
				{
					foreach ($requests as $request)
					{				
						$this->load->view('dashboard/admin_dashboard/admin_dashboard_wrapper',$request);
					}		
				}
			}
			$this->load->view('dashboard/admin_dashboard/admin_dashboard_wrapper_end');
		}
		else
		{
			redirect('authentication_controller/signin');
		}
	}
	function add_category()
	{
		if($this->session->userdata('ISLOGIN'))
		{
			$data = array(
				'EMAIL' => $this->session->userdata('EMAIL'),
				'USERNAME' => $this->session->userdata('USERNAME'),
				'TAB_TITLE' =>  "Add Category",
				'SELECTED' => "ADDCATEGORY"
			);										
			$this->load->view('dashboard/admin_dashboard/admin_dashboard',$data);
			
			if($this->session->userdata('USERROLE') != 0)//check if the user is authenticate 
			{					
				redirect('home_controller/home');
			}
				
			if($this->input->post())
			{
				$category=$this->input->post('inputCategory');
				$this->load->model('category_model');
				if(!is_null($category))
				{
					$is_available=$this->category_model->isCategoryAvailable($category);
					if($is_available)
					{
						$this->category_model->insertCategory($category);
						$this->load->view('dashboard/admin_dashboard/new_category');
					}
					else
					{
						echo "Category Name exist";
						$this->load->view('dashboard/admin_dashboard/new_category');
					}
				}
			}
			else
			{
				$this->load->view('dashboard/admin_dashboard/new_category');
			}
		}
	}
	
	function report()
	{	
		if($this->session->userdata('ISLOGIN'))
		{
			$data = array(
				'EMAIL' => $this->session->userdata('EMAIL'),
				'USERNAME' => $this->session->userdata('USERNAME'),
				'TAB_TITLE' =>  "View Report",
				'SELECTED' => "REPORT"
			);										
			$this->load->view('dashboard/admin_dashboard/admin_dashboard',$data);
			
			if($this->session->userdata('USERROLE') != 0)//check if the user is authenticate 
			{					
				redirect('home_controller/home');
			}
			
			$config = array(
			'Skew' => 60,
			'SpliceDistance'=>40,
			'SpliceHeight'=>120,
			'TitleFontSize' => 18,
			'LegendFontSize' => 12,
			'LabelFontSize' => 28,
			'PieFontSize' => 20,
			'PieFontName' => 'GeosansLight.ttf',
			'TitleFontName' => 'MankSans.ttf',
			'LegendFontName' => 'tahoma.ttf',
			'LabelFontName' => 'MankSans.ttf',
			'TitleBGR' => 44,
			'TitleBGG' => 62,
			'TitleBGB' => 80,
			'TitleFGR' => 0,
			'TitleFGG' => 240,
			'TitleFGB' => 0,
			'ImgR' => 240,
			'ImgG' => 240,
			'ImgB' => 240,
			'BorderR' => 20,
			'BorderG' => 60,
			'BorderB' => 80,
			'LegendR' => 20,
			'LegendG' => 250,
			'LegendB' => 20,
			'LabelBGR' => 0,
			'LabelBGG' => 10,
			'LabelBGB' => 240,
			'LabelFGR' => 250,
			'LabelFGG' => 0,
			'LabelFGB' => 0);
			
			
			$this->load->model('course_model');
			$this->load->model('user_model');
			$this->load->model('student_model');
			$teacher_unreg_students= $this->user_model->getUnregUserStatistics();
			$reg_students=$this->student_model->getAllRegStudents();
			$categories=$this->course_model->getTenCourseByCategoryGroup();
			$monthly_courses=$this->course_model->getCourseByMonth();
			
			if(!is_null($categories ))
			{
				$category_name=array();
				$course_number_cat = array();
				foreach ($categories as $category)
				{
					array_push($category_name,$category->NAME);
					array_push($course_number_cat,$category->CATEGORY_NUMBER);
				}					
				$title = 'Course Statistics';
				$bottom_label=date('d-M-y');
				//$data['image'] = $this->charts->pieChart(100,$percents,$legend,'',$title,$bottom_label,$config);
				$data['img_course'] = $this->charts->pieChart(100,$course_number_cat,$category_name,'',$title,$bottom_label,$config);
				//$data['image'] = $this->charts->cartesianChart('bar',$category_name,$course_number_cat,500,250);
				
				
			
				if(!is_null($reg_students && $teacher_unreg_students))
				{
					$roll_name=array();
					$user_number = array();
					
					foreach ($teacher_unreg_students as $user)
					{					
						if ($user->USER_ROLE == 1)
						{
							array_push($roll_name,"STUDENT");
							array_push($user_number,$user->USER_NUMBER);
						}
						else if($user->USER_ROLE == 2)
						{
							array_push($roll_name,"TEACHER");					
							array_push($user_number,$user->USER_NUMBER);							
						}	
					 	
					}
					
					
					array_push($roll_name,"REGISTERED STUDENT");						
					array_push($user_number,$reg_students->REG_USER_NUMBER);
					
					$title = 'User Statistics';
					$data['img_user'] = $this->charts->pieChart(100,$user_number,$roll_name,'',$title,$bottom_label,$config);					
				}
				else
				{
					echo "Problem in Data Fetching Problem";
				}				
				if(!is_null($monthly_courses))
				{
					$month_name=array();
					$monthly_course_number = array();
					foreach($monthly_courses as $m_course)
					{
						array_push($month_name,$m_course->MONTH_NAME);
						array_push($monthly_course_number,$m_course->MONTH_COURSE_NUMBER);
					}
				}
					
				$title = 'Monthly Course';
				$data['img_monthly_course'] = $this->charts->cartesianChart('bar',$month_name,$monthly_course_number,500,250);
				//$data['img_monthly_course'] = $this->charts->pieChart(100,$monthly_course_number,$month_name,'',$title,$bottom_label,$config);					
				$this->load->view('dashboard\admin_dashboard\charts',$data);
			}
			else
			{
				echo "Data Fetching Problem";
			}
		}
	}
	
	function student()
	{
		if($this->session->userdata('ISLOGIN'))
		{
			if($this->session->userdata('USERCURRENTROLE') != 1)//check if the user is authenticate 
			{					
				redirect('home_controller/home');
			}
			$data = array(
				'EMAIL' => $this->session->userdata('EMAIL'),
				'USERNAME' => $this->session->userdata('USERNAME')
			);	
			$student_id= $this->session->userdata('USERID');
			$this->load->model('student_model');
			$reg_courses= $this->student_model->getAllCourse($student_id); 
			$this->load->view('dashboard/student_dashboard/student_dashboard',$data);
			if( !is_null ($reg_courses))
			{
				$this->load->model('course_model');								
				$this->load->model('course_teacher_model');
				$this->load->model('user_model');
				foreach ($reg_courses as $course)
				{
					
					$wrapper_data["course"]=$this->course_model->getCourseById($course->COURSE_ID);
					$teacher=$this->course_teacher_model->getCourseTeacherID($course->COURSE_ID);
					$wrapper_data["teacher"]=$this->user_model->getUserByID($teacher->TEACHER_ID);
					//$wrapper_data["teacher"] = $this->teacher_model->getTeacherDetails(intval($teacher_id->TEACHER_ID));
					$this->load->view('dashboard/student_dashboard/student_dashboard_wrapper',$wrapper_data);
				}
			}
			
			$this->load->view('dashboard/student_dashboard/student_dashboard_wrapper_end');
		}
		else
		{
			redirect('authentication_controller/signin');
		}
		
	}
	function teacher()
	{
		if($this->session->userdata('ISLOGIN'))
		{
			if($this->session->userdata('USERCURRENTROLE') != 2)//check if the user is authenticate 
			{					
				redirect('home_controller/home');
			}
			$data = array(
				'EMAIL' => $this->session->userdata('EMAIL'),
				'USERNAME' => $this->session->userdata('USERNAME')
			);
			$user_id= $this->session->userdata('USERID');
			
			
			$this->load->model('course_teacher_model');
			$this->load->model('course_model');
			$this->load->model('course_teacher_model');
			$this->load->model('user_model');
			$courses= $this->course_teacher_model->getAllCourses($user_id); 
			$this->load->view('dashboard/teacher_dashboard/teacher_dashboard',$data);
			if( !is_null ($courses))
			{				
				foreach ($courses as $course)
				{	
					$data["course"]=$this->course_model->getCourseById($course->COURSE_ID);
					$teacher=$this->course_teacher_model->getCourseTeacherID($course->COURSE_ID);
					$data["teacher"]=$this->user_model->getUserByID($teacher->TEACHER_ID);					
					$this->load->view('dashboard/teacher_dashboard/teacher_dashboard_wrapper',$data);
				}
			}
			$this->load->view('dashboard/teacher_dashboard/teacher_dashboard_wrapper_end');
		}
		else
		{
			redirect('authentication_controller/signin');
		}
		
	}
	function apply()
	{
		//$this->load->view('dashboard/teacher_dashboard');	
		if($this->session->userdata('ISLOGIN'))
		{
			if($this->input->post()) 
			{
				$this->form_validation->set_rules('inputFieldExpert','Field of Expertise','required');	
				//$this->form_validation->set_rules('inputImage','Image','required');	
				
			
				if($this->form_validation->run()) 
				{
					$this->load->model('teacher_model');
					
					$user_id=intval($this->session->userdata('USERID'));
					$expertise = $this->input->post('inputFieldExpert');
					$phone= $this->input->post('inputPhone');
					$birth_date = $this->input->post('inputDate');
					$institution = $this->input->post('inputInstitutionName');	
					$gender = $this->input->post('inputGender');					
					//$image_path = $this->input->post('inputImage');
					//$image_path = "No - path";
					
					$upload_file_name=$user_id;							
					$config['upload_path'] = './uploads/picture/';
					$config['allowed_types'] = 'jpg|png';
					$config['max_size']	= '8000';
					$config['max_width']  = '1024';
					$config['max_height']  = '768';
					$config['file_name']=$upload_file_name;
					

					
					$this->load->library('upload', $config);

					if ( $this->upload->do_upload('inputImage') )
					{	
						$data= $this->upload->data();			
						//$image_path ='./uploads/picture/'.$upload_file_name.$data['file_ext'];
						$image_path =base_url().'uploads/picture/'.$upload_file_name.$data['file_ext'];						
					}
					else 
					{
						echo "Image Upload Failed";
						$image_path="";
						echo $this->upload->display_errors();
					}
					$data = array(
						constant('teacher_model::ID') => $user_id,
						constant('teacher_model::PHONE') => $phone,
						constant('teacher_model::GENDER') => $gender,
						constant('teacher_model::PICTURE_PATH') => $image_path,
						constant('teacher_model::BIRTH_DATE') =>  date('d-M-y',strtotime($birth_date)),
						constant('teacher_model::INSTITUTION_NAME') => $institution,
						constant('teacher_model::FIELD_OF_EXPERTISE') => $expertise,						
						constant('teacher_model::IS_APPROVED')=>0
					);
					$create_teacher=$this->teacher_model->createTeacher($data);	
					if($create_teacher)
					{
						
						redirect('home_controller/home');
					}
					else 
					{
						echo "teacher  is not requested";
                        echo $this->db->last_query();
						$this->load->view('forms/new_teacher');
					}
					
				}
				else
				{
					echo "Form Submission Failed";
					$this->load->view('forms/new_teacher');
				}
			}
			else
			{				
				$this->load->view('forms/new_teacher');
			}
		}
		else
		{			
			redirect('authentication_controller/signin');
		}
		
	}

    function edit_student_info() {
		if($this->session->userdata('ISLOGIN'))
			{
			$this->load->model('user_model');
			$email=$this->input->post('inputEmail');
			$full_name=$this->input->post('inputFullname');
			$user_id= $this->session->userdata('USERID');
			$this->user_model->updateUserInfo($full_name,$email,$user_id);
			$this->session->set_userdata('EMAIL', $email);		
			redirect('dashboard_controller/student');
		}
		else
		{
			redirect('authentication_controller/signin');
		}
      //  echo "Submitted email in dashboard_controller/edit_student_info ===:".$this->input->post('inputEmail');
    }
	
}
?>