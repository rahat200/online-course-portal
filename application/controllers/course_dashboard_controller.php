<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Course_dashboard_controller extends CI_Controller {
  
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','form','file'));
	}

	public function index()
	{
		
	}
	public function course_home()
	{
		if( $this->session->userdata('ISLOGIN') )
		{
			$user_id=$this->session->userdata('USERID') ;
			$this->load->model('student_model');
			$this->load->model('course_model');
			$this->load->model('course_teacher_model');
			
			$this->load->model('teacher_model');
			$this->load->model('user_model');
				
			$course_id = $this->input->get('course_id');
			$teacher_id= $this->course_teacher_model->getCourseTeacherID($course_id);
			$data['teacher_details'] = $this->teacher_model->getTeacherDetails(intval($teacher_id->TEACHER_ID));
			$data['user'] = $this->user_model->getUserByID(intval($teacher_id->TEACHER_ID));
			
			$data['course'] = $this->course_model->getCourseById($course_id);
			$data['teacher']=  $this->course_teacher_model->getCourseTeacherID($course_id);
			if($data['teacher']->TEACHER_ID == $user_id || $this->student_model->isStudentRegistered($course_id,$user_id))
			{
				
				if(is_null($data['course'])) {
					echo "course is null in course dashboard controller";
				}
				else
				{
					$this->load->view('course_dashboard/course_home',$data);
				}	
			}
			else
			{
				redirect("home_controller/home");
			}
        }
		else
		{
			redirect("authentication_controller/signin");
		}
	}
    
    
	public function edit_course_home()
	{		
        $this->load->model('course_model');
        $this->load->model('category_model');
        
        $course_id = $this->input->get('course_id');
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('course-title','Course Title','required');
        $this->form_validation->set_rules('course-description','Course Description','required');
        $this->form_validation->set_rules('course-start-date','Course Start Date','required');
        $this->form_validation->set_rules('category-id','Category','required');
        $this->form_validation->set_rules('course-length','Course Length','required');
        $this->form_validation->set_rules('course-effort','Effort Per Week','required');
        $this->form_validation->set_rules('language','Course Lnaguage','required');
        
        if($this->input->post('submit')) {
            
            if($this->form_validation->run()) {
                
                $data = array(
                    'COURSE_ID'=>$course_id,
                    constant('course_model::TITLE') => $this->input->post('course-title'),
                    constant('course_model::DESCRIPTION') => $this->input->post('course-description'),
                    constant('course_model::ABOUTCOURSE') => $this->input->post('about-course'),
                    constant('course_model::STARTDATE') => date('d-M-y',strtotime($this->input->post('course-start-date'))),//'1-JAN-80'
                    constant('course_model::CATEGORYID') => $this->input->post('category-id'),
                    constant('course_model::PREREQCOURSE') => $this->input->post('course-prerequisite'),
                    constant('course_model::SYLLABUS') => $this->input->post('course-syllabus'),
                    constant('course_model::COURSEFORMAT') => $this->input->post('course-format'),
                    constant('course_model::COURSELENGTHWEEK') => $this->input->post('course-length'),
                    constant('course_model::EFFORTPERWEEK') => $this->input->post('course-effort'),
                    constant('course_model::COURSELANGUAGE') => $this->input->post('language'),
                    constant('course_model::ISPUBLISHED') => 0, //change latter
                    
                );
                
                $isUpdated = $this->course_model->updateCourse($data);
                if($isUpdated) {
                    //course updateed succesfully
                    redirect('/course_dashboard_controller/course_home?course_id='.$course_id,'refresh');
                }
                else {
                    //error, cannot be updated
                    echo "course could not create successfully";
                    //redirect('/course_dashboard_controller/edit_course_home/'.$course_id,'refresh');
                    $this->db->last_query();
                }
                
            }
        }        
        
        
        
        $data['course'] = $this->course_model->getCourseById($course_id);
        if(is_null($data['course'])) {
            echo "course is null in course dashboard controller";
        }
        
        $data['categories'] = $this->category_model->getAllCategories();
        if(is_null($data['categories'])) {
            echo "categories null in course dashboard controller";
        }
        $this->load->view('course_dashboard/edit_course_home',$data);
        
	}
    
    
	public function week_lectures()
	{
        $this->load->model('course_model');
        $this->load->model('lecture_model');
        $this->load->model('material_model');
        
        $course_id = $this->input->get('course_id');
        $week_num = $this->input->get('week_num')?$this->input->get('week_num'):1;
        $lecture_id = $this->input->get('lecture_id');
        //echo "lecture id:".$this->input->get('course_id');
        //echo "course id:".$this->input->get('lecture_id');
        
        $data['course'] = $this->course_model->getCourseById($course_id);
        $data['lecture_list'] = $this->lecture_model->getLecturesOfWeek($course_id,$week_num);
        $data['lec_count'] = $this->lecture_model->getWeeklyLectureCount($course_id);
        $data['current_week'] = $week_num;
		$this->load->model('course_teacher_model');
		$data['teacher']=  $this->course_teacher_model->getCourseTeacherID($course_id);
        //var_dump($data['lec_count']);
        if(is_null($data['course'])) {
            echo "course is null in course dashboard controller";
        }
        
        $this->load->view('course_dashboard/lectures/lectures_top_left',$data);
        if($lecture_id == FALSE) {
            //show lecture list
            $this->load->view('course_dashboard/lectures/lecture_list',$data);
        }
        else {
            //show lecture content
            $data['lecture'] = $this->lecture_model->getLectureById($lecture_id,$course_id);
            $data['lecture_video'] = $this->material_model->getLectureVideo($lecture_id);
            if(is_null($data['lecture'])) {
                echo "lecture is null in course dashboard controller/week_lectures";
            }
            $this->load->view('course_dashboard/lectures/lecture_content',$data);
        }
        $this->load->view('course_dashboard/lectures/lectures_bottom',$data);
        
	}
    public function create_new_lecture() {
        
        $this->load->model('course_model');
        $this->load->model('lecture_model');
        
        $course_id = $this->input->get('course_id');
        $week_num = $this->input->get('week_num')?$this->input->get('week_num'):1;
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('lecture-topic','Lecture Topic','required');
        $this->form_validation->set_rules('lecture-overview','Lecture Overview','required');
        
            if($this->input->post('submit')) {
            
            if($this->form_validation->run()) {
                
                 //echo "lecture is submitted";
                 
                 //upload video file
                $upload_file_name= time();     
                $vid_config['upload_path'] = './uploads/lecture_video/';
                $vid_config['allowed_types'] = 'mp4|mpg|mpeg|avi|mov';
                $vid_config['max_size'] = '20000';
                $vid_config['file_name']=$upload_file_name;
                
                $this->load->library('upload', $vid_config);
                $this->upload->initialize($vid_config);
                $video_path = "";
                if ( $this->upload->do_upload('inputVideo') )
                {   
                    $data= $this->upload->data();           
                    $video_path =base_url().'uploads/lecture_video/'.$upload_file_name.$data['file_ext'];  
                }
                else 
                {
                    echo "Lecture video is not added";
                    $video_path="";
                    echo $this->upload->display_errors();
                }
                 
                $data = array(
                    constant('lecture_model::TOPIC_NAME') => $this->input->post('lecture-topic'),
                    constant('lecture_model::LECTURE_OVERVIEW') => $this->input->post('lecture-overview'),
                    constant('lecture_model::COURSE_ID') => $course_id,
                    constant('lecture_model::WEEK_NUM') => $week_num,
                    constant('lecture_model::UPDATE_TIME') => date('d-M-y'),
                    'MATERIAL_TYPE' => 'video',
                    'CONTENT_PATH' => $video_path
                    
                );
                
                $isCreated = $this->lecture_model->createLecture($data);
                if($isCreated) {
                    //echo "lecture created succesfully";
                    $last_id = $this->lecture_model->getLastInsertedLectureId();
                    redirect(site_url("course_dashboard_controller/week_lectures?course_id={$course_id}&week_num={$week_num}&lecture_id={$last_id}"),'refresh');
                    //now insert the video in material
                    /*
                    $this->load->model('material_model');
                    $mat_data = array(
                        'MATERIAL_TYPE' => 'video',
                        'CONTENT_PATH' => $video_path,
                        'LECTURE_ID' => $last_id
                    );
                    $isMatInserted = $this->material_model->insertMaterial($mat_data);
                    if($isMatInserted) {
                        //material created successfully
                        redirect(site_url("course_dashboard_controller/week_lectures?course_id={$course_id}&week_num={$week_num}&lecture_id={$last_id}"),'refresh');
                    } else {
                        echo "Material could not be inserted";
                        $this->db->last_query();
                    } */
                    
                }
                else {
                    //error, cannot be updated
                    echo "lecture could not create successfully";
                    //redirect('/course_dashboard_controller/edit_course_home/'.$course_id,'refresh');
                    $this->db->last_query();
                }
                
            }
        }        
 
        $data['course'] = $this->course_model->getCourseById($course_id);
        $data['current_week'] = $week_num;
        $data['lec_count'] = $this->lecture_model->getWeeklyLectureCount($course_id);
        if(is_null($data['course'])) {
            echo "course is null in course dashboard controller";
        }
        $this->load->view('course_dashboard/lectures/lectures_top_left',$data);        
        $this->load->view('course_dashboard/lectures/new_lecture',$data);        
        $this->load->view('course_dashboard/lectures/lectures_bottom',$data);        
    }
    
    function edit_lecture() {
        
        $this->load->model('course_model');
        $this->load->model('lecture_model');
        
        $course_id = $this->input->get('course_id');
        $lecture_id = $this->input->get('lecture_id');
        $week_num = $this->input->get('week_num');
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('lecture-topic','Lecture Topic','required');
        $this->form_validation->set_rules('lecture-overview','Lecture Overview','required');
        
            if($this->input->post('submit')) {
            
            if($this->form_validation->run()) {
                
                 //echo "lecture is submitted";
                 
                $data = array(
                    constant('lecture_model::TOPIC_NAME') => $this->input->post('lecture-topic'),
                    constant('lecture_model::LECTURE_OVERVIEW') => $this->input->post('lecture-overview'),
                    constant('lecture_model::UPDATE_TIME') => date('d-M-y'),
                    'LECTURE_ID' => $lecture_id
                    
                );
                
                $isUpdated = $this->lecture_model->updateLecture($data);
                if($isUpdated) {
                    //echo "lecture updated succesfully";
                    redirect(site_url("course_dashboard_controller/week_lectures?course_id={$course_id}&week_num={$week_num}&lecture_id={$lecture_id}"),'refresh');
                }
                else {
                    //error, cannot be updated
                    echo "lecture could not update successfully";
                    //redirect('/course_dashboard_controller/edit_course_home/'.$course_id,'refresh');
                    $this->db->last_query();
                }
                
            }
        }        
 
        $data['course'] = $this->course_model->getCourseById($course_id);
        $data['current_week'] = $week_num;
        $data['lec_count'] = $this->lecture_model->getWeeklyLectureCount($course_id);
        $data['lecture'] = $this->lecture_model->getLectureById($lecture_id,$course_id);
        if(is_null($data['course'])) {
            echo "course is null in course dashboard controller";
        }
        if(is_null($data['lecture'])) {
            echo "lecture is null in course dashboard controller/edit_lecture";
        }
        $this->load->view('course_dashboard/lectures/lectures_top_left',$data);        
        $this->load->view('course_dashboard/lectures/edit_lecture',$data);        
        $this->load->view('course_dashboard/lectures/lectures_bottom',$data);          
    }

    public function delete_lecture() {
        $this->load->model('lecture_model');
        
        $course_id = $this->input->get('course_id');
        $lecture_id = $this->input->get('lecture_id');
        $week_num = $this->input->get('week_num');
		$lecture_path=$this->lecture_model->getLectureVideoPath($lecture_id);        
        $isDeleted = $this->lecture_model->deleteLecture($lecture_id);
        if($isDeleted) {
            
			echo $lecture_path->CONTENT_PATH;
			if(!is_null($lecture_path->CONTENT_PATH))
			{			
				if(unlink($lecture_path->CONTENT_PATH))
				{
					echo "File Deleted";
				}
				
			}
			redirect(site_url("Course_dashboard_controller/week_lectures?course_id={$course_id}&week_num={$week_num}"));
        } else {
            echo "lecture could not be deleted";
            echo $this->db->last_query();
            redirect(site_url("Course_dashboard_controller/week_lectures?course_id={$course_id}&week_num={$week_num}&lecture_id={$lecture_id}"));
        }
    }
    
    
	public function course_quizes()
	{
        $this->load->model('course_model');
        $this->load->model('quiz_model');        
		$this->load->model('course_teacher_model');
		$this->load->model('quiz_result_model');
		$this->load->model('student_model');
        
		$course_id = $this->input->get('course_id');
		$user_id=$this->session->userdata('USERID') ;
        $data['teacher']=  $this->course_teacher_model->getCourseTeacherID($course_id);
        $data['course'] = $this->course_model->getCourseById($course_id);
        $quizes = $this->quiz_model->getQuizesOfCourse($course_id);
		$data['registeredstudent']=$this->student_model->isStudentRegistered($course_id,$user_id);
        
        if(is_null($data['course'])) {
            echo "course is null in course dashboard controller";
        }
        $this->load->view('course_dashboard/quizes/quizes_top',$data);
        if(!is_null($quizes)) {
            foreach ($quizes as $quiz) {
                 $data['quiz'] = $quiz;
                $data['marks'] = $this->quiz_result_model->getResultOfQuiz($quiz->ID);
                 $this->load->view('course_dashboard/quizes/each_quiz',$data);
             }
         } else {
             $this->load->view('course_dashboard/quizes/no_quiz_msg');
         }

        $this->load->view('course_dashboard/quizes/quizes_bottom',$data);
        //$this->load->view('course_dashboard/quizes/quizes',$data);
        
	}
    
    public function create_quiz() {
        
        $this->load->model('course_model');
        $this->load->model('quiz_model');
        $this->load->model('question_model');
        $this->load->model('choice_model');
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('quiz-topic','Quiz Topic','required');
        $this->form_validation->set_rules('quiz-deadline','Quiz Deadline','required');
        $this->form_validation->set_rules('quiz-week-num','Quiz Week','required');
        
        $course_id = $this->input->get('course_id');
        
        if($this->input->post('submit')) {
            if($this->form_validation->run()) {
                    
               $quiz_data = array(
                    'COURSE_ID' => $course_id,
                    'TOPIC' => $this->input->post('quiz-topic'),
                    'WEEK_NUM' => $this->input->post('quiz-week-num'),
                    'UPDATE_TIME' => date('d-M-y'),
                    'HARD_DEADLINE' => date('d-M-y',strtotime($this->input->post('quiz-deadline'))),
                    
                );
                $isQuizCreated = $this->quiz_model->createQuiz($quiz_data);
                if($isQuizCreated) {
                    //quiz inserted, now insert question and choice
                    $lastQuizId = $this->quiz_model->getLastInsertedQuizId();
                    for ($i=1; $i <=5 ; $i++) { //this loop iterates for each question
                            
                        if($this->input->post("qn{$i}")) {
                            $ques_data = array (
                                'QUIZ_ID' => $lastQuizId,
                                'QUES_TEXT' => $this->input->post("qn{$i}")
                            );
                            $isQuesCreated= $this->question_model->createQuestion($ques_data);
                            if($isQuesCreated) {
                                //question inserted, now insert choices
                                $lastQuesId = $this->question_model->getLastInsertedQuestionId();
                                for ($j=1; $j <=4 ; $j++) { 
                                    if($this->input->post("qn{$i}ch{$j}")) {
                                        $choice_text = $this->input->post("qn{$i}ch{$j}");
                                        $choice_data = array(
                                        'QUES_ID' => $lastQuesId,
                                        'CHOICE_TEXT' => $choice_text
                                        );
                                        $isChoiceCreated = $this->choice_model->createChoice($choice_data);
                                        if($isChoiceCreated) {
                                        if($this->input->post("ans{$i}")=="qn{$i}ch{$j}") {
                                            //if this choice is right ans,
                                            // then insert its id in question table answer_id field
                                            $lastChoiceId = $this->choice_model->getLastInsertedChoiceId();
                                            $isUpdated = $this->question_model->updateChoiceId($lastQuesId,$lastChoiceId);
                                            if($isUpdated) {
                                                //redirect(site_url("course_dashboard_controller/course_quizes?course_id=".$course_id));
                                                echo "choice id updated";
                                            } else {
                                                echo "question{$i} table cannot be updated with choice_id";
                                            }
                                        }
                                        } else {
                                            "choice{$j} cannot be inserted".$this->db->last_query();
                                        }
                                    } else {
                                        echo "choice {$j} of qsn {$i} is not posted ";
                                    }
                                }
                            } else {
                                echo "question{$i} cannot be inserted".$this->db->last_query();
                            }
                        } else {
                            echo "question{$i} is not posted</br>";
                        }


                    }
                } else {
                    echo "quiz is not created---".$this->db->last_query();
                }
                
                redirect(site_url("course_dashboard_controller/course_quizes?course_id=".$course_id));
                    
            }
        }
        
        
        
        $data['course'] = $this->course_model->getCourseById($course_id);
        
        $this->load->view('course_dashboard/quizes/new_quiz',$data);
    }
    
    function delete_quiz() {
        $quiz_id = $this->input->get('quiz_id');
        $course_id = $this->input->get('course_id');
        $this->load->model('quiz_model');
        $isdeleted = $this->quiz_model->deleteQuiz($quiz_id);
        if($isdeleted) {
            redirect('course_dashboard_controller/course_quizes?course_id='.$course_id);
        }
            
    }
    
	public function course_assignments()
	{
        $this->load->model('course_model');
		$this->load->model('assignment_model');
		if( $this->session->userdata('ISLOGIN') )
		{
			$user_id=$this->session->userdata('USERID') ;
			$this->load->model('student_model');			
			$this->load->model('course_teacher_model');
			$this->load->model('teacher_model');
			$this->load->model('user_model');
			$course_id = intval($this->input->get('course_id'));
			$teacher_id= $this->course_teacher_model->getCourseTeacherID($course_id);
			$data['teacher']=  $this->course_teacher_model->getCourseTeacherID($course_id);
			$data['registeredstudent']=$this->student_model->isStudentRegistered($course_id,$user_id);
			$data['teacher_details'] = $this->teacher_model->getTeacherDetails(intval($teacher_id->TEACHER_ID));
			$data['user'] = $this->user_model->getUserByID(intval($teacher_id->TEACHER_ID));
			if($data['teacher']->TEACHER_ID == $user_id || $this->student_model->isStudentRegistered($course_id,$user_id))
			{
        
				$course_id = $this->input->get('course_id');
				
				$data['course'] = $this->course_model->getCourseById($course_id);
				
				if(is_null($data['course'])) {
					echo "course is null in course dashboard controller";
				}
				$data['assignments']=$this->assignment_model->getAllAssignments($course_id);	
				$this->load->view('course_dashboard/assignments/course_assignments',$data);
			}
			else
			{
				redirect('home_controller/home');
			}
		}
		else
		{
			redirect('home_controller/home');
		}
        
	}
	function create_course_assignment()
	{
		if( $this->session->userdata('ISLOGIN') )
		{
			$user_id=$this->session->userdata('USERID') ;
			$this->load->model('student_model');			
			$this->load->model('course_teacher_model');
			$course_id = intval($this->input->get('course_id'));
			$teacher_id= $this->course_teacher_model->getCourseTeacherID($course_id);
			$data['teacher']=  $this->course_teacher_model->getCourseTeacherID($course_id);
			if($data['teacher']->TEACHER_ID == $user_id || $this->student_model->isStudentRegistered($course_id,$user_id))
			{
				if($this->input->post())
				{		
					$course_id = intval($this->input->get('course_id'));
					$this->load->model('assignment_model');
					$assignment=$this->input->post('inputAssignment');
					$title = $this->input->post('inputTitle');
					$start_date = $this->input->post('inputStartDate');
					$end_date = $this->input->post('inputEndDate');
					$text=$this->input->post('inputText');
					
					$upload_file_name=time();							
					$config['upload_path'] = './uploads/assignment/';
					$config['allowed_types'] = 'pdf|doc|txt|docx|ppt|pptx';
					$config['max_size']	= '8000';				
					$config['file_name']=$upload_file_name;
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					$assignment_path = "";
					if ( $this->upload->do_upload('inputAssignment') )
					{   
						$data= $this->upload->data();           
						$assignment_path =base_url().'uploads/assignment/'.$upload_file_name.$data['file_ext'];  
					}
					else 
					{
						echo "Assignment is not added";
						$assignment_path="";
						echo $this->upload->display_errors();
					}
					 
					$data = array(
						'COURSE_ID' => $course_id,
						'TITLE'=> $title,				 
						'START_DATE' => date('d-M-y',strtotime($start_date)),
						'END_DATE' => date('d-M-y',strtotime($end_date)),
						'TEXT'=> $text,
						'CONTENT_PATH'=> $assignment_path
					);	
					
					
					$is_created=$this->assignment_model->insertAssignment($data);
					if($is_created)
					{
						echo "Successfully Created";
						redirect('home_controller/home');// it should be changed
					}
					else
					{
						echo "Not Created";
						echo $this->db->last_query();
					}
					
					
				}
				else
				{
					$this->load->view('course_dashboard/assignments/new_assignment');
				}
			}
			else
			{
				redirect('home_controller/home');
			}
		}
		else
		{
			redirect('authentication_controller/signin');
		}
	}
	
	function submitAssignment()
	{
		if( $this->session->userdata('ISLOGIN') )
		{
			$course_id= $this->input->get('course_id');
			$assignment_id= intval($this->input->get('assignment_id'));
			$user_id=intval($this->session->userdata('USERID')) ;
			$this->load->model('student_model');
			
			if($this->input->post())
			{
				 if($this->student_model->isStudentRegistered($course_id,$user_id))
				 {
						$course_id = intval($this->input->get('course_id'));
						$this->load->model('submitted_assignment_model');
						$assignment=$this->input->post('inputAssignment');					
						
						$upload_file_name=time();							
						$config['upload_path'] = './uploads/assignment/';
						$config['allowed_types'] = 'pdf|doc|txt|docx|ppt|pptx';
						$config['max_size']	= '8000';				
						$config['file_name']=$upload_file_name;
						
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						$assignment_path = "";
						if ( $this->upload->do_upload('inputAssignment') )
						{   
							$data= $this->upload->data();           
							$assignment_path =base_url().'uploads/submitted_assignment/'.$upload_file_name.$data['file_ext'];  
						}
						else 
						{
							echo "Assignment is not Submitted";
							$assignment_path="";
							echo $this->upload->display_errors();
						}
						 
						$data = array(							
							'STUDENT_ID'=> $user_id,				 
							'ASSIGNMENT_ID' => $assignment_id,
							'SUBMITTED_DATE' => date('d-M-y'),							
							'CONTENT_PATH'=> $assignment_path
						);						
						
						$is_created=$this->submitted_assignment_model->insertSubmittedAssignment($data);
						if($is_created)
						{
							echo "Successfully Submitted";
							redirect('home_controller/home');// it should be changed
						}
						else
						{
							echo "Not Submitted";
							echo $this->db->last_query();
						}
					}
				else
				{
					redirect('home_controller/home');
				}					
			}
			else
			{
			//?course_id='.$course_id.'&assignment_id=',$assignment_id
				$this->load->view('course_dashboard/assignments/submit_assignment');
			}
		}
		else
		{
			redirect('authentication_controller/signin');
		}
	}
	
	function view_submission()
	{
		$this->load->model('course_model');
		$this->load->model('assignment_model');
		if( $this->session->userdata('ISLOGIN') )
		{
			$user_id=$this->session->userdata('USERID') ;
			$this->load->model('student_model');			
			$this->load->model('course_teacher_model');
			$this->load->model('teacher_model');
			$this->load->model('user_model');
			$course_id = intval($this->input->get('course_id'));
			$teacher_id= $this->course_teacher_model->getCourseTeacherID($course_id);
			$data['teacher']=  $this->course_teacher_model->getCourseTeacherID($course_id);
			$data['registeredstudent']=$this->student_model->isStudentRegistered($course_id,$user_id);
			$data['teacher_details'] = $this->teacher_model->getTeacherDetails(intval($teacher_id->TEACHER_ID));
			$data['user'] = $this->user_model->getUserByID(intval($teacher_id->TEACHER_ID));
			$this->load->model('submitted_assignment_model');
			$data['submittedassignments']=$this->submitted_assignment_model->getAllSubmittedAssignments($course_id);
			if($data['teacher']->TEACHER_ID == $user_id || $this->student_model->isStudentRegistered($course_id,$user_id))
			{
        
				$course_id = $this->input->get('course_id');
				
				$data['course'] = $this->course_model->getCourseById($course_id);
				
				if(is_null($data['course'])) {
					echo "course is null in course dashboard controller";
				}
				$data['assignments']=$this->assignment_model->getAllAssignments($course_id);	
				$this->load->view('course_dashboard/assignments/course_assignments_list_show',$data);			
			}
			else
			{
				redirect('home_controller/home');
			}
		}
		else
		{
			redirect('home_controller/home');
		}
	}
	
	
	function forum()
	{
		if( $this->session->userdata('ISLOGIN') )
		{
			$this->load->model('student_model');			
			$this->load->model('course_teacher_model');
			$this->load->model('teacher_model');
			$this->load->model('user_model');
			$this->load->model('course_model');
			$this->load->model('forum_ques_model');
			$this->load->model('lecture_model');
			$user_id=$this->session->userdata('USERID') ;
			$course_id = intval($this->input->get('course_id'));
			$teacher_id= $this->course_teacher_model->getCourseTeacherID($course_id);			
			$data['registeredstudent']=$this->student_model->isStudentRegistered($course_id,$user_id);
			$data['teacher_details'] = $this->teacher_model->getTeacherDetails(intval($teacher_id->TEACHER_ID));
			$data['user'] = $this->user_model->getUserByID(intval($teacher_id->TEACHER_ID));
			
			
			
			$course_id = $this->input->get('course_id');
			$data['teacher']=  $this->course_teacher_model->getCourseTeacherID($course_id);
			if($data['teacher']->TEACHER_ID == $user_id || $this->student_model->isStudentRegistered($course_id,$user_id))
			{
				$data['course'] = $this->course_model->getCourseById($course_id);
				$forum_questions = $this->forum_ques_model->getAllForumQuestionsByCourseId($course_id);			
				if(is_null($data['course'])) {
					echo "course is null in course dashboard controller";
				}
				else
				{
					//$data['weekly_lectures']=$this->lecture_model->getWeeklyLectureCount($course_id);						
					$data['weekly_lectures']=$this->lecture_model->getAllLectures($course_id);
					$data['current']=0;
					$this->load->view('course_dashboard/forum/forum',$data);
					
					if(!is_null($forum_questions))
					{
						$this->load->model('forum_ans_model');
						foreach($forum_questions as $question)
						{
							$data["questions"]=$question;
							$data["answers"]=$this->forum_ans_model->getAllForumAnswer($question->ID);
							$this->load->view('course_dashboard/forum/forum_wrapper',$data);
						}
					}
					$this->load->view('course_dashboard/forum/forum_wrapper_end');
				}
			}
			else
			{
				redirect("home_controller/home");
			}
		}
		else
		{
			redirect("authentication_controller/signin");
		}
		
	}
	
	
	function lecture_forum()
	{
		if( $this->session->userdata('ISLOGIN') )
		{
			$this->load->model('course_model');
			$this->load->model('forum_ques_model');
			$user_id=$this->session->userdata('USERID') ;
			$this->load->model('student_model');
			$this->load->model('lecture_model');
			$this->load->model('course_teacher_model');
			$course_id = $this->input->get('course_id');
			$lecture_id = $this->input->get('lecture_id');
			$data['teacher']=  $this->course_teacher_model->getCourseTeacherID($course_id);
			if($data['teacher']->TEACHER_ID == $user_id || $this->student_model->isStudentRegistered($course_id,$user_id))
			{
				$data['course'] = $this->course_model->getCourseById($course_id);
				$forum_questions = $this->forum_ques_model->getAllForumQuestionsByLectureId($lecture_id);			
				if(is_null($data['course'])) {
					echo "course is null in course dashboard controller";
				}
				else
				{
					//$data['weekly_lectures']=$this->lecture_model->getWeeklyLectureCount($course_id);						
					$data['weekly_lectures']=$this->lecture_model->getAllLectures($course_id);
					$data['current']=$lecture_id;
					$this->load->view('course_dashboard/forum/forum',$data);
					
					if(!is_null($forum_questions))
					{
						$this->load->model('forum_ans_model');
						foreach($forum_questions as $question)
						{
							$data["questions"]=$question;
							$data["answers"]=$this->forum_ans_model->getAllForumAnswer($question->ID);
							$this->load->view('course_dashboard/forum/forum_wrapper',$data);
						}
					}
					$this->load->view('course_dashboard/forum/forum_wrapper_end');
				}
			}
			else
			{
				redirect("home_controller/home");
			}
		}
		else
		{
			redirect("authentication_controller/signin");
		}
		
	}
	
	function forum_ask()
	{
		
		if( $this->session->userdata('ISLOGIN') )
		{
			$user_id=$this->session->userdata('USERID') ;
			$this->load->model('student_model');			
			$this->load->model('course_teacher_model');
		
			$course_id = $this->input->get('course_id');
			$lecture_id= $this->input->get('lecture_id');
			if(!is_null($course_id) && !is_null($lecture_id))
			{
				$data['teacher']=  $this->course_teacher_model->getCourseTeacherID($course_id);
				if($data['teacher']->TEACHER_ID == $user_id || $this->student_model->isStudentRegistered($course_id,$user_id))
				{
					if($this->input->post())
					{
						$this->load->library('form_validation');
						$this->form_validation->set_rules('inputQuestion','Question Overview','required');            
						if($this->form_validation->run()) 
						{
							$data=array(
							'USER_ID'=> $user_id,
							'TEXT'=> $this->input->post('inputQuestion'),
							'LECTURE_ID'=> $lecture_id,
							'POSTED_DATE'=>date('d-M-y') 						
							);
                            $this->load->model('forum_ques_model');
                            $isCreated=$this->forum_ques_model->createNewQuestion($data);
                            
                            if($isCreated) {
                                redirect('course_dashboard_controller/lecture_forum?course_id='.$course_id.'&lecture_id='.$lecture_id);
                            } else {
                                echo "Problem in adding Question. Sorry try again";
                            }
						}
					}
					else
					{
						$data=array(							
							'COURSE_ID'=> $course_id,
							'LECTURE_ID'=> $lecture_id												
							);
						$this->load->view('course_dashboard/forum/new_question',$data);
					}
				}
				else
				{
					redirect("home_controller/home");
				}
			}
			else
			{
				redirect("home_controller/home");
			}
        }
		else
		{
			redirect("authentication_controller/signin");
		}
		
		
	}
	function forum_answer()
	{
		
		if( $this->session->userdata('ISLOGIN') )
		{
			$user_id=$this->session->userdata('USERID') ;
			$this->load->model('student_model');			
			$this->load->model('course_teacher_model');
		
			$course_id = $this->input->get('course_id');
			$lecture_id= $this->input->get('lecture_id');
			$forum_question_id=$this->input->get('forum_question_id');
			if(!is_null($course_id) && !is_null($forum_question_id)  && !is_null($lecture_id))
			{
				$data['teacher']=  $this->course_teacher_model->getCourseTeacherID($course_id);
				if($data['teacher']->TEACHER_ID == $user_id || $this->student_model->isStudentRegistered($course_id,$user_id))
				{
					if($this->input->post())
					{
						$this->load->library('form_validation');
						$this->form_validation->set_rules('inputAnswer','Answer Overview','required');            
						if($this->form_validation->run()) 
						{
							$data=array(
							'USER_ID'=> $user_id,
							'TEXT'=> $this->input->post('inputAnswer'),
							'FORUM_QUESTION_ID'=> $forum_question_id,
							'POSTED_DATE'=>date('d-M-y') 						
							);
                            $this->load->model('forum_ans_model');
                            $isCreated=$this->forum_ans_model->createNewAnswer($data);
                            
                            if($isCreated) {
                                redirect('course_dashboard_controller/lecture_forum?course_id='.$course_id.'&lecture_id='.$lecture_id);
                            } else {
                                echo "Problem in adding Answer. Sorry try again";
                            }
                            
						}
						
					}
					else
					{
						$data=array(							
							'COURSE_ID'=> $course_id,
							'FORUM_QUESTION_ID'=> $forum_question_id,
							'LECTURE_ID'=>$lecture_id
							);
						$this->load->view('course_dashboard/forum/new_answer',$data);
					}
				}
				else
				{
					redirect("home_controller/home");
				}
			}
			else
			{
				redirect("home_controller/home");
			}
        }
		else
		{
			redirect("authentication_controller/signin");
		}
		
	}
	
}

