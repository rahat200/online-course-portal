<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Authentication_controller extends CI_Controller
{
	
	
	function __construct()
	{
		parent::__construct();

		$this->load->helper(array('form', 'url'));		
		$this->load->library('security');			
		$this->load->library('form_validation');
			
	}	
	function index()
	{			
		echo "Welcome to the secret index";			
	}	
	function signin()
	{
	
		if($this->session->userdata('ISLOGIN'))
		{			
			redirect('home_controller/home');
		}
		if($this->input->post())		
		{
			
			$this->form_validation->set_rules('inputEmail','Emali','required');
			$this->form_validation->set_rules('inputPassword','Password','required');
			
			if($this->form_validation->run()) 
			{			
				$email = strtolower($this->input->post('inputEmail'));
				$password = $this->input->post('inputPassword');
				$this->load->model('user_model');
				$user=$this->user_model->getUserLogin($email,$password);	
				
				if( !empty($user) )
				{
					$userSessionData = array(
							'USERNAME'  => $user->USERNAME,
							'USERID'  => $user->ID,
							'EMAIL' => $user->EMAIL,
							'ISLOGIN'  => true,							
							'USERCURRENTROLE' => 1, //always first show the user as student
							'USERROLE'  => $user->USER_ROLE                
						   );
					$this->session->set_userdata($userSessionData);		
					redirect('home_controller/home');
				}	
				else
				{
					echo "Invalid Login Credential";
					$this->load->view('forms/signin');
				}
			}
			else
			{
				$this->load->view('forms/signin');		
			}
		}
		else
		{
			$this->load->view('forms/signin');		
		}
	}

	
	
	
	function signup()
	{	
		if($this->session->userdata('ISLOGIN'))
		{			
			redirect('home_controller/home');
		}
		if($this->input->post()) 
		{
			$this->form_validation->set_rules('inputEmail','Emali','required');
			$this->form_validation->set_rules('inputUsername','Username','required');
			$this->form_validation->set_rules('inputFullName','Full Name','required');
			$this->form_validation->set_rules('inputPassword','Password','required|matches[inputRetypePassword]');
			$this->form_validation->set_rules('inputRetypePassword','Re-type Password','required');
			
			if($this->form_validation->run()) 
			{
				$this->load->model('user_model');
				$email = strtolower($this->input->post('inputEmail'));
				$password = $this->input->post('inputPassword');
				$username = strtolower($this->input->post('inputUsername'));
				$fullname = $this->input->post('inputFullName');	
				if($this->user_model->isUsernameAvailable($username) && $this->user_model->isEmailAvailable($email))
				{							
					$data = array(
						constant('user_model::EMAIL') => $email,
						constant('user_model::PASSWORD') => $password,
						constant('user_model::USERNAME') => $username,
						constant('user_model::FULLNAME') => $fullname,
						constant('user_model::USERROLE')=>1
						
					);
					$isUserCreated = $this->user_model->createUser($data);
                    if(!$isUserCreated)
                    {
                        echo "user is not created";
                        //echo $this->db->last_query();
                    }
                    else	
					    redirect('authentication_controller/signin');
				}
				else
				{
					echo "Username or Email already exist";
					$this->load->view('forms/signup');		
				}
			}
			else
			{
				$this->load->view('forms/signup');		
			}
		}
		else
		{
			$this->load->view('forms/signup');		
		}
	}
	
	function signout()
	{
		$this->session->sess_destroy();
		redirect('home_controller/home');
	}

	
}
?>