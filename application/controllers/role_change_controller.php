<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Role_change_controller extends CI_Controller {
  
	/**
	 * Constructor. 
	 */
	function __construct()
	{
	  parent::__construct();
	  $this->load->helper('url');
	}
	
	function index()
	{	        
	}
	function teacher()
	{		
		if($this->session->userdata('USERROLE') != 2)//check if user is a teacher
		{					
			redirect('home_controller/home');
		}
		$this->session->set_userdata('USERCURRENTROLE', 2);
		redirect('dashboard_controller/teacher');
	}
	function student()
	{
		if($this->session->userdata('USERCURRENTROLE') != 2)
		{					
			redirect('home_controller/home');
		}
		$this->session->set_userdata('USERCURRENTROLE', 1);					
		redirect('dashboard_controller/student');
	}
   
   
} 
/* End of file index_controller.php */
/* Location: ./application/controllers/index_controller.php */