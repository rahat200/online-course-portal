<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home_controller extends CI_Controller
{
	
	
	function __construct()
	{
		parent::__construct();

		$this->load->helper(array('form', 'url'));		
		$this->load->library('security');			
		$this->load->library('form_validation');
		$this->load->library('charts');	
			
	}	
	function index()
	{			
		echo "Welcome to the secret index";			
	}
	
	function home()
	{	
		$this->load->model('course_model');
		$this->load->model('student_model');
		$this->load->view('home/home');
		
	    $courses=$this->course_model->getNewCourses();
		$title['heading']="MOST RECENT";
		$this->load->view('home/home_wrapper_top',$title);
		if(!is_null($courses))
		{			
			foreach($courses as $course)
			{				
				$this->load->view('home/home_wrapper',$course);
			}
			
		}
		
		$this->load->view('home/home_wrapper_row_end');
		$courses=$this->course_model->getPastCourses();
		$title['heading']="Past";
		$this->load->view('home/home_wrapper_top',$title);
		if(!is_null($courses))
		{			
			foreach($courses as $course)
			{				
				$this->load->view('home/home_wrapper',$course);
			}
			
		}
		
			
		$this->load->view('home/home_wrapper_row_end');
		
		
		$title['heading']="MOST POPULAR";
		$this->load->view('home/home_wrapper_top',$title);
		
		$max_reg_course=$this->student_model->getMaxRegisteredCourse();		
		if(!is_null($max_reg_course))
		{			
			$popular=array();
			foreach ($max_reg_course as $course)
			{						
				array_push($popular,$this->course_model->getCourseById($course->COURSE_ID));				
			}
			foreach($popular as $course)
			{					
				$this->load->view('home/home_wrapper',$course);
			}		
		}	
		$this->load->view('home/home_wrapper_row_end');
		
		$this->load->view('home/home_wrapper_end');
	}
	function search()
	{
		$search_item=$this->input->get('search_item');
		if($this->input->post())
		{			
			$search_item= $this->input->post('search-item');			
			if(!is_null($search_item))
			{									
				$this->load->model('course_model');
				$data["courses"]=$this->course_model->getCourseSearchByTitle($search_item);
				$data["selected"]= "all";
				$data["search_item"]=$search_item;
				if(!is_null($data))
				{
					$this->load->view('home/search',$data);
				}
				else
				{
					echo "Sorry there exist no course like".$search_item;
				}
			}			
		}
		else if(!is_null($search_item))
		{
			$this->load->model('course_model');
			$data["courses"]=$this->course_model->getCourseSearchByTitle($search_item);
			$data["selected"]= "all";
			$data["search_item"]=$search_item;			
			if(!is_null($data))
			{
				$this->load->view('home/search',$data);
			}
			else
			{
				echo "Sorry there exist no course like".$search_item;
			}
		}
		else
		{
			redirect('home_controller/home');
		}
	}
	function search_teacher()
	{
		//$course_teacher= $this->session->userdata('SEARCHITEM');
		$course_teacher=$this->input->get('search_item');
		if(!is_null($course_teacher))
		{	
			$this->load->model('course_model');
			$data["search_item"]=$course_teacher;			
			$data["courses"]=$this->course_model->getCourseSearchByTeacher($course_teacher);	
			$data["selected"]= "teacher";			
			if(!is_null($data))
			{
				$this->load->view('home/search',$data);
			}
			else
			{
				echo "Sorry there exist no teacher like".$course_teacher;
			}
		}		
	}
	function search_category()
	{
		$course_category=$this->input->get('search_item');
		if(!is_null($course_category))
		{								
			$this->load->model('course_model');
			$data["search_item"]=$course_category;			
			$data["courses"]=$this->course_model->getCourseSearchByCategory($course_category);		
			$data["selected"]= "category";
			if(!is_null($data))
			{
				$this->load->view('home/search',$data);
			}
			else
			{
				echo "Sorry there exist no category like".$course_category;
			}
		}	
	}
	
}
?>