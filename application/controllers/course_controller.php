<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Course_controller extends CI_Controller {
  
	/**
	 * Constructor. 
	 */
	function __construct()
	{
	  parent::__construct();
      $this->load->helper('url');
	}
	
	function index()
	{
	    //load helpers and library
		$this->load->helper(array('form','url'));
        
	}
    

    /*
    function dashboard()
    {
        $this->load->helper('url');
        $this->load->view('student/student_dashboard');
    }
    */
    
    function create_new_course() {
        
        $this->load->helper('form');
        $this->load->library('form_validation');
		if( $this->session->userdata('ISLOGIN') )
		{
			$user_role=$this->session->userdata('USERROLE') ;
			if($user_role == 2)
			{
				
				$this->form_validation->set_rules('course-title','Course Title','required');
				$this->form_validation->set_rules('course-description','Course Description','required');
				$this->form_validation->set_rules('course-start-date','Course Start Date','required');
				$this->form_validation->set_rules('category-id','Category','required');
				$this->form_validation->set_rules('course-length','Course Length','required');
				$this->form_validation->set_rules('course-effort','Effort Per Week','required');
				$this->form_validation->set_rules('language','Course Lnaguage','required');
				
				if($this->input->post('submit')) {
					
					if($this->form_validation->run()) {
						
						$this->load->model('course_model');
						$this->load->model('course_teacher_model');
						//upload image file
						$upload_file_name= time();     
						$img_config['upload_path'] = './uploads/course_banner/';
						$img_config['allowed_types'] = 'jpg|png';
						$img_config['max_size'] = '8000';
						$img_config['max_width']  = '1024';
						$img_config['max_height']  = '768';
						$img_config['file_name']=$upload_file_name;
							
						$this->load->library('upload', $img_config);
						$image_path = null;
						if ( $this->upload->do_upload('inputImage') )
						{   
							$data= $this->upload->data();           
							$image_path =base_url().'uploads/course_banner/'.$upload_file_name.$data['file_ext'];  
						}
						else 
						{
							echo "Banner is not added.load default banner";
							$image_path=base_url().'uploads/course_banner/default_banner.png';
							echo $this->upload->display_errors();
						}
						//upload video file
						$upload_file_name= time();     
						$vid_config['upload_path'] = './uploads/course_intro_video/';
						$vid_config['allowed_types'] = 'mp4|mpg|mpeg|avi|mov';
						$vid_config['max_size'] = '20000';
						$vid_config['file_name']=$upload_file_name;
							
						$this->upload->initialize($vid_config);
						$video_path = null;
						if ( $this->upload->do_upload('inputVideo') )
						{   
							$data= $this->upload->data();           
							$video_path =base_url().'uploads/course_intro_video/'.$upload_file_name.$data['file_ext'];  
						}
						else 
						{
							echo "!!! No Intro video added for this course";
							$video_path="";
							echo $this->upload->display_errors();
						}
						
						
						$data = array(
						//pass teacher id with $data
							'TEACHER_ID' => intval($this->session->userdata('USERID')),
							constant('course_model::TITLE') => $this->input->post('course-title'),
							constant('course_model::DESCRIPTION') => $this->input->post('course-description'),
							constant('course_model::ABOUTCOURSE') => $this->input->post('about-course'),
							constant('course_model::STARTDATE') => date('d-M-y',strtotime($this->input->post('course-start-date'))),//'1-JAN-80'
							constant('course_model::CATEGORYID') => $this->input->post('category-id'),
							constant('course_model::PREREQCOURSE') => $this->input->post('course-prerequisite'),
							constant('course_model::SYLLABUS') => $this->input->post('course-syllabus'),
							constant('course_model::COURSEFORMAT') => $this->input->post('course-format'),
							constant('course_model::COURSELENGTHWEEK') => $this->input->post('course-length'),
							constant('course_model::EFFORTPERWEEK') => $this->input->post('course-effort'),
							constant('course_model::COURSEPOSTEDDATE') => date('d-M-y'),
							constant('course_model::COURSELANGUAGE') => $this->input->post('language'),
							constant('course_model::ISPUBLISHED') => 0, //change latter
							constant('course_model::IMAGEPATH') => $image_path, //change latter
							constant('course_model::INTROVIDEOPATH') => $video_path, //change latter
						);
						
						$isCreated = $this->course_model->createCourse($data);
						if($isCreated) {
							
							$last_inserted_id = intval($this->course_model->getLastInsertedCourseId());
							redirect('/course_dashboard_controller/course_home?course_id='.$last_inserted_id,'refresh');
						
						} else {
							echo "course could not create successfully";
						}
						
						 
						 /*if($isCreated) {
							//course created succesfully
							$last_inserted_id = intval($this->course_model->getLastInsertedCourseId());
							$teacher_id = intval($this->session->userdata('USERID'));
							$isInserted = $this->course_teacher_model->insert($teacher_id,$last_inserted_id);
							if(!$isInserted) {
								echo "could not insert into course teacher";
								echo $this->db->last_query();
							}
							else
								redirect('/course_dashboard_controller/course_home?course_id='.$last_inserted_id,'refresh');
							
						}
						else {
							//error, cannot create
							echo "course could not create successfully";
						} */
						
					}
				}
				
				
				$this->load->model('category_model');
				$data['categories'] = $this->category_model->getAllCategories();		
				$this->load->view('forms/new_course',$data);
			}
			else
			{
				redirect("home_controller/home");
			}
		}
		else
		{
			redirect("authentication_controller/signin");
		}
				
    }
	function course()
	{	
		$user_id=$this->session->userdata('USERID');
		$course_id=intval($this->input->get('course_id'));		
		$this->load->model('student_model');
		if(!is_null($user_id))
		{
			$registered= $this->student_model->isStudentRegistered($course_id,$user_id);
			if(!$registered)
			{
				$this->load->model('course_model');		
				$this->load->model('course_teacher_model');
				$this->load->model('teacher_model');
				$this->load->model('user_model');
				
				$course["COURSE"] = $this->course_model->getCourseById($course_id);				
				$teacher_id= $this->course_teacher_model->getCourseTeacherID($course_id);
				$course["TEACHER"] = $this->teacher_model->getTeacherDetails(intval($teacher_id->TEACHER_ID));
				$course["USER"] = $this->user_model->getUserByID(intval($teacher_id->TEACHER_ID));		
				$course["RELATED_COURSES"]=$this->course_model->getRelatedCourses($course["COURSE"]->CATEGORY_ID,$course_id);	
				
				
				if(!is_null($course))
				{
					//$this->course_model->getCourseByCategory();
					$this->load->view('course/course_home',$course);		
				}
			}
			else
			{
				redirect('course_dashboard_controller/course_home?course_id='.$course_id);
			}
		}
		else
		{
			redirect('course_dashboard_controller/course_home?course_id='.$course_id);
		}
		
	}
	function courses()
	{
		
		$this->load->model('course_model');		
		$courses=$this->course_model->getAllCourses();		
		$course_group["categories"]= $this->course_model->getCourseByCategoryGroup();	
		
		/*
		foreach($course_group as $course)
		{
			foreach($course as $single_course)
			{	
				echo $this->course_model-> getCourseById($single_course->CATEGORY_ID);
			}						
		}
		*/		
		if(!is_null($course_group))
		{
			$this->load->model('category_model');
			$this->load->view('course/courses',$course_group);			
			if(!is_null($courses))
			{			
				foreach ($courses as $course)
				{
					$this->load->view('course/courses_wrapper',$course);
				}
			}
		}
		else
		{
			echo " No Course Found";
		}
		$this->load->view('course/courses_wrapper_end');
	}
	function past_course()
	{		
		$this->load->model('course_model');		
		$courses=$this->course_model->getPastCourses();
		$course_group["categories"]= $this->course_model->getCourseByCategoryGroup();		
		//$category_id=$course_group->CATEGORY_ID;		
		if(!is_null($course_group))
		{
			$this->load->model('category_model');
			$this->load->view('course/courses',$course_group);
			/*
			foreach($course_group as $course)
			{
				
				//$category_name=$this->category_model->getCategoryByID($course->CATEGORY_ID);
			}		
		    */ 
			if(!is_null($courses))
			{
				foreach ($courses as $course)
				{
					$this->load->view('course/courses_wrapper',$course);
				}
			}
		}
		else
		{
			echo " No Course Found";
		}
		$this->load->view('course/courses_wrapper_end');
		/*
		$this->load->view('course/courses');
		$this->load->model('course_model');		
		
		if(! is_null($courses))
		{
			foreach ($courses as $course)
			{
				$this->load->view('course/courses_wrapper',$course);
			}
		}
		else
		{
			echo "No Course To View";
		}
		$this->load->view('course/courses_wrapper_end');
		*/
	}	
	function new_course()
	{
		
		$this->load->model('course_model');		
		$courses=$this->course_model->getNewCourses();
		$course_group["categories"]= $this->course_model->getCourseByCategoryGroup();		
		//$category_id=$course_group->CATEGORY_ID;		
		if(!is_null($course_group))
		{
			$this->load->model('category_model');
			$this->load->view('course/courses',$course_group);
			/*
			foreach($course_group as $course)
			{
				
				//$category_name=$this->category_model->getCategoryByID($course->CATEGORY_ID);
			}		
		    */ 
			if(!is_null($courses))
			{
				foreach ($courses as $course)
				{
					$this->load->view('course/courses_wrapper',$course);
				}
			}
		}
		else
		{
			echo " No Course Found";
		}
		$this->load->view('course/courses_wrapper_end');
		/*
		$this->load->view('course/courses');
		$this->load->model('course_model');		
		
		if(! is_null($courses))
		{
			foreach ($courses as $course)
			{
				$this->load->view('course/courses_wrapper',$course);
			}
		}
		else
		{
			echo "No Course To View";
		}
		$this->load->view('course/courses_wrapper_end');
		*/
		
	}
	function category()
	{	
		$category_id=$this->input->get('cat_id');
		$this->load->model('course_model');		
		$courses=$this->course_model->getCourseByCategory($category_id);		
		$course_group["categories"]= $this->course_model->getCourseByCategoryGroup();		
		//$category_id=$course_group->CATEGORY_ID;		
		if(!is_null($course_group))
		{
			$this->load->model('category_model');
			$this->load->view('course/courses',$course_group);
			/*
			foreach($course_group as $course)
			{
				
				//$category_name=$this->category_model->getCategoryByID($course->CATEGORY_ID);
			}		
		    */ 
			if(!is_null($courses))
			{			
				foreach ($courses as $course)
				{
					$this->load->view('course/courses_wrapper',$course);
				}
			}
		}
		else
		{
			echo " No Course Found";
		}
		$this->load->view('course/courses_wrapper_end');
	
		
	}
	function course_reg()
	{
		/*		 
		$data =array(
			'COURSE_ID' => intval($this->input->get('course_id')),
			'STUDENT_ID' =>intval($this->session->userdata('USERID'))
			);		
		*/
		$this->load->model('student_model');
		$course_id=$this->input->get('course_id');
		$isRegistered = $this->student_model->registerForCourse(intval($course_id),intval($this->session->userdata('USERID')));
        if($isRegistered) {
            redirect('Course_dashboard_controller/week_lectures?course_id='.$course_id);
        } else {
            echo "can not register.. check course_controller/course_reg";
        }
		
		
	}

   
} 
/* End of file index_controller.php */
/* Location: ./application/controllers/index_controller.php */