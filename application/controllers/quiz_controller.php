<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quiz_controller extends CI_Controller {
        
    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url','form'));
    }
    function quiz_script() {
        $this->load->model('quiz_model');
        $this->load->model('question_model');
        $this->load->model('choice_model');
        
        $quiz_id = $this->input->get('quiz_id');
        $all_questions = $this->question_model->getQuestionsOfQuiz($quiz_id);
        $data['quiz_id'] = $quiz_id;
        $this->load->view('quiz/quiz_script_top',$data);
        //var_dump($all_questions);
        foreach ($all_questions as $key =>$question) {
            $data['question'] = $question;
            $data['q_count'] = $key+1;
            $all_choices = $this->choice_model->getChoicesOfQuestion($question->ID);
            //var_dump($all_choices);
            $data['choice1'] = $all_choices[0];
            $data['choice2'] = $all_choices[1];
            $data['choice3'] = $all_choices[2];
            $data['choice4'] = $all_choices[3];
            //echo $all_choices[3]->ID;
            $this->load->view('quiz/each_question',$data);
        }
        
        $this->load->view('quiz/quiz_script_bottom');
    }
    
    function evaluate_quiz() {
         $this->load->model('quiz_model');
        $this->load->model('question_model');
        $this->load->model('quiz_result_model');
        
        $quiz_id = $this->input->get('quiz_id');
        
        if($this->input->post('submit'))
        {
            $quiz_id = $this->input->get('quiz_id');
            
            $all_questions = $this->question_model->getQuestionsOfQuiz($quiz_id);
            
            $total_qsn = sizeof($all_questions);
            $correct = 0;
            $incorrect = 0;
            
            foreach ($all_questions as $question) {
                $submitted_id = $this->input->post('qsn'.$question->ID);
                $correct_id = $this->question_model->getCorrectChoiceId($question->ID);
                if($submitted_id != $correct_id)
                {
                    $incorrect++;
                } else {
                    $correct++;
                }
            }
            $grade = "";
            if($correct>=5) $grade = "A+";
            else if($correct==4) $grade = "A";
            else  $grade = "B";
            $marks = $correct*4;
            $data = array(
                "QUIZ_ID"=>$quiz_id,
                "STUDENT_ID"=> intval($this->session->userdata('USERID')),
                "MARKS"=>$marks,
                "GRADE"=> $grade
            );
            $isInserted = $this->quiz_result_model->insertQuizResult($data);
            if($isInserted) {
                //echo "result added";
                $data['total_question']= $total_qsn;
                $data['correct_ans']= $correct;
                $this->load->view('quiz/quiz_result',$data);
            } else {
                echo "result cannot be onserted";
            }
            
        }
        
    }
    
}
