<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Profile_controller extends CI_Controller
{
	
	
	function __construct()
	{
		parent::__construct();

		$this->load->helper(array('form', 'url'));		
		$this->load->library('security');			
		$this->load->library('form_validation');
			
	}	
	function index()
	{			
		echo "Welcome to the secret index";			
	}
	
	function profile()
	{			
		$user_id=$this->session->userdata('USERID');	
		$this->load->model('user_model');
		$this->load->model('teacher_model');
		
		if(!is_null($user_id))
		{
			if($this->session->userdata('USERROLE') == 2)
			{
				$data["USER"]=$this->user_model->getUserByID($user_id);
				$data["TEACHER"]=$this->teacher_model->getTeacherDetails($user_id);
				$this->load->view('profile/profile',$data);
			}			
			else
			{
				redirect('home_controller/home');
			}
			
		}
		else
		{
			redirect('home_controller/home');
		}
	}
	
}
?>