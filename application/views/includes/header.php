<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css'); ?>" type="text/css" media="screen"charset="utf-8"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>" type="text/css" media="screen"charset="utf-8"/>
	
    
    <script src="<?php echo base_url('assets/js/jquery.js'); ?>"></script> 
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script> 
    
        
</head>
<body>
    
    
<div class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        
            <a class="navbar-brand" href="<?php echo site_url("home_controller/home") ?>">Online Course Portal</a>
        </div>
		
		
		<?php
		$this->load->helper(array('form','url'));   
		echo form_open('home_controller/search',array('class'=>'navbar-form pull-left form-search'));?>       
		 <div class="form-group">
			  <input type="text" name="search-item" class="form-control" placeholder="Search by Title">
        </div>
        <button type="submit" class="btn btn-default">Search</button>
		<?php echo form_close();?>
		
        <div class="collapse navbar-collapse "id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li ><a href="<?php echo site_url("course_controller/courses");?>">Courses</a></li>
				<?php 
					if( $this->session->userdata('ISLOGIN') )
					{
						echo 
						'<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">'.$this->session->userdata['USERNAME'].' <b class="caret"></b></a>							
							 <ul class="dropdown-menu">';
							if($this->session->userdata('USERROLE') == 2 )//user as teacher
							{								
								if($this->session->userdata('USERCURRENTROLE') == 2 )
								{
									echo '<li><a href="'.base_url().'index.php/role_change_controller/student'.'">Use As Student</a></li>
									      <hr>
										  <li><a href="'.base_url().'index.php/course_controller/create_new_course'.'">Create Course</a></li>
										  <li><a href="'.base_url().'index.php/profile_controller/profile'.'">Profile</a></li>										  
										  <li><a href="'.base_url().'index.php/dashboard_controller/teacher'.'">Dashboard</a></li>
										  
									';									
								}
								else if ($this->session->userdata('USERCURRENTROLE') == 1)
								{
									echo '<li><a href="'.base_url().'index.php/role_change_controller/teacher'.'">Use As Teacher</a></li>
										  <hr>
										  <li><a href="'.base_url().'index.php/dashboard_controller/student'.'">Dashboard</a></li>
									';									
								}
								
							}
							else if ($this->session->userdata('USERROLE') == 1 ) // student
							{
								echo '<li><a href="'.site_url('/dashboard_controller/student').'">Dashboard</a></li>';
							}
							else if ($this->session->userdata('USERROLE') == 0 ) //admin
							{
								echo '<li><a href="'.site_url('/dashboard_controller/admin').'">Dashboard</a></li>';
							}
							
							echo 
								'<li><a href="'.site_url('authentication_controller/signout').'">Sign out</a></li>
							 </ul>
						</li>' ;
					}
					else
					{
						echo 
							'<li><a href="'.site_url('authentication_controller/signin').'">Sign In</a></li>
							<li><a href="'.site_url('authentication_controller/signup').'">Sign Up</a></li>
							';
					}
				?>
            </ul>
        </div>
    </div>
      
</div>


