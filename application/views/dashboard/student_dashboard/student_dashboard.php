<?php $this->load->view('includes/header')?>

<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->

<!-- all contents will reside here -->
            
            <div class="row">
                <div class="col-md-4">
                    <button class="btn btn-link" data-toggle="modal" data-target="#myModal">edit</button>
                    <ul class="list-group">
                        
                        <li class="list-group-item">
                            full name
                            
                            <span >
                                <h3><?php echo $USERNAME; ?></h3>
                            </span>
                            
                        </li>
                        
                        <li class="list-group-item">
                            Email
                            
                            <span >
                                <h4><?php echo $EMAIL; ?></h4>
                            </span>

                        </li>
                    </ul>
                </div>
                <div class="col-md-8">
                     <div class="row bottom-line" >
                        <h3>Your courses</h3>
                     </div></br>
                     