<?php $this->load->view('includes/header')?>

<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->

<!-- all contents will reside here -->
            
            <div class="row">
                <div class="col-md-4">
                    
                    <ul class="list-group">
                        
                        <li class="list-group-item">
                            full name
                            <span class="pull-right">
                                (<a href="#">edit</a>)
                            </span>
                            <span >
                                <h3><?php echo $USERNAME; ?></h3>
                            </span>
                            
                        </li>
                        
                        <li class="list-group-item">
                            Email
                            <span class="pull-right">
                                (<a href="#">edit</a>)
                            </span>
                            <span >
                                <h4><?php echo $EMAIL; ?></h4>
                            </span>

                        </li>
                    </ul>
                </div>
                <div class="col-md-8">
                     <div class="row bottom-line" >
                        <h3>Your courses</h3>
                     </div></br>
                      
                    <div class="row course-list-item" > <!--start of each list item-->
                        <div class="col-md-3">
                            <!-- course image -->
                            <img src="<?php echo base_url('assets/img/logo.png')?>" />
                        </div>
                        <div class="col-md-6">
                                <span><h3><a href="#">Course name</a></h3></span>
                                <span><h5><a href="#">instructor name</a></h5></span>
                                <span><h5><a href="#">institution name</a></h5></span>
                        </div>
                        <div class="col-md-3">
                                <span><h4>start time:3 march,2014</h4></span>
                                <span><h4>4 weeks long</h4></span>
                                <a  class="btn btn-default btn-sm" href="#" role="button">View Course Record</a>
                        </div>
                    </div></br> <!--end of each list item-->                             
                     
                </div>
            </div>

<!-- end of all contents -->

        </div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div> 
</div>

<?php $this->load->view('includes/footer')?>