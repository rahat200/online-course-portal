                </div>
            </div>
                     
                
<!-- end of all contents -->

        </div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div> 
</div>



<?php $this->load->view('includes/footer')?>


     
     
<!-- start of user info edit form/modal dialog -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        
        <?php echo form_open('dashboard_controller/edit_student_info',array('class'=>'form-horizontal'));?>           
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Edit User Info</h4>
      </div>
      <div class="modal-body">
        
        <div class="form-group">
                    <label for="inputEmail" class="col-md-2 control-label">Email</label>
                    <div class="col-md-10">                    
                        <input type="text" class="form-control" id="inputEmail" name="inputEmail" placeholder="Email" value="<?php echo set_value('inputEmail'); ?>">   
                        <?php echo form_error('inputEmail','<p class="text-danger">','</p>'); ?>
                    </div>
                </div>                   
                
                 <div class="form-group">
                    <label for="inputFullname" class="col-md-2 control-label">Full name:</label>
                    <div class="col-md-10">                    
                        <input type="text" class="form-control" id="inputFullname" name="inputFullname" placeholder="Full name" value="<?php echo set_value('inputFullname'); ?>">                                   
                        <?php echo form_error('inputUFullname','<p class="text-danger">','</p>'); ?>
                    </div>
                </div>
        
        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      <?php echo form_close(); ?>
                    

<!-- start of user info edit form/modal dialog -->



