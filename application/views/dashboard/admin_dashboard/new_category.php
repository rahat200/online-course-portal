
<!-- all contents will reside here -->


    </br>
    <!-- end header -->
    
    <div class="row">
        <!-- form container -->
        <div class="col-md-2"></div>
        <div class="col-md-8">

            <?php echo form_open_multipart('dashboard_controller/add_category',array('class'=>'form-horizontal'));?>
                
                <!--Add A Category-->
                <div class="form-group">
                    <label for="inputCategory" class="col-md-2 control-label">Category Name:</label>
                    <div class="col-md-10">
                      <input type="text" class="form-control" id="inputCategory" name="inputCategory" placeholder="Category Name" value="<?php echo set_value('inputCategory'); ?>" required>
                       <?php echo form_error('inputCategory','<p class="text-danger">','</p>'); ?>
                    </div>
                </div>                
                 <div class="form-group">
                   <div class="col-lg-10 col-lg-offset-2">
                      <input type="submit" name="submit" class="btn btn-success" value="Add">
                  </div>
                 </div>
            
            <?php echo form_close();?>
        
        </div>
        
        <div class="col-md-2"></div>
     </div>  <!--end form container -->
	 

</br></br>
                </div>
            </div>
		
<!-- end of all contents -->

        </div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div> 
</div>

<?php $this->load->view('includes/footer')?>
