<?php $this->load->view('includes/header')?>

<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->

<!-- all contents will reside here -->            
            <div class="row">
                <div class="col-md-4">
                    <br>
                    <ul class="list-group">
                        
                        <li class="list-group-item">
                            full name
                           
                            <span >
                                <h3><?php echo $USERNAME; ?></h3>
                            </span>
                            
                        </li>
                        
                        <li class="list-group-item">
                            Email
                            
                            <span >
                                <h4><?php echo $EMAIL; ?></h4>
                            </span>

                        </li>
                    </ul>
                </div>
				<!-- all contents will reside here -->
				<div class="row bg-primary">
					<ul class="nav nav-pills">
					<?php 	
						$selected = $SELECTED;
						if($selected == "TEACHERREQUEST")
						{
							$active_class_tr="active";
							$active_class_ac="";
							$active_class_vr="";
						}
						else if($selected == "ADDCATEGORY")
						{
							$active_class_tr="";
							$active_class_ac="active";
							$active_class_vr="";
						}
						else
						{
							$active_class_tr="";
							$active_class_ac="";
							$active_class_vr="active";
						}
						
					?>
						<li <?php echo "class=".$active_class_tr;?> ><a href= '<?php echo site_url("dashboard_controller/admin"); ?>' >Teacher Request</a></li>
						<li <?php echo "class=".$active_class_ac;?> ><a href= '<?php echo site_url("dashboard_controller/add_category"); ?>' >Add A category</a></li>
						<li <?php echo "class=".$active_class_vr;?> ><a href= '<?php echo site_url("dashboard_controller/report"); ?>' >View Report</a></li>
					</ul>
				</div>
				<br>
                <div class="col-md-8">
                     <div class="row bottom-line" >
                        <h3><?php echo $TAB_TITLE ;?></h3>
                     </div></br>
                     