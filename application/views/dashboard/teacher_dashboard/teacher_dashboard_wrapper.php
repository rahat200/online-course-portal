			<div class="row course-list-item" > <!--start of each list item-->                    
                        <div class="col-md-3">
                            <!-- course image -->
                            <img height="100%" width="100%" src="<?php echo $course->IMAGE_PATH;?>" />
                        </div>
                        <div class="col-md-6">
                                <span><h3><a href="<?php echo site_url('course_dashboard_controller/course_home?course_id='.$course->ID); ?>"><?php echo $course->TITLE; ?></a></h3></span>
                                <span><h5><a href="#"><?php echo $teacher->FULL_NAME; ?></a></h5></span>                                
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <span><p>start on:<?php echo $course->START_DATE;?></p></span>
                                <span><p><?php echo $course->COURSE_LENGTH_WEEK; ?> weeks</p></span>
                            </div>
                            <div class="row">
                                 <a  class="btn btn-default pull-right btn-sm" href="<?php echo site_url('course_dashboard_controller/course_home?course_id='.$course->ID); ?>" role="button">
                                     View Course</a>
                            </div>
                                
                        </div>
            </div>
			</br> <!--end of each list item-->                             
                    