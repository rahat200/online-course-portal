<?php $this->load->view('includes/header')?>

<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->

<!-- all contents will reside here -->
            
            <div class="row">
                <div class="col-md-4">
                    
                    <ul class="list-group">
                        
                        <li class="list-group-item">
                            Full Name

                            <span >
                                <h3><?php echo $USERNAME; ?></h3>
                            </span>
                            
                        </li>
                        
                        <li class="list-group-item">
                            Email
                            
                            <span >
                                <h4><?php echo $EMAIL; ?></h4>
                            </span>

                        </li>
                    </ul>
					<div class="col-md-12 ">
						<a href="<?php echo site_url('course_controller/create_new_course'); ?>" role="button" class="btn btn-primary">
							Add New Course
						</a>						
					</div>
                </div>
				
					
				
						
                <div class="col-md-8"></br>
                     <div class="row bottom-line" >
                        <h3>Your courses</h3>
                     </div></br>
                     