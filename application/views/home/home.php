<?php $this->load->view('includes/header')?>

<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->


<!-- all contents will reside here -->
        <div class="row home-banner">		
            <h1 style="font-size:60px;">Online course portal</h1>
        </div>
		
		<?php 	
			echo '<br>';
			if($this->session->userdata('ISLOGIN') && $this->session->userdata('USERROLE') != 2 )//check first whether the registered user is not teacher
			{
			echo ' 
				<div class="row">
					<div class="pull-right"> 
						<a href="'.			
						site_url('dashboard_controller/apply')
						.'" class="btn btn-primary"> Join As Teacher </a>
					</div>
				</div>';
			}
			
		?>

       