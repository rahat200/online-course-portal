<?php  $this->load->view('includes/header')?>

<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->
		
		<div class="row">
                <div class="col-md-3">
                    <ul class="list-group">
                        <?php							
							if($selected == "all")
							{
								$selectedteacher = '';	
								$selectedcategory = '';
								$selectedall='text-success';
							}
							else if ($selected == "teacher")
							{
								$selectedteacher = 'text-success';	
								$selectedcategory = '';	
								$selectedall='';
							}
							else if ($selected == "category")
							{
								$selectedteacher = '';	
								$selectedcategory = 'text-success';
								$selectedall='';	
							}
							echo '<br>';
							echo "<li class='list-group-item'><a href='".site_url('home_controller/search?search_item='.$search_item)."'  class='text-primary {$selectedall}'>Title </a>";
							echo "<li class='list-group-item'><a href='".site_url('home_controller/search_teacher?search_item='.$search_item)."'  class='text-primary {$selectedteacher}'>Teacher </a>";
							echo "<li class='list-group-item'><a href='".site_url('home_controller/search_category?search_item='.$search_item)."'  class='text-primary {$selectedcategory}'>Category </a>";
														
                        ?>
                          
                    </ul> 
                    
                </div>
			</div>
	
		<?php 
			if(!is_null($courses))
			{
				foreach($courses as $course)
				{
					echo '
					<div class="span7">
						<div class="panel panel-info">
							<div class="panel-heading">
							  <span><h3><a href="'.site_url("course_controller/course?course_id=".$course->ID).'">'.$course->TITLE.'</a></h3></span>
							</div>
							<div class="panel-body">
							<p>
								
								<span><h5><a href="'.site_url("course_controller/course?course_id=".$course->ID).'">'.$course->DESCRIPTION.'</a></h5></span>									
							</p>
							<p>
								<span><h5>'.$course->COURSE_POSTED_DATE.'</a></h5></span>	
							</p>
							</div>				
							<div class="row">
								<div class="col-md-12 ">
									<a href="'.site_url("course_controller/course?course_id=".$course->ID).'" class="btn btn-info pull-right">
										Go To ->
									</a>
								</div>
							</div>
							
							
						</div>
					</div>';
				}
			}
		?>
		
		</div>
		<br>
	</div>
</div>

<?php   $this->load->view('includes/footer');?>
		


       