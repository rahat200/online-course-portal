<?php $this->load->view('includes/header')?>

<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->


<!-- all contents will reside here -->
        <div class="row home-banner">		
            <h1 style="font-size:60px;">Online course portal</h1>
        </div>
		
        <div class="row">
			<div class="pull-right"> 
				<a href="<?php echo site_url('dashboard_controller/apply');?>" class="btn btn-primary"> Join As Teacher </a>
			</div>
        </div>
        <div class="row">
                <div class="col-md-12">
                  <h3>Most Recent</h3>
                </div>
        </div></br>
        <!-- first row -->
        <div class="row">
                <!-- related course container -->
				<?php 
					if(!is_null($recent))
					{
						foreach($recent as $course)
						{
							echo '
							<div class="col-md-4">
								<div class="col-md-1"></div>
								<div class="col-md-10 canvas" style="height: 150px;">
									<img src="#" />
									<a class="text-center" href="'.site_url("course_controller/course?course_id=".$course->ID).'">'.$course->ID.'</a>
								</div>
								
								<div class="col-md-1"></div>
							</div>';
						}
					}
				?>
        </div><hr>
        <!-- end of first row -->
         <div class="row">
                <div class="col-md-12">
                  <h3>Most Popular</h3>
                </div>
        </div></br>
        <div class="row">
                <!-- related course container -->
				<?php 				 
					if(!is_null($popular))
					{
						foreach($popular as $course)
						{
							echo '
							<div class="col-md-4">
								<div class="col-md-1"></div>
								<div class="col-md-10 canvas" style="height: 150px;">
									<img src="#" />
									<a class="text-center" href="#">'.$course->TITLE.'</a>
								</div>
								
								<div class="col-md-1"></div>
							</div>';
						}
					}
				?>
        </div><hr>
        <!-- end of 2nd row -->

<!-- end of all contents -->

        </div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div> 
</div>

<?php $this->load->view('includes/footer')?>