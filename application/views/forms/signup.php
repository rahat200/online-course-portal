<?php 
	$this->load->view('includes/header'); 
	if( $this->session->userdata('ISLOGIN') )
	{
		redirect();
	}
?>
<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->
    
    
    <div class="row" >
        <!-- sign up header -->
        <div class="col-md-1"></div>
        <div class="col-md-10 bottom-line">
                <h1>Sign Up</h1>
                </br>
        </div>
        <div class="col-md-1"></div>
    </div>
    </br>
    <!-- end sign up header -->
    
    <div class="row">
        <!-- form container -->
          

            <?php echo form_open('authentication_controller/signup',array('class'=>'form-horizontal'));?>           
                <div class="form-group">
                    <label for="inputEmail" class="col-md-2 control-label">Email</label>
                    <div class="col-md-10">                    
						<input type="text" class="form-control" id="inputEmail" name="inputEmail" placeholder="Email" value="<?php echo set_value('inputEmail'); ?>">	
						<?php echo form_error('inputEmail','<p class="text-danger">','</p>'); ?>
                    </div>
                </div>                   
				
				 <div class="form-group">
                    <label for="inputUsername" class="col-md-2 control-label">Username</label>
                    <div class="col-md-10">                    
						<input type="text" class="form-control" id="inputUsername" name="inputUsername" placeholder="Username" value="<?php echo set_value('inputUsername'); ?>">									
						<?php echo form_error('inputUsername','<p class="text-danger">','</p>'); ?>
                    </div>
                </div>
				
				<div class="form-group">
                    <label for="inputFullName" class="col-md-2 control-label">Full Name</label>
                    <div class="col-md-10">                    
						<input type="text" class="form-control" id="inputFullName" name="inputFullName" placeholder="Full Name" value="<?php echo set_value('inputFullName'); ?>">									
						<?php echo form_error('inputUsername','<p class="text-danger">','</p>'); ?>
					</div>
                </div>
				
				<div class="form-group">
                    <label for="inputPassword" class="col-md-2 control-label">Password</label>
                    <div class="col-md-10">
						<input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="Password" value="<?php echo set_value('inputPassword'); ?>">
						<?php echo form_error('inputPassword','<p class="text-danger">','</p>'); ?>
					</div>
                </div>
                
                <div class="form-group">
                    <label for="inputRetypePassword" class="col-md-2 control-label">Re-type Password</label>
                    <div class="col-md-10">
						<input type="password" class="form-control" id="inputRetypePassword" name="inputRetypePassword" placeholder="Re-type Password" value="<?php echo set_value('inputRetypePassword'); ?>">
						<?php echo form_error('inputRetypePassword','<p class="text-danger">','</p>'); ?>
					</div>
                </div>
                
                <div class="form-group">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-10">											
						<label>
							all the fields are required
                        </label>
						<div class="checkbox">
							<label>
							  <input type="checkbox"> I Agree the terms & Condition						  
							</label>
						</div>
                    </div>
                  </div>
                  
                 <div class="form-group">
                   <div class="col-lg-10 col-lg-offset-2">
                      <button type="submit" class="btn btn-success">Submit</button>					  
                  </div>
                 </div>
            <?php echo form_close(); ?>
        </div>
        <div class="col-md-1"></div>
     </div>  <!--end form container -->

</br></br>


</div> <!--end of container column-->
        <div class="col-md-1"></div>
    

</div>
<?php 
	//echo form_close(); 
	$this->load->view('includes/footer');
?>