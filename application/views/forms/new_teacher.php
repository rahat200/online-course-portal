<?php 
	$this->load->view('includes/header');
?>

<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->


<!-- all contents will reside here -->

    <div class="row" >
        <!--  header title -->
        <div class="col-md-1"></div>
        <div class="col-md-10 bottom-line">
                <h1>Apply For Teacher</h1>
                </br>
        </div>
        <div class="col-md-1"></div>
    </div>
    </br>
    <!-- end header -->
    
    <div class="row">
        <!-- form container -->
        <div class="col-md-2"></div>
        <div class="col-md-8">

            <?php echo form_open_multipart('dashboard_controller/apply',array('class'=>'form-horizontal'));?>
                
                <!--FieldExpert-->
                <div class="form-group">
                    <label for="inputFieldExpert" class="col-md-3 control-label">*Field of Expertise:</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" id="inputFieldExpert" name="inputFieldExpert" placeholder="Expertise" value="<?php echo set_value('inputFieldExpert'); ?>">
                       <?php echo form_error('inputFieldExpert','<p class="text-danger">','</p>'); ?>
                    </div>
                </div>
                <!-- Phone -->
                <div class="form-group">
                    <label for="inputPhone" class="col-md-3 control-label">Phone :</label>
                    <div class="col-md-9">
                      <input type="text"  class="form-control" id="inputPhone" name="inputPhone" placeholder="Phone Number" value="<?php echo set_value('inputPhone'); ?>">
                        <?php echo form_error('inputPhone','<p class="text-danger">','</p>'); ?>
                    </div>
                </div>
                <!-- Birth date -->
                <div class="form-group">
                    <label for="inputDate" class="col-md-3 control-label">Birth date:</label>
                    <div class="col-md-9">
                      <input type="date" name="inputDate" class="form-control" id="inputDate" placeholder="ex: 01-jan-14" >
                      <?php echo form_error('inputDate','<p class="text-danger">','</p>'); ?>
                    </div>
                </div>
                <!-- Institution Name -->
                <div class="form-group">
                    <label for="inputInstitutionName" class="col-md-3 control-label">Institution Name:</label>
                    <div class="col-md-9">
                      <textarea  class="form-control" name="inputInstitutionName" rows="3" id="inputInstitutionName" ><?php echo set_value('inputInstitutionName'); ?></textarea>
                    </div>
                </div>                                           
                <!-- Teacher Profile Picture -->
                <div class="form-group">
                    <label for="inputImage" class="col-md-3 control-label">*Picture :</label>
                    <div class="col-md-6">
                      <input type="file" name="inputImage" class="form-control" id="inputImage" >
                    </div>
                </div>         
				<!-- Select Gender -->
                <div class="form-group">
                    <label for="inputGender" class="col-md-3 control-label">Select Gender: </label>
                    <div class="col-md-5">
                        <select name="inputGender" class="form-control input-sm col-md-3" id="inputGender">
                                <option value="Male">Male</option>
                                <option value="Femail">Female</option>
                        </select>
                    </div>
                </div>                
                  
                 <div class="form-group">
                   <div class="col-lg-10 col-lg-offset-2">
                      <input type="submit" name="submit" class="btn btn-success" value="Apply">
                  </div>
                 </div>
            
            <?php echo form_close();?>
        
        </div>
        
        <div class="col-md-2"></div>
     </div>  <!--end form container -->

</br></br>


<!-- end of all contents -->

        </div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div> 
</div>

<?php $this->load->view('includes/footer')?>