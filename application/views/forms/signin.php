<?php 
	$this->load->view('includes/header'); 
	if( $this->session->userdata('ISLOGIN') )
	{
		redirect();
	}
?>
<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->
    
    
    <div class="row" >
        <!-- sign in header -->
        <div class="col-md-1"></div>
        <div class="col-md-10 bottom-line">
                <h1>Sign In</h1>
                </br>
        </div>
        <div class="col-md-1"></div>
    </div>
    </br>
    <!-- end sign in header -->
    
    <div class="row">
        <!-- form container -->
        <div class="col-md-1"></div>
        <div class="col-md-5" style=" border-right: 1px ridge #AAAAAA;">
		<?php echo form_open('authentication_controller/signin',array('class'=>'form-horizontal'));?>            
                <div class="form-group">
                    <label for="inputEmail" class="col-md-2 control-label">Email</label>
                    <div class="col-md-10">                    						
						<input type="text" class="form-control" id="inputEmail" name="inputEmail" placeholder="Email" value="<?php echo set_value('inputEmail'); ?>">	
						<?php echo form_error('inputEmail','<p class="text-danger">','</p>'); ?>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="inputPassword" class="col-md-2 control-label">Password</label>
                    <div class="col-md-10">                      
					  <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="Password" value="<?php echo set_value('inputPassword'); ?>">
					  <?php echo form_error('inputPassword','<p class="text-danger">','</p>'); ?>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-10">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox"> Remember me
                        </label>
                      </div>
                    </div>
                  </div>
                  
                 <div class="form-group">
                   <div class="col-lg-10 col-lg-offset-2">
                      <button type="submit" class="btn btn-success">Submit</button>					  
                  </div>
                 </div>
            </form>
        </div>
        <div class="col-md-5">
            <p>No Account Yet?</p>
            <a href="<?php echo site_url('authentication_controller/signup');?>">sign up </a>today
        </div>
        <div class="col-md-1"></div>
     </div>  <!--end form container -->

</br></br>


</div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div> 

</div>
<?php 
	echo form_close(); 
	$this->load->view('includes/footer');
?>