<?php $this->load->view('includes/header')?>

<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->


<!-- all contents will reside here -->

    <div class="row" >
        <!--  header title -->
        <div class="col-md-1"></div>
        <div class="col-md-10 bottom-line">
                <h1>Create New Course</h1>
                </br>
        </div>
        <div class="col-md-1"></div>
    </div>
    </br>
    <!-- end sign in header -->
    
    <div class="row">
        <!-- form container -->
        <div class="col-md-1"></div>
        <div class="col-md-10">

            <?php echo form_open_multipart('course_controller/create_new_course',array('class'=>'form-horizontal'));?>
                
                <!-- title -->
                <div class="form-group">
                    <label for="inputTitle" class="col-md-3 control-label">*Course title:</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" id="inputTitle" name="course-title" placeholder="Course Title"value="<?php echo set_value('course-title'); ?>">
                       <?php echo form_error('course-title','<p class="text-danger">','</p>'); ?>
                    </div>
                </div>
                <!-- description -->
                <div class="form-group">
                    <label for="inputDescription" class="col-md-3 control-label">*Short Description:</label>
                    <div class="col-md-9">
                      <textarea  class="form-control" rows="3" id="inputDescription" name="course-description"><?php echo set_value('course-description'); ?></textarea>
                        <?php echo form_error('course-description','<p class="text-danger">','</p>'); ?>
                    </div>
                </div>
                <!-- about course -->
                <div class="form-group">
                    <label for="inputAbout" class="col-md-3 control-label">*About this course:</label>
                    <div class="col-md-9">
                      <textarea  class="form-control" rows="3" id="inputAbout" name="about-course"><?php echo set_value('about-course'); ?></textarea>
                        <?php echo form_error('about-course','<p class="text-danger">','</p>'); ?>
                    </div>
                </div>
                <!-- start date -->
                <div class="form-group">
                    <label for="inputDate" class="col-md-3 control-label">*Start date:</label>
                    <div class="col-md-9">
                      <input type="date" name="course-start-date" class="form-control" id="inputDate" placeholder="ex: 01-jan-14" value="<?php echo set_value('course-start-date');?>" >
                      <?php echo form_error('course-start-date','<p class="text-danger">','</p>'); ?>
                    </div>
                </div>
                <!-- prerequisite -->
                <div class="form-group">
                    <label for="inputPrerequisite" class="col-md-3 control-label">Pre-requisite:</label>
                    <div class="col-md-9">
                      <textarea  class="form-control" name="course-prerequisite" rows="3" id="inputPrerequisite" ><?php echo set_value('course-prerequisite'); ?></textarea>
                    </div>
                </div>                
                <!-- syllabus -->
                <div class="form-group">
                    <label for="inputSyllabus" class="col-md-3 control-label">Syllabus:</label>
                    <div class="col-md-9">
                      <textarea  class="form-control" name="course-syllabus" rows="3" id="inputSyllabus" ><?php echo set_value('course-syllabus'); ?></textarea>
                    </div>
                </div>                
                <!-- course-format -->
                <div class="form-group">
                    <label for="inputFormat" class="col-md-3 control-label">Course Format:</label>
                    <div class="col-md-9">
                      <textarea  class="form-control" name="course-format" rows="3" id="inputFormat" ><?php echo set_value('course-format'); ?></textarea>
                    </div>
                </div>                

                <!-- category -->
                <div class="form-group">
                    <label for="select-category" class="col-md-3 control-label">*Select Category: </label>
                    <div class="col-md-6">
                        <select name="category-id" class="form-control input-sm col-md-4" id="select-category">
                                <?php
                                foreach ($categories as $category) {
                                    echo "<option value='{$category->ID}'>{$category->NAME}</option>";
                                }
                                ?>
                        </select>
                    </div>
                </div> 
 
                <!-- corse length/duration -->
                <div class="form-group">
                    <label for="course-length" class="col-md-3 control-label">*Course-Duration: </label>
                    <div class="col-md-6">
                        <select name="course-length" class="form-control input-sm col-md-3" id="course-length">
                                <?php
                                for ($i=1; $i <11 ; $i++) { 
                                    echo "<option value='{$i}'>{$i} Week</option>";
                                }
                                ?>
                        </select>
                    </div>
                </div> 
                               
                <!-- effort per week -->
                <div class="form-group">
                    <label for="course-effort" class="col-md-3 control-label">*Effort per week: </label>
                    <div class="col-md-6">
                        <select name="course-effort" class="form-control input-sm col-md-3" id="course-effort">
                                <?php
                                for ($i=1; $i <25 ; $i++) { 
                                    echo "<option value='{$i}'>{$i} hours</option>";
                                }
                                ?>
                        </select>
                    </div>
                </div> 
                               
                <div class="form-group">
                    <label for="select-language" class="col-md-3 control-label">*Select Language: </label>
                    <div class="col-md-6">
                        <select name="language" class="form-control input-sm col-md-3" id="select-language">
                                <option value="Bangla">Bangla</option>
                                <option value="English">English</option>
                        </select>
                    </div>
                </div> 
                
                <!-- course image -->
                <div class="form-group">
                    <label for="inputImage" class="col-md-3 control-label">Course Banner:</label>
                    <div class="col-md-5">
                      <input type="file" name="inputImage" class="form-control" id="inputImage" >
                    </div>
                </div>
                <!-- course video -->
                <div class="form-group">
                    <label for="inputVideo" class="col-md-3 control-label">Course Intro Video:</label>
                    <div class="col-md-5">
                      <input type="file" name="inputVideo" class="form-control" id="inputVideo" >
                      <p>video must be in mp4 format and max size 5MB</p>
                    </div>
                </div>               
                  
                 <div class="form-group">
                   <div class="col-lg-10 col-lg-offset-3">
                      <input type="submit" name="submit" class="btn btn-success" value="Create New Course">
                  </div>
                 </div>
                 
            <?php echo form_close();?>
        
        </div>
        
        <div class="col-md-1"></div>
     </div>  <!--end form container -->

</br></br>


<!-- end of all contents -->

        </div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div> 
</div>

<?php $this->load->view('includes/footer')?>