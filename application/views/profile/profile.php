<?php $this->load->view('includes/header')?>

<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->


        <div class="row">
            <div class="col-md-3">
                <img height="80%" width="80%" src="<?php echo $TEACHER->PICTURE_PATH; ?>" />
            </div>
            <div class="col-md-9">
                <h2 class="text-left"><?php echo $USER->FULL_NAME;?></h2>
                <p class="text-left"><?php echo $TEACHER->INSTITUTION_NAME;?></p><hr>
                
                <h5 class="text-left"><b>Email: </b><i><?php echo $USER->EMAIL;?></i></h5>
                <h5 class="text-left"><b>Contact no: </b><?php echo $TEACHER->PHONE;?></h5>
                <h5 class="text-left"><b>Field of expertise: </b><?php echo $TEACHER->FIELD_OF_EXPERTISE;?></h5>
                
            </div>
        </div>

        
		
        
        
        
       
       



        </div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div> 
</div>

<?php $this->load->view('includes/footer')?>