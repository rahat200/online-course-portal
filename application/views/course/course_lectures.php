<?php $this->load->view('includes/header')?>

<div class="container">   <!--top title(course title) bar container-->
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10"> <!-- container column-->
            <div class="row">
                
                <div class="col-md-2">
                    <img src="<?php echo base_url('assets/img/logo.png')?>" />
                </div>
                <div class="col-md-10"> <!--course name/title-->
                    <h2>Software Engineering:Design Pattern</h2>
                    by: <span> Sunny Rahman</span>
                </div>
                
            </div>
        </div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div> 
</div>




<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->


<!-- all contents will reside here -->
            <div class="row bottom-line">
                <ul class="nav nav-pills">
                    <li><a href="#">Home</a></li>
                    <li class="active"><a href="#">Lectures</a></li>
                    <li><a href="#">Quizes</a></li>
                    <li><a href="#">Assignments</a></li>
                </ul>
            </div></br>
            
            
            <div class="row">
                <div class="col-md-3">
                    <!-- leftside navigation for weeks/lectures -->
                    
                    <ul class="list-group">
                          <li class="list-group-item active">
                              <a href="#" class="text-info ">Week 1</a>
                              <span class="badge">2</span>
                          </li>
                          <li class="list-group-item">
                              <a href="#">Lecture 1</a>
                          </li>
                          <li class="list-group-item">
                              <a href="#">Lecture 2</a>
                          </li>
                          <li class="list-group-item">
                              <a href="#" class="text-info">Week 2</a>
                          </li>
                          <li class="list-group-item" >
                              <a href="#" class="text-info">Week 3</a>
                          </li>
                    </ul>
                    <!-- alternative -->
                    <div class="list-group">alternative
                        <a href="#" class="list-group-item ">Week 1</a>
                        <a href="#" class="list-group-item active">Week 1</a>
                        <a href="#" class="list-group-item ">Week 1</a>
                    </div>
                    
                </div>
                <div class="col-md-9">
                    <!-- lecture content area -->
                    <div class="row " >
                        <h3 class="well">Week3/Lecture 1</h3>
                    </div>
                    
                    <div class="row">
                        <h3 >Topic: topic name</h3>
                    </div></br>
                    
                    <div class="row">
                        <video width="90%" height="100%" controls>
                            <source src="" type="video/mp4">
                            <source src="movie" type="video/ogg">
                        </video>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-4">
                            <h3>Handouts:</h3>
                        </div>
                        <div class="col-md-8"></br></br>
                            <ul>
                                <li><a href="#">software design</a></li>
                                <li><a href="#">software design</a></li>
                                <li><a href="#">software design</a></li>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-4">
                            <h3>References:</h3>
                        </div>
                        <div class="col-md-8"></br></br>
                            <ul>
                                <li><a href="#">software design</a></li>
                                <li><a href="#">software design</a></li>
                                <li><a href="#">software design</a></li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
            

<!-- end of all contents -->

        </div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div> 
</div>

<?php $this->load->view('includes/footer')?>