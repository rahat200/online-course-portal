<?php $this->load->view('includes/header');

?>

<div class="container">
    <div class="row"> 
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->
<!-- all contents reside here -->
        </br>
            <div class="row" >
                <!-- title and intro video container -->
                <div class="col-md-7" >
                    <!-- title of the course -->
                    
                        <h1><?php echo $COURSE->TITLE ;?></h1>
                        </br>
                        <div style=" border-bottom: 1px ridge #AAAAAA;"></div>
                        </br>
                        <!-- introduction about the course -->
                        <p class="lead"><i>
                           <?php echo $COURSE->DESCRIPTION ; ?>
                        </i></p>
                    <!-- end of title and video -->
                </div>
                <div class="col-md-5">
                    <!-- intro video -->
                    <video width="100%" height="250px" controls>
                      <source src="<?php echo $COURSE->INTRO_VIDEO_PATH;?>" type="video/mp4">
                    </video>
                </div>
            </div>
            <!-- end of row -->
            <div style=" border-bottom: 1px solid #AAAAAA;"></div>
            
            </br></br>
            
            <div class="row">
                <!-- description and right side bar container -->
                
                <div class="col-md-8">
                    <!--  description -->
                    
                    <h3>About the course</h3> 
                    </br>
                    <p class="text-justify">
                        <?php echo $COURSE->ABOUT_COURSE; ; ?>
                    </p>
                    </br></br>
                    
                    <h3>Course Syllabus</h3>
                    </br>
                    <p class="text-justify">
						<?php echo $COURSE->SYLLABUS ; ?>
                    </p>
                    </br></br>
                    
                    <h3>Prerequisite</h3>
                    </br>
                    <p class="text-justify">
						<?php echo $COURSE->PRE_REQ_COURSE ; ?>                         
                    </p>
                    </br></br>
                    
                    <h3>Course Format</h3>
                    </br>
                    <p class="text-justify">
						<?php echo $COURSE->COURSE_FORMAT ; ?>                                                 
                    </p>
                    </br></br>
                    <!--end of discussion  -->
                </div>
                
                <div class="col-md-4">
                    <!-- right side bar -->         
					<?php 
						if( $this->session->userdata('ISLOGIN') && $this->session->userdata('USERCURRENTROLE') == 1  && $this->session->userdata('USERID') != $TEACHER->ID )
						{
						/*
						echo '
							<button type="button" class="btn btn-primary btn-lg btn-block" onclick="location.href ='.site_url("course_controller/course_reg?course_id=".$COURSE->ID).';">
								Register for this Course
							</button></br>
						';
						*/
						echo ' 
						<div class="row">
							<div class="pull-right"> 
								<a href="'.			
								site_url('course_controller/course_reg?course_id='.$COURSE->ID)
								.'" class="btn btn-primary"> Register for this Course </a>
							</div>
						</div>';
						}
					?>
					<!--
					<button type="button" class="btn btn-primary btn-lg btn-block" onclick="location.href ='./course_reg?course_id=<?php echo $COURSE->ID;?>';">
								Register for this Course
							</button></br>
                    -->
					
                    
                    <!-- course at a glance -->
                    <table class="table">
                        <tr class="active">
                            <td>Course Start</td>
                            <td><?php echo $COURSE->START_DATE  ; ?> </td>
                        </tr>
                        <tr class="active">
                            <td>Course Length</td>
                            <td><?php echo $COURSE->COURSE_LENGTH_WEEK  ; ?> </td>
                        </tr>
                        <tr class="active">
                            <td>Estimated effort</td>
                            <td><?php echo $COURSE->EFFORT_PER_WEEK ; ?> </td>
                        </tr>
                        <tr class="active">
                            <td>Language</td>
                            <td><?php echo $COURSE->COURSE_LANGUAGE ; ?> </td>
                        </tr>
                    </table>
                    <!-- instructor -->
                    <h3>Instructor:</h3></br>
                    <div class="col-md-4">image</div>
                    <div class="col-md-8">
                        <a href="#"><strong><?php 
						echo $USER->FULL_NAME; 
						?></strong></a>
						<br>
						<p>  
                        <?php echo $TEACHER->INSTITUTION_NAME; ?></p>
                    </div>
                </div>
            </div>
            <!-- end of row -->
            
            <div style=" border-bottom: 1px ridge #AAAAAA;"></div>
            </br>
			
            <!-- related course header -->
            <div class="row">
                <div class="col-md-12">
                  <h3>Related Courses</h3>
                </div>
            </div>
            
            </br>
            </br>
            <div class="row">
                <!-- related course container -->
				<?php 
				if(!is_null($RELATED_COURSES))
				{
					$i=0;
					foreach($RELATED_COURSES as $COURSE)
					{
						echo '
						<div class="col-md-4">
							<div class="col-md-1"></div>
							<div class="col-md-10 course-grid-box" >
								<img height="100%" width="100%" src="'.$COURSE->IMAGE_PATH.'" />
								<a class="text-center" href="'.site_url('course_controller/course?course_id='.$COURSE->ID).'">'.$COURSE->TITLE.'</a>
							</div>							
							
							<div class="col-md-1"></div>
						</div>
							';
						
							
					}
				}
                ?>

                
            
            </div>
        </br></br>


            
        </div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div>    
</div>

<?php $this->load->view('includes/footer');?>