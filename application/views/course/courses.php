<?php $this->load->view('includes/header')?>

<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->

<!-- all contents will reside here -->
            
            <div class="row bottom-line" >
                <!-- all courses header -->
                  <h3>Courses</h3>
                  </br>
                
            </div> 
            
            </br>
 
            <div class="row bottom-line">
                <!-- navigation area -->
                
                <div class="col-md-6">
				<!--
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label for="select-category" class="col-md-4 control-label">Select Category: </label>
                            <div class="col-md-8">
                                <select class="form-control input-sm col-md-10" id="select-category">
                                <option value="all-subjects">all subjects</option>
                                <option value="cs-algorithm">CS:Algorithm</option>
                                <option value="cs-compiler">CS:Compiler</option>
                                <option value="cs-networking">CS:Networking</option>
                                <option value="cs-basic">CS:Basic</option>
                                </select>
                            </div>
                        </div>
                    </form>
                	-->    
                </div>
			
                <div class="col-md-3 col-md-offset-3">
                    <ul class="list-inline">
                        <li> <a href="<?php echo site_url('course_controller/courses'); ?>">all</a> </li>
                        <!-- <li> <a href="#">current</a> </li> -->
                        <li> <a href="<?php echo site_url('course_controller/new_course'); ?>">new</a> </li>
                        <li> <a href="<?php echo site_url('course_controller/past_course'); ?>">past</a> </li>
                    </ul>
                </div>
            </div>  <!--end of navigation area-->            

            </br>
            
            <div class="row">  
                <div class="col-md-3">
                    <!-- category list area -->
                    <ul class="list-group">
						<?php
							if(!is_null($categories))
							{								
								foreach ($categories as $category) {
								
									echo '<li class="list-group-item">'.
									'<a href="'.site_url('course_controller/category?cat_id=').$category->CATEGORY_ID.'">'.$category->NAME.'</a>'.
									'<span class="badge">'.$category->CATEGORY_NUMBER.'</span>'.
									'</li>';
								}
							}
                        ?>
                    </ul>
                    
                </div>
<div class="col-md-9">