	
		<div class="row course-list-item" > <!--start of each list item-->
			<div class="col-md-3">
			<!-- course image -->
			<img height="90%" width="100%" src="<?php echo $IMAGE_PATH;?>" />
			</div>			
				<div class="col-md-6">
					<span><h5><a  href="<?php echo site_url("course_controller/course?course_id=".$ID); ?>"><?php echo $TITLE;?></a></h5></span>
				</div>
				<div class="col-md-3">
					<span><p><?php echo  $START_DATE ; ?></p></span>
					<span><h5><?php echo  $COURSE_LENGTH_WEEK ; ?> weeks</h5></span>
					<a  class="btn btn-primary pull-right" href="<?php echo site_url("course_controller/course?course_id=".$ID); ?>" role="button">Learn more</a>
				</div>
		</div> <!--end of each list item-->

