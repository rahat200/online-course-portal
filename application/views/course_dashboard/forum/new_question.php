<?php $this->load->view('includes/header')?>
<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->
		
                <div class="col-md-9">
                    <!-- add new lecture form -->
                    <div class="row" >
                        <!--  header title -->
                        <div class="col-md-12 bottom-line">
                                <h3>Ask Question</h3>
                                
                        </div>
                    </div> </br>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <?php 
							echo form_open("course_dashboard_controller/forum_ask?course_id=".$COURSE_ID."&lecture_id=".$LECTURE_ID,array('class'=>'form-horizontal'));
							?>
                
                            <!-- title -->
                            <div class="form-group">
                                <label for="inputQuestion" class="col-md-2 control-label">*Question:</label>
                                <div class="col-md-10">
                                  <input type="text" class="form-control" id="inputQuestion" name="inputQuestion" placeholder="Question"value="<?php echo set_value('inputQuestion'); ?>">
                                   <?php echo form_error('linputQuestion','<p class="text-danger">','</p>'); ?>
                                </div>
                            </div>                                                
                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                  <input type="submit" name="submit" class="btn btn-success" value="ASK">
                                </div>
                            </div>
                            
                            <?php echo form_close();?>
                        </div>
                    </div>                  
                                      
                </div>
		</div> <!--end of container column-->
        <div class="col-md-1"></div>
		</div>
    </div> 
</div>
                
<?php $this->load->view('includes/footer')?>