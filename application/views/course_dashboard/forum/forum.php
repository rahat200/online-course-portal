<?php $this->load->view('includes/header')?>


<div class="container">   <!--top title(course title) bar container-->
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 course-top-bar"> <!-- container column-->
            <div class="row">
                
                <div class="col-md-3">
                    <img height="100%" width="100%" src="<?php echo $course->IMAGE_PATH;?>" />
                </div>
                <div class="col-md-9"> <!--course name/title-->
                    <h2><?php echo $course->TITLE;?></h2>
                    
                </div>
                
            </div>
        </div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div> 
</div>





<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->


<!-- all contents will reside here -->
            <div class="row bg-primary">
                <ul class="nav nav-tabs">
                    <li  ><a href= '<?php echo site_url("Course_dashboard_controller/course_home?course_id={$course->ID}"); ?>' >Home</a></li>
                    <li  ><a href= '<?php echo site_url("Course_dashboard_controller/week_lectures?course_id={$course->ID}"); ?>' >Lectures</a></li>
                    <li  ><a href= '<?php echo site_url("Course_dashboard_controller/course_quizes?course_id={$course->ID}"); ?>' >Quizes</a></li>
                    <li  ><a href= '<?php echo site_url("Course_dashboard_controller/course_assignments?course_id={$course->ID}"); ?>' >Assignments</a></li>
					<li class="active" ><a href= '<?php echo site_url("Course_dashboard_controller/forum?course_id={$course->ID}"); ?>' >Forum</a></li>
					
                </ul>
            </div></br>
            
			<?php  
			if($current != 0)
			{
				echo '				
				<div class="row">
					<div class="col-md-12 ">
						<a href="'.site_url("Course_dashboard_controller/forum_ask?course_id={$course->ID}&lecture_id={$current}").'" class="btn btn-success pull-right">
							Ask Question
						</a>
					</div>
				</div>
				';
			}
			
			?>
            </br>
			
			 <div class="row">
                <div class="col-md-3">
                    <!-- leftside navigation for weeks/lectures -->
                    
                    <ul class="list-group">
                        
                        <?php
							
							if(!is_null($weekly_lectures))
							{		
								if($current == 0)
								{
									$selected = 'text-success';	
								}
								else
								{
									$selected = '';	
								}
								echo "<li class='list-group-item'><a href='".site_url('course_dashboard_controller/forum?course_id='.$course->ID)."'  class='text-primary {$selected}'>All Lectures </a>";
								
								foreach($weekly_lectures as $lecture)
								{
									if($current== $lecture->ID)
									{
										$selected = 'text-success';	
									}
									else
									{
										$selected = '';	
									}
									echo  "<li class='list-group-item'><a href='".site_url('course_dashboard_controller/lecture_forum?course_id='.$lecture->COURSE_ID.'&lecture_id='.$lecture->ID)."'  class='text-primary {$selected}'>".$lecture->TOPIC_NAME ." </a>";
								}
							}
                        ?>
                          
                    </ul> 
                    
        </div> <!--end of col-md-3-->
        <div class="col-md-9">
			



           
