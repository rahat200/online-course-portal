<div class="row">
	<div class="span7">
	  <div class="panel panel-primary">
		<div class="panel-heading ">
		  <h4>Question #<?php echo $questions->ROWNUM;?></h4>
		</div>
		<div class="panel-body">
		  <h4>
			  <i><?php echo $questions->TEXT;?></i> <br><br>
		  </h4>
		  <h5>by <b><?php echo $questions->FULL_NAME;?></b></h5>
		  <h6><?php echo $questions->POSTED_DATE;?></h6>
		</div>
			<?php  
				echo '				
				<div class="row">
					<div class="col-md-12 ">
						<a href="'.site_url("Course_dashboard_controller/forum_answer?course_id={$course->ID}&forum_question_id={$questions->ID}&lecture_id={$questions->LECTURE_ID}").'" class="btn btn-primary btn-sm pull-right">
							give answer
						</a>
					</div>
				</div>
				';
			?>
	  </div>
	</div>
	<div class="span7 ">
	  <div class="panel panel-default">
		<div class="panel-heading">
		  <h4>Answer </h4>
		  <?php if(is_null($answers)) echo "<p class='text-danger'>(Nobody answered yet!)</p>" ?>
		</div>
		<?php 
		if(!is_null($answers))
		{
			foreach($answers as $answer)
			{			
				echo '
				<div class="panel-body">
					<div class="divider">
					  <p>
						  '.$answer->TEXT.'<br><br>
					  </p>
					   <h5">by <b><i>'.$answer->FULL_NAME.'</i></b></h5>
					  <h6>'.$answer->POSTED_DATE.'</h6>
					</div>					
				</div>
				<div style=" border-bottom: 1px solid #AAAAAA;"></div>
				';
			}
		}
		?>
	  </div>
	</div>
</div>