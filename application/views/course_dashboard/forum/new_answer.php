<?php $this->load->view('includes/header')?>
<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->
		
                <div class="col-md-9">
                    <!-- add new lecture form -->
                    <div class="row" >
                        <!--  header title -->
                        <div class="col-md-12 bottom-line">
                                <h3>Respond to Question</h3>
                                
                        </div>
                    </div> </br>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <?php 
							echo form_open("course_dashboard_controller/forum_answer?course_id={$COURSE_ID}&forum_question_id={$FORUM_QUESTION_ID}&lecture_id={$LECTURE_ID}",array('class'=>'form-horizontal'));
							?>
                
                            <!-- title -->
                            <div class="form-group">
                                <label for="inputAnswer" class="col-md-2 control-label">*Answer:</label>
                                <div class="col-md-10">
                                  <input type="text" class="form-control" id="inputAnswer" name="inputAnswer" placeholder="Answer"value="<?php echo set_value('inputAnswer'); ?>">
                                   <?php echo form_error('inputAnswer','<p class="text-danger">','</p>'); ?>
                                </div>
                            </div>                                                
                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                  <input type="submit" name="submit" class="btn btn-success" value="Answer">
                                </div>
                            </div>
                            
                            <?php echo form_close();?>
                        </div>
                    </div>                  
                                      
                </div>
		</div> <!--end of container column-->
        <div class="col-md-1"></div>
		</div>
    </div> 
</div>
                
<?php $this->load->view('includes/footer')?>