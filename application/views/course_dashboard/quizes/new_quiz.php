<?php $this->load->view('includes/header')?>

<div class="container">   <!--top title(course title) bar container-->
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 course-top-bar"> <!-- container column-->
            <div class="row">
                
                <div class="col-md-3">
                    <img height="100%" width="100%" src="<?php echo $course->IMAGE_PATH;?>" />
                </div>
                <div class="col-md-9"> <!--course name/title-->
                    <h2><?php echo $course->TITLE;?></h2>
                    Starts on: <span> <?php echo $course->START_DATE; ?></span>
                </div>
                
            </div>
        </div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div> 
</div>





<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->


<!-- all contents will reside here -->
           <div class="row bg-primary">
                <ul class="nav nav-tabs">
                    <li  ><a href= '<?php echo site_url("Course_dashboard_controller/course_home?course_id={$course->ID}"); ?>' >Home</a></li>
                    <li  ><a href= '<?php echo site_url("Course_dashboard_controller/week_lectures?course_id={$course->ID}"); ?>' >Lectures</a></li>
                    <li class="active" ><a href= '<?php echo site_url("Course_dashboard_controller/course_quizes?course_id={$course->ID}"); ?>' >Quizes</a></li>
                    <li  ><a href= '<?php echo site_url("Course_dashboard_controller/course_assignments?course_id={$course->ID}"); ?>' >Assignments</a></li>
                </ul>
            </div></br>
            
            
            <div class="row">

<!-- start of form fields -->

            <?php echo form_open('Course_dashboard_controller/create_quiz?course_id='.$course->ID,array('class'=>'form-horizontal'));?>
                
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    
                    
                    <div class="row">
                    <div>
                      *Quiz Topic:<input type="text"  name="quiz-topic" value="<?php echo set_value('quiz-topic') ?>">
                       <?php echo form_error('quiz-topic','<p class="text-danger">','</p>'); ?>
                    </div></br>
                    
                    <div>
                        *Select Week:
                        <select name="quiz-week-num">
                            <option value="">select week</option>
                            <?php
                                for ($i=1; $i <=$course->COURSE_LENGTH_WEEK ; $i++) { 
                                    echo "<option value={$i}>week {$i}</option>";
                                }
                             ?>
                        </select>
                        <?php echo form_error('quiz-week-num','<p class="text-danger">','</p>'); ?>
                    </div></br>
                    
                    <div>
                        *Hard-deadline: <input type="date" name="quiz-deadline" placeholder="ex:01-jan-14" />
                        <?php echo form_error('quiz-deadline','<p class="text-danger">','</p>'); ?>
                    </div></br>
                    
                    </div><hr>
                    
                    
                    
                    <div class="row">
                        <!-- question input container -->
                        <h3>Set Questions</h3>
                        <?php
                            for ($i=1; $i <=5 ; $i++) { 
                                echo "Question {$i}: <input type='text' name='qn{$i}' /></br>";
                                for ($j=1; $j <=4 ; $j++) { 
                                    echo "</br>Choice {$j}: <input type='text' name='qn{$i}ch{$j}' />";
                                    echo "<input type='checkbox' name='ans{$i}' value='qn{$i}ch{$j}' />";
                                }
                                echo "</br></br><hr>";
                            }
                            
                        ?>
                    </div>
                    <div class="row">
                    <input type="submit" name="submit" class="btn btn-success" value="Create Quiz">
                     </div>
                </div>

                <div class="col-md-1"></div>
                

            
            <?php echo form_close();?>

<!-- end of form fields -->
            </div>
            

<!-- end of all contents -->

        </div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div> 
</div>

<?php $this->load->view('includes/footer')?>