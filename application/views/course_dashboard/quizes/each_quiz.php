<div class="row course-quiz-container" style="padding: 10px;" >
    <div class="col-md-12">
    <h3 class="well">Quiz On Week<?php echo " ".$quiz->WEEK_NUM;?></h3>
    <table class="table table-striped">
        <tbody>
            <tr>
                <td>Topic</td>
                <td><?php echo $quiz->TOPIC;?></td>
            </tr>
            <tr>
                <td>Marks</td>
                <td><?php echo $quiz->MARKS;?></td>
            </tr>
            <tr>
                <td>Hard Deadline</td>
                <td><?php echo $quiz->HARD_DEADLINE;?></td>
            </tr>
            <tr>
                <td>Marks</td>
            <?php
                if(is_null($marks))
                    echo '<td>0</td>';
                else {
                    echo '<td>{$marks}</td>';
                }
            ?>
            </tr>
        </tbody>
    </table>
	<?php
	if($registeredstudent)
	{
		echo '
		<a target="_blank" href="'.site_url('Quiz_controller/quiz_script?quiz_id='.$quiz->ID).'" class="btn btn-primary btn-sm pull-left">
			Attempt Quiz
		</a>';
	}
    if($teacher->TEACHER_ID == $this->session->userdata('USERID'))
    {
        echo '
        <a target="_blank" href="'.site_url('course_dashboard_controller/delete_quiz?course_id='.$course->ID.'&quiz_id='.$quiz->ID).'" class="btn btn-danger btn-sm pull-left">
            Delete Quiz
        </a>';
    }
	?>
    </div></br> 
                       

    
</div></br>