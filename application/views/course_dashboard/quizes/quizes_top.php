<?php $this->load->view('includes/header')?>

<div class="container">   <!--top title(course title) bar container-->
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 course-top-bar"> <!-- container column-->
            <div class="row">
                
                <div class="col-md-3">
                    <img height="100%" width="100%" src="<?php echo $course->IMAGE_PATH;?>" />
                </div>
                <div class="col-md-9"> <!--course name/title-->
                    <h2><?php echo $course->TITLE;?></h2>
                    Starts on: <span> <?php echo $course->START_DATE; ?></span>
                </div>
                
            </div>
        </div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div> 
</div>





<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->


<!-- all contents will reside here -->
           <div class="row bg-primary">
                <ul class="nav nav-tabs">
                    <li  ><a href= '<?php echo site_url("Course_dashboard_controller/course_home?course_id={$course->ID}"); ?>' >Home</a></li>
                    <li  ><a href= '<?php echo site_url("Course_dashboard_controller/week_lectures?course_id={$course->ID}"); ?>' >Lectures</a></li>
                    <li class="active" ><a href= '<?php echo site_url("Course_dashboard_controller/course_quizes?course_id={$course->ID}"); ?>' >Quizes</a></li>
                    <li  ><a href= '<?php echo site_url("Course_dashboard_controller/course_assignments?course_id={$course->ID}"); ?>' >Assignments</a></li>
                    <li  ><a href= '<?php echo site_url("Course_dashboard_controller/forum?course_id={$course->ID}"); ?>' >Forum</a></li>
                </ul>
            </div></br>
            
            
             <div class="row" >
                <!-- all quizes header -->
                 <div class="col-md-10 col-md-offset-1">
                     <h2>Quizes</h2>
                 </div>
            </div><hr>
            <?php           
                if($this->session->userdata('USERROLE')==2)
                {
                    $teacher_id=$teacher->TEACHER_ID;
                    $user_id=$this->session->userdata('USERID');
                    if($teacher_id==$user_id)
                    {
                        echo '  
                            <div class="row">
                                <div class="col-md-12 ">
                                    <a href="'.site_url("Course_dashboard_controller/create_quiz?course_id={$course->ID}").'" class="btn btn-info pull-right">
                                        Add New Quiz
                                    </a>
                                </div>
                            </div></br> ';
                    }
                }
            
            ?>
            <div class="row">
                <div class="col-md-1">
                    
                </div>
                <div class="col-md-10">