<?php $this->load->view('includes/header')?>

<div class="container">   <!--top title(course title) bar container-->
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 course-top-bar"> <!-- container column-->
            <div class="row">
                
                <div class="col-md-3">
                    <img height="100%" width="100%" src="<?php echo $course->IMAGE_PATH;?>" />
                </div>
                <div class="col-md-9"> <!--course name/title-->
                    <h2><?php echo $course->TITLE;?></h2>
                    Starts on: <span> <?php echo $course->START_DATE; ?></span>
                </div>
                
            </div>
        </div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div> 
</div>





<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->


<!-- all contents will reside here -->
           <div class="row bg-primary">
                <ul class="nav nav-tabs">
                    <li class="active" ><a href= '<?php echo site_url("Course_dashboard_controller/course_home?course_id={$course->ID}"); ?>' >Home</a></li>
                    <li  ><a href= '<?php echo site_url("Course_dashboard_controller/week_lectures?course_id={$course->ID}"); ?>' >Lectures</a></li>
                    <li  ><a href= '<?php echo site_url("Course_dashboard_controller/course_quizes?course_id={$course->ID}"); ?>' >Quizes</a></li>
                    <li  ><a href= '<?php echo site_url("Course_dashboard_controller/course_assignments?course_id={$course->ID}"); ?>' >Assignments</a></li>
                </ul>
            </div></br>
            
            
            <div class="row">

<!-- start of form fields -->

            <?php echo form_open('Course_dashboard_controller/edit_course_home?course_id='.$course->ID,array('class'=>'form-horizontal'));?>
                
                <!-- title -->
                <div class="form-group">
                    <label for="inputTitle" class="col-md-3 control-label">*Course title:</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" id="inputTitle" name="course-title" placeholder="Course Title"value="<?php echo $course->TITLE; ?>">
                       <?php echo form_error('course-title','<p class="text-danger">','</p>'); ?>
                    </div>
                </div>
                <!-- description -->
                <div class="form-group">
                    <label for="inputDescription" class="col-md-3 control-label">*Course Description:</label>
                    <div class="col-md-9">
                      <textarea  class="form-control" rows="3" id="inputDescription" name="course-description"><?php echo $course->DESCRIPTION; ?></textarea>
                        <?php echo form_error('course-description','<p class="text-danger">','</p>'); ?>
                    </div>
                </div>
                <!-- about course -->
                <div class="form-group">
                    <label for="inputAbout" class="col-md-3 control-label">*About this course:</label>
                    <div class="col-md-9">
                      <textarea  class="form-control" rows="3" id="inputAbout" name="about-course"><?php echo $course->ABOUT_COURSE; ?></textarea>
                        <?php echo form_error('about-course','<p class="text-danger">','</p>'); ?>
                    </div>
                </div>
                <!-- start date -->
                <div class="form-group">
                    <label for="inputDate" class="col-md-3 control-label">*Start date:</label>
                    <div class="col-md-9">
                      <input type="text" name="course-start-date" class="form-control" id="inputDate" placeholder="ex: 01-jan-14" value="<?php echo $course->START_DATE;?>">
                      <?php echo form_error('course-start-date','<p class="text-danger">','</p>'); ?>
                    </div>
                </div>
                <!-- prerequisite -->
                <div class="form-group">
                    <label for="inputPrerequisite" class="col-md-3 control-label">Pre-requisite:</label>
                    <div class="col-md-9">
                      <textarea  class="form-control" name="course-prerequisite" rows="3" id="inputPrerequisite" ><?php echo $course->PRE_REQ_COURSE; ?></textarea>
                    </div>
                </div>                
                <!-- syllabus -->
                <div class="form-group">
                    <label for="inputSyllabus" class="col-md-3 control-label">Syllabus:</label>
                    <div class="col-md-9">
                      <textarea  class="form-control" name="course-syllabus" rows="3" id="inputSyllabus" ><?php echo $course->SYLLABUS; ?></textarea>
                    </div>
                </div>                
                <!-- course-format -->
                <div class="form-group">
                    <label for="inputFormat" class="col-md-3 control-label">Course Format:</label>
                    <div class="col-md-9">
                      <textarea  class="form-control" name="course-format" rows="3" id="inputFormat" ><?php echo $course->COURSE_FORMAT; ?></textarea>
                    </div>
                </div>                

                <!-- category -->
                <div class="form-group">
                    <label for="select-category" class="col-md-2 control-label">*Select Category: </label>
                    <div class="col-md-8">
                        <select name="category-id" class="form-control input-sm col-md-4" id="select-category">
                                <?php
                                foreach ($categories as $category) {
                                    if($category->ID == $course->CATEGORY_ID) {
                                        echo "<option value='{$category->ID}' selected>{$category->NAME}</option>";
                                    }
                                    else    
                                        echo "<option value='{$category->ID}'>{$category->NAME}</option>";
                                }
                                ?>
                        </select>
                    </div>
                </div> 
 
                <!-- corse length/duration -->
                <div class="form-group">
                    <label for="course-length" class="col-md-2 control-label">*Course-Duration: </label>
                    <div class="col-md-8">
                        <select name="course-length" class="form-control input-sm col-md-3" id="course-length">
                                <?php
                                for ($i=1; $i <11 ; $i++) {
                                    if($course->COURSE_LENGTH_WEEK == $i)
                                         echo "<option value='{$i}' selected>{$i}</option>";
                                    else
                                        echo "<option value='{$i}'>{$i}</option>";
                                }
                                ?>
                        </select>
                    </div>
                </div> 
                               
                <!-- effort per week -->
                <div class="form-group">
                    <label for="course-effort" class="col-md-2 control-label">*Effort per week: </label>
                    <div class="col-md-8">
                        <select name="course-effort" class="form-control input-sm col-md-3" id="course-effort">
                                <?php
                                for ($i=1; $i <25 ; $i++) { 
                                    if($course->EFFORT_PER_WEEK == $i)
                                         echo "<option value='{$i}' selected>{$i}</option>";
                                    else
                                        echo "<option value='{$i}'>{$i}</option>";
                                }
                                ?>
                        </select>
                    </div>
                </div> 
                               
                <div class="form-group">
                    <label for="select-language" class="col-md-2 control-label">*Select Language: </label>
                    <div class="col-md-8">
                        <select name="language" class="form-control input-sm col-md-3" id="select-language">
                                <option value="Bangla"<?php if($course->COURSE_LANGUAGE=='Bangla') echo 'selected';?>>Bangla</option>
                                <option value="English"<?php if($course->COURSE_LANGUAGE=='English') echo 'selected';?>>English</option>
                        </select>
                    </div>
                </div>                
                  
                 <div class="form-group">
                   <div class="col-lg-10 col-lg-offset-2">
                      <input type="submit" name="submit" class="btn btn-success" value="Update Course">
                  </div>
                 </div>
            
            <?php echo form_close();?>

<!-- end of form fields -->
            </div>
            

<!-- end of all contents -->

        </div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div> 
</div>

<?php $this->load->view('includes/footer')?>