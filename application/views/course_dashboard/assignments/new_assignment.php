<?php 
	$this->load->view('includes/header');
?>

<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->


<!-- all contents will reside here -->

    <div class="row" >
        <!--  header title -->
        <div class="col-md-1"></div>
        <div class="col-md-10 bottom-line">
                <h1>Create New Assignment</h1>
                </br>
        </div>
        <div class="col-md-1"></div>
    </div>
    </br>
    <!-- end header -->
    
    <div class="row">
        <!-- form container -->
        <div class="col-md-2"></div>
        <div class="col-md-8">

            <?php 
				$course_id= $this->input->get('course_id');
				echo form_open_multipart('course_dashboard_controller/create_course_assignment?course_id='.$course_id,array('class'=>'form-horizontal'));?>
                                                    
                
				<div class="form-group">
                    <label for="inputTitle" class="col-md-3 control-label">*Title:</label>
                    <div class="col-md-9">
                      <textarea  class="form-control" name="inputTitle" rows="3" id="inputTitle" required ><?php echo set_value('inputTitle'); ?></textarea>
                    </div>
                </div> 
				
                <div class="form-group">
                    <label for="inputAssignment" class="col-md-3 control-label">*Assignment :</label>
                    <div class="col-md-6">
                      <input type="file" name="inputAssignment" class="form-control" id="inputAssignment" required >
                    </div>
                </div>  
				<div class="form-group">
                    <label for="inputText" class="col-md-3 control-label">Text:</label>
                    <div class="col-md-9">
                      <textarea  class="form-control" name="inputText" rows="3" id="inputText" required ><?php echo set_value('inputText'); ?></textarea>
                    </div>
                </div> 				
				<div class="form-group">
                    <label for="inputStartDate" class="col-md-3 control-label">*Start date:</label>
                    <div class="col-md-9">
                      <input type="date" name="inputStartDate" class="form-control" id="inputStartDate" placeholder="ex: 01-jan-14" required >
                      <?php echo form_error('inputStartDate','<p class="text-danger">','</p>'); ?>
                    </div>
                </div>		
				<div class="form-group">
                    <label for="inputEndDate" class="col-md-3 control-label">*End date:</label>
                    <div class="col-md-9">
                      <input type="date" name="inputEndDate" class="form-control" id="inputEndDate" placeholder="ex: 01-jan-14" required >
                      <?php echo form_error('inputEndDate','<p class="text-danger">','</p>'); ?>
                    </div>
                </div>		
				 <div class="form-group">
				   <div class="col-lg-10 col-lg-offset-2">
					  <input type="submit" name="submit" class="btn btn-success" value="Create">
				  </div>
				 </div>
            
            <?php echo form_close();?>
        
        </div>
        
        <div class="col-md-2"></div>
     </div>  <!--end form container -->

</br></br>


<!-- end of all contents -->

        </div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div> 
</div>

<?php $this->load->view('includes/footer')?>