<?php $this->load->view('includes/header')?>

<div class="container">   <!--top title(course title) bar container-->
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 course-top-bar"> <!-- container column-->
            <div class="row">
                
                <div class="col-md-3">
                    <img height="100%" width="100%" src="<?php echo $course->IMAGE_PATH;?>" />
                </div>
                <div class="col-md-9"> <!--course name/title-->
                    <h2><?php echo $course->TITLE;?></h2>
                    by: <span><?php echo $user->FULL_NAME ;?></span>
                </div>
                
            </div>
        </div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div> 
</div>





<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->


<!-- all contents will reside here -->
           <div class="row bg-primary">
                <ul class="nav nav-tabs">
                    <li ><a href= '<?php echo site_url("Course_dashboard_controller/course_home?course_id={$course->ID}"); ?>' >Home</a></li>
                    <li  ><a href= '<?php echo site_url("Course_dashboard_controller/week_lectures?course_id={$course->ID}"); ?>' >Lectures</a></li>
                    <li  ><a href= '<?php echo site_url("Course_dashboard_controller/course_quizes?course_id={$course->ID}"); ?>' >Quizes</a></li>
                    <li class="active" ><a href= '<?php echo site_url("Course_dashboard_controller/course_assignments?course_id={$course->ID}"); ?>' >Assignments</a></li>
					<li  ><a href= '<?php echo site_url("Course_dashboard_controller/forum?course_id={$course->ID}"); ?>' >Forum</a></li>
                </ul>
            </div></br>
            
           <?php 
			
			if($this->session->userdata('USERID') == $teacher_details->ID)
			{
			echo '<div class="row">
				<a href="		
				 '.site_url('course_dashboard_controller/create_course_assignment?course_id='.$course->ID).'" role="button" class="btn btn-primary pull-right">
					Add New Assignment
				</a>
					
				
				<a href="		
				 '.site_url('course_dashboard_controller/view_submission?course_id='.$course->ID).'" style="margin-right:3px;" role="button" class="btn btn-primary pull-right">
					View Submission
				</a>
				</div>';
			}
			?>
			
			<br>
			<br>
			<br>
			
			<?php 		
  
				if(!is_null($assignments))
				{
					foreach($assignments as $assignment)
					{
						echo '
						<div class="row course-list-item" > <!--start of each list item-->
																		
									
							<div class="col-md-12">
								<span><p>'.$assignment->TITLE.'</p></span>								
							
							
								<span><h5><a href="'.$assignment->CONTENT_PATH.'">DOWNLOAD</a></h5></span>
								<span><h5>SUBMISSION START:'.$assignment->START_DATE.'</a></h5></span>
								<span><h5>DEADLINE :'.$assignment->END_DATE.'</a></h5></span>
								<span><h5>'.$assignment->TEXT.'</a></h5></span>';
								if($registeredstudent && !is_null($registeredstudent))
								{
									echo '<a href="'.site_url('course_dashboard_controller/submitAssignment?course_id='.$course->ID.'&assignment_id='.$assignment->ID).'" role="button" class="btn btn-primary pull-right">
										Submit Assignment
									</a>';
								}
								echo '
							</div>
						</div> 
						';
					}
				}
			
			?>
            

<!-- end of all contents -->

        </div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div> 
</div>

<?php $this->load->view('includes/footer')?>