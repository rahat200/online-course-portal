<?php 
	$this->load->view('includes/header');
?>

<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->


<!-- all contents will reside here -->

    <div class="row" >
        <!--  header title -->
        <div class="col-md-1"></div>
        <div class="col-md-10 bottom-line">
                <h1>Submit Assignment</h1>
                </br>
        </div>
        <div class="col-md-1"></div>
    </div>
    </br>
    <!-- end header -->
    
    <div class="row">
        <!-- form container -->
        <div class="col-md-2"></div>
        <div class="col-md-8">

            <?php 
				$course_id= $this->input->get('course_id');
				$assignment_id= $this->input->get('assignment_id');
				echo form_open_multipart('course_dashboard_controller/submitAssignment?course_id='.$course_id.'&assignment_id='.$assignment_id,array('class'=>'form-horizontal'));?>
                <div class="form-group">
                    <label for="inputAssignment" class="col-md-3 control-label">*Assignment :</label>
                    <div class="col-md-6">
                      <input type="file" name="inputAssignment" class="form-control" id="inputAssignment" required >
                    </div>
                </div>  
					
				 <div class="form-group">
				   <div class="col-lg-10 col-lg-offset-2">
					  <input type="submit" name="submit" class="btn btn-success" value="Submit">
				  </div>
				 </div>
            
            <?php echo form_close();?>
        
        </div>
        
        <div class="col-md-2"></div>
     </div>  <!--end form container -->

</br></br>


<!-- end of all contents -->

        </div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div> 
</div>

<?php $this->load->view('includes/footer')?>