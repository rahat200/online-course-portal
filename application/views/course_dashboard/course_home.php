<?php $this->load->view('includes/header')?>


<div class="container">   <!--top title(course title) bar container-->
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 course-top-bar"> <!-- container column-->
            <div class="row">
                
                <div class="col-md-3">
                    <img height="100%" width="100%" alt="course banner" src="<?php echo $course->IMAGE_PATH;?>"  />
                </div>
                <div class="col-md-9"> <!--course name/title-->
                    <h2><?php echo $course->TITLE;?></h2>
                    by: <span> <?php echo $user->FULL_NAME?></span>
                </div>
                
            </div>
        </div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div> 
</div>





<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->


<!-- all contents will reside here -->
            <div class="row bg-primary">
                <ul class="nav nav-tabs">
                    <li class="active" ><a href= '<?php echo site_url("Course_dashboard_controller/course_home?course_id={$course->ID}"); ?>' >Home</a></li>
                    <li  ><a href= '<?php echo site_url("Course_dashboard_controller/week_lectures?course_id={$course->ID}"); ?>' >Lectures</a></li>
                    <li  ><a href= '<?php echo site_url("Course_dashboard_controller/course_quizes?course_id={$course->ID}"); ?>' >Quizes</a></li>
                    <li  ><a href= '<?php echo site_url("Course_dashboard_controller/course_assignments?course_id={$course->ID}"); ?>' >Assignments</a></li>
					<li  ><a href= '<?php echo site_url("Course_dashboard_controller/forum?course_id={$course->ID}"); ?>' >Forum</a></li>
					
                </ul>
            </div></br>
            
			<?php  
				if($this->session->userdata('USERROLE')==2)
				{
					$teacher_id=$teacher->TEACHER_ID;
					$user_id=$this->session->userdata('USERID');
					if($teacher_id==$user_id)
					{
						echo '				
						<div class="row">
							<div class="col-md-12 ">
								<a href="'.site_url("Course_dashboard_controller/edit_course_home?course_id={$course->ID}").'" class="btn btn-success pull-right">
									Edit Course Information
								</a>
							</div>
						</div>
						';
					}
				}
			?>
            </br>
            <div class="row bottom-line" >
                <!-- title and intro video container -->
                <div class="col-md-7" >
                    <!-- title of the course -->
                    
                        <h1><?php echo $course->TITLE; ?></h1>
                        </br>
                        <div style=" border-bottom: 1px ridge #AAAAAA;"></div>
                        </br>
                        <!-- intro description about the course -->
                        <p class="lead"><i>
                           <?php echo $course->DESCRIPTION ; ?>
                        </i></p>
                    <!-- end of title and video -->
                </div>
                <div class="col-md-5" >
                    <!-- intro video -->
                    <video width="100%" height="250px" controls style="border: 3px solid #2C3E50;border-radius: 5px;">
                      <source src="<?php echo $course->INTRO_VIDEO_PATH;?>" type="video/mp4">
                      
                    </video>
                </div>
            </div>
            <!-- end of row -->
    <hr>
            
            </br></br>
            
            <div class="row">
                <!-- details and right side bar container -->
                
                <div class="col-md-8">
                    <!--  details -->
                    
                    <h3>About the course</h3> 
                    </br>
                    <p class="text-justify">
                        <?php echo $course->ABOUT_COURSE; ?> </p>
                    </br></br>
                    
                    <h3>Course Syllabus</h3>
                    </br>
                    <p class="text-justify">
                        <?php echo $course->SYLLABUS;?>
                    </p>
                    </br></br>
                    
                    <h3>Prerequisite</h3>
                    </br>
                    <p class="text-justify">
                        <?php echo $course->PRE_REQ_COURSE;?>
                    </p>
                    </br></br>
                    
                    <h3>Course Format</h3>
                    </br>
                    <p class="text-justify">
                        <?php echo $course->COURSE_FORMAT;?>
                    </p>
                    </br></br>
                    <!--end of discussion  -->
                </div>
                
                <div class="col-md-4">
                    <!-- right side bar -->
                    
                    
                    <!-- course at a glance -->
                    <table class="table">
                        <tr class="active">
                            <td>Course Start</td>
                            <td><?php echo $course->START_DATE;?></td>
                        </tr>
                        <tr class="active">
                            <td>Course Length</td>
                            <td><?php echo $course->COURSE_LENGTH_WEEK;?></td>
                        </tr>
                        <tr class="active">
                            <td>Estimated effort</td>
                            <td><?php echo $course->EFFORT_PER_WEEK;?></td>
                        </tr>
                        <tr class="active">
                            <td>Language</td>
                            <td><?php echo $course->COURSE_LANGUAGE;?></td>
                        </tr>
                    </table>
                    <!-- instructor -->
                    <h3>Teacher:</h3></br>
                    <div class="col-md-4">image</div>
                    <div class="col-md-8">
                        <a href="#"><strong><?php echo $user->FULL_NAME?></strong></a>
                        <p><?php echo $teacher_details->INSTITUTION_NAME?></p>
                    </div>
                </div>
            </div>
            <!-- end of row -->



           

<!-- end of all contents -->

        </div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div> 
</div>

<?php $this->load->view('includes/footer')?>