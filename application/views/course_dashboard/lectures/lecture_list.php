                <div class="col-md-9">
                    <!-- add new lecture button -->
					<?php  
						if($this->session->userdata('USERROLE')==2)
						{
							$teacher_id=$teacher->TEACHER_ID;
							$user_id=$this->session->userdata('USERID');
							if($teacher_id==$user_id)
							{
								echo '	
								<div class="row ">
									<div class="col-md-12">
									<a href="'.site_url("Course_dashboard_controller/create_new_lecture?course_id={$course->ID}&week_num={$current_week}").'" class="btn btn-info pull-right">
										Add New Lecture
									</a>
									</div>
								</div></br>';
							}
						}
					?>
                    
                    <!-- lecture content area -->
                    <div class="row " >
                        <div class="col-md-12">
                            <?php
                            if(is_null($lecture_list))
                                echo "<h3 class='text-danger well'>No Lectures added yet!<h4>";
                            else {
                                foreach ($lecture_list as $key => $lecture) {
                                    echo "<div class='row well'>";
                                        
                                            echo "<h3 class='pull-left'><a href='"
                                            .site_url("Course_dashboard_controller/week_lectures?course_id={$course->ID}&week_num={$current_week}&lecture_id={$lecture->ID}")
                                            ."'>week ".$current_week.": ".$lecture->TOPIC_NAME. "</a></h3>";
                                        
                                        //echo "<h4 class='pull-right text-primary'>Topic:".($lecture->TOPIC_NAME)."</h4>";
                                    echo "</div>";
                                }
                            }
                            ?>
                            
                        </div>
                    </div>
                </div>