
                <div class="col-md-9">
                    <!-- add new lecture form -->
                    <div class="row" >
                        <!--  header title -->
                        <div class="col-md-12 bottom-line">
                                <h3>Create New Lecture</h3>
                                
                        </div>
                    </div> </br>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo form_open_multipart("course_dashboard_controller/create_new_lecture?course_id={$course->ID}&week_num={$current_week}",array('class'=>'form-horizontal'));?>
                
                            <!-- title -->
                            <div class="form-group">
                                <label for="inputTopic" class="col-md-2 control-label">*Lecture topic:</label>
                                <div class="col-md-10">
                                  <input type="text" class="form-control" id="inputTopic" name="lecture-topic" placeholder="Topic of the lecture"value="<?php echo set_value('lecture-topic'); ?>">
                                   <?php echo form_error('lecture-topic','<p class="text-danger">','</p>'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputOverview" class="col-md-2 control-label">*Lecture overview:</label>
                                <div class="col-md-10">
                                    <textarea  class="form-control" name="lecture-overview" rows="3" id="inputOverview" ><?php echo set_value('course-overview'); ?></textarea>
                                   <?php echo form_error('lecture-overview','<p class="text-danger">','</p>'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputVideo" class="col-md-2 control-label">*Lecture Video:</label>
                                <div class="col-md-5">
                                  <input type="file" name="inputVideo" class="form-control" id="inputVideo">
                                  <p>video must be in mp4 format and max size 5MB</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                  <input type="submit" name="submit" class="btn btn-success" value="Create New Lecture">
                                </div>
                            </div>
                            
                            <?php echo form_close();?>
                        </div>
                    </div> 
                    
                                      
                </div>
                
