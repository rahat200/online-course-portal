
                <div class="col-md-9">
                    <!-- lecture content area -->
					<?php  
			
					$teacher_id=$teacher->TEACHER_ID;
					$user_id=$this->session->userdata('USERID');
					if($teacher_id==$user_id)
					{
						echo '	
						<div class="row">
							<div class="col-md-12 ">
								<a href="'.site_url("course_dashboard_controller/edit_lecture?course_id={$course->ID}&week_num={$current_week}&lecture_id={$lecture->ID}").'" class="btn btn-primary pull-right">
									Edit
								</a>
								<a href="'.site_url("course_dashboard_controller/delete_lecture?course_id={$course->ID}&week_num={$current_week}&lecture_id={$lecture->ID}").'" class="btn btn-danger pull-right" style="margin-right:3px;">
									Delete
								</a>
							</div>
						</div></br>';
					}
					?>
                    
                    <div class="row well" >
                        <h3 class="text-success"><?php echo "Week ".$lecture->WEEK_NUM.": ".$lecture->TOPIC_NAME;?></h3>
                        <p><i>Last Updated: <?php echo $lecture->UPDATE_TIME;?></i></p>
                    </div>
                    
                    <div class="row">
                        <h3>Overview:</h3></br>
                        <h4  class=""><i><?php echo $lecture->LECTURE_OVERVIEW;?></i></h4>
                    </div></br>
                    
                    <div class="row">
                        <video width="90%" height="400px" controls>
                            <source src="<?php if(!is_null($lecture_video)) echo $lecture_video->CONTENT_PATH;?>" type="video/mp4">
                        </video>
                    </div></br></br></br>
                    
                    <div class="row">
                        <div class="col-md-4">
                            <h3>Handouts:</h3>
                        </div>
                        <div class="col-md-8"></br></br>
                            <ul>
                                <li><a href="#">not available yet</a></li>
                                <li><a href="#">not available yet</a></li>
                                <li><a href="#">not available yet</a></li>
                            </ul>
                        </div>
                    </div></br></br>
                    
                  <!--  <div class="row">
                        <div class="col-md-4">
                            <h3>References:</h3>
                        </div>
                        <div class="col-md-8"></br></br>
                            <ul>
                                <li><a href="#">not available yet</a></li>
                                <li><a href="#">not available yet</a></li>
                                <li><a href="#">not available yet</a></li>
                            </ul>
                        </div>
                    </div> -->
                    
                </div>
