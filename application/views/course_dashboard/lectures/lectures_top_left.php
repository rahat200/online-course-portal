<?php $this->load->view('includes/header')?>

<div class="container">   <!--top title(course title) bar container-->
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 course-top-bar"> <!-- container column-->
            <div class="row">
                
                <div class="col-md-3">
                    <img height="" width="100%" src="<?php echo $course->IMAGE_PATH;?>" />
                </div>
                <div class="col-md-9"> <!--course name/title-->
                    <h2><?php echo $course->TITLE;?></h2>
                    Starts On:<span><?PHP echo $course->START_DATE ;?></span>
                </div>
                
            </div>
        </div> <!--end of container column-->
        <div class="col-md-1"></div>
    </div> 
</div>





<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 canvas"> <!-- container column-->


<!-- all contents will reside here -->
           <div class="row bg-primary">
                <ul class="nav nav-tabs">
                    <li  ><a href= '<?php echo site_url("Course_dashboard_controller/course_home?course_id={$course->ID}"); ?>' >Home</a></li>
                    <li class="active" ><a href= '<?php echo site_url("Course_dashboard_controller/week_lectures?course_id={$course->ID}"); ?>' >Lectures</a></li>
                    <li  ><a href= '<?php echo site_url("Course_dashboard_controller/course_quizes?course_id={$course->ID}"); ?>' >Quizes</a></li>
                    <li  ><a href= '<?php echo site_url("Course_dashboard_controller/course_assignments?course_id={$course->ID}"); ?>' >Assignments</a></li>
					<li  ><a href= '<?php echo site_url("Course_dashboard_controller/forum?course_id={$course->ID}"); ?>' >Forum</a></li>
                </ul>
            </div></br>
            
            
            <div class="row">
                <div class="col-md-3">
                    <!-- leftside navigation for weeks/lectures -->
                    
                    <ul class="list-group">
                        
                        <?php
                        
                        for ($i=1; $i <=$course->COURSE_LENGTH_WEEK ; $i++) {
                            $selected = $current_week == $i?"text-success":""; 
                            $lec_num = 0;
                            if(!is_null($lec_count)) {
                                foreach ($lec_count as $value) {
                                    if($value['WEEK_NUM'] == $i) {
                                        $lec_num = $value['LEC_COUNT'];
                                        break;
                                    }
                                }
                            }
                            echo "<li class='list-group-item'>";
                            echo "<a href='".site_url("Course_dashboard_controller/week_lectures?course_id={$course->ID}&week_num={$i}")."' class='text-primary {$selected}'>Week ".$i."</a>";
                            
                            echo "<span class='badge'>".$lec_num."</span>";
                            
                            }
                            
                        ?>
                          
                    </ul> 
                    
                </div>