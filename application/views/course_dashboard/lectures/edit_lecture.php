
                <div class="col-md-9">
                    <!-- add new lecture form -->
                    <div class="row" >
                        <!--  header title -->
                        <div class="col-md-12 bottom-line">
                                <h3>Edit This Lecture </h3>
                                
                        </div>
                    </div> </br>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo form_open("course_dashboard_controller/edit_lecture?course_id={$course->ID}&week_num={$current_week}&lecture_id={$lecture->ID}",array('class'=>'form-horizontal'));?>
                
                            <!-- topic -->
                            <div class="form-group">
                                <label for="inputTopic" class="col-md-2 control-label">*Lecture topic:</label>
                                <div class="col-md-10">
                                  <input type="text" class="form-control" id="inputTopic" name="lecture-topic" placeholder="Topic of the lecture"value="<?php echo $lecture->TOPIC_NAME; ?>">
                                   <?php echo form_error('lecture-topic','<p class="text-danger">','</p>'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputOverview" class="col-md-2 control-label">*Lecture overview:</label>
                                <div class="col-md-10">
                                    <textarea  class="form-control" name="lecture-overview" rows="3" id="inputOverview" ><?php echo $lecture->LECTURE_OVERVIEW; ?></textarea>
                                   <?php echo form_error('lecture-overview','<p class="text-danger">','</p>'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputVideo" class="col-md-2 control-label">Lecture Video:</label>
                                <div class="col-md-5">
                                  <input type="file" name="lecture-video" class="form-control" id="inputVideo" >
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                  <input type="submit" name="submit" class="btn btn-success" value="Create New Lecture">
                                </div>
                            </div>
                            
                            <?php echo form_close();?>
                        </div>
                    </div> 
                    
                                      
                </div>
                
